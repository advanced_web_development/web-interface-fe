import React, { useState, useContext, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Grid,
  Avatar,
  TextField,
  FormControl,
  InputLabel,
  InputAdornment,
  Input,
  OutlinedInput,
  IconButton,
  CircularProgress,
  Typography,
  Skeleton,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";
import { Campaign, Send } from "@mui/icons-material";

import InforClass from "../../components/newsTabComponent/inforClass";
import TabBar from "../../components/tab/tabbar";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  PostsContext,
  ReviewsContext,
  UserContext,
} from "../../store/context";
import { classService, gradeService } from "../../services";
import {
  authConst,
  classesConst,
  currentClassConst,
  severityConst,
} from "../../const";
import InfiniteScroll from "react-infinite-scroll-component";
import ClassPost from "../../components/post/classPost";
import { socket } from "../../socket";

function getCurrentClassInClassesById(classes, id) {
  let curClass = null;
  const indexTeaching = classes.teaching.findIndex(
    (item) => item.class_id === id
  );
  if (indexTeaching !== -1) {
    curClass = classes.teaching[indexTeaching].class_entity;
    curClass.role = classes.teaching[indexTeaching].role;
  }

  const indexEnrolled = classes.enrolled.findIndex(
    (item) => item.class_id === id
  );
  if (indexEnrolled !== -1) {
    curClass = classes.enrolled[indexEnrolled].class_entity;
    curClass.role = classes.enrolled[indexEnrolled].role;
  }
  return curClass;
}

function NewsPage() {
  const [currentClass, currentClassDispatch] = useContext(CurrentClassContext);
  const [authState, authDispatch] = useContext(AuthContext);
  const [classes] = useContext(ClassesContext);
  const { classId } = useParams();
  const [newContent, setNewContent] = useState("");
  const navigate = useNavigate();
  const backgroundImage =
    "url(https://www.gstatic.com/classroom/themes/img_learnlanguage.jpg)";

  const [creatingPost, setCreatingPost] = useState(false);
  const [lastPostId, setLastPostId] = useState(-1);
  const [hasMore, setHasMore] = useState(true);
  const [loadingPosts, setLoadingPosts] = useState(false);
  const [gradeStucture, setGradeStructure] = useState([]);
  const [, reviewsDispatch] = useContext(ReviewsContext);
  const [user] = useContext(UserContext);
  const [posts, postsDispatch] = useContext(PostsContext);

  const handleCreatePost = () => {
    setCreatingPost(true);
    if (newContent.length > 0) {
      classService
        .createClassPost({
          class_id: classId,
          content: newContent,
        })
        .then((data) => {
          data.comments = [];
          const author = {
            first_name: user.firstName,
            last_name: user.lastName,
          };
          data.author = author;
          postsDispatch({
            type: classesConst.CREATE_POST,
            payload: data,
          });
          enqueueSnackbar("Create a post successfully", {
            variant: severityConst.SUCCESS,
          });
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .finally(() => {
          setCreatingPost(false);
          setNewContent("");
        });
    }
  };

  const getClassPosts = (classId, lastId) => {
    classService
      .getClassPostWithPagination(classId, lastId)
      .then((data) => {
        if (data?.length > 0) {
          setLastPostId(data[data.length - 1].id);
          postsDispatch({
            type: classesConst.ADD_POSTS,
            payload: data,
          });
        }
        if (data?.length < 10) {
          setHasMore(false);
        }
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  };

  useEffect(() => {
    const curClass = getCurrentClassInClassesById(classes, classId);
    reviewsDispatch({
      type: classesConst.SET_REVIEWS,
      payload: [],
    });
    if (curClass && !curClass.is_activate) {
      enqueueSnackbar("Class is inactive", {
        variant: severityConst.ERROR,
      });
      navigate("/home");
      return;
    }
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
      setLoadingPosts(true);
      setHasMore(true);
      if (!classId) {
        return;
      }
      classService
        .getClassPostWithPagination(classId, -1)
        .then((data) => {
          if (data?.length > 0) {
            postsDispatch({
              type: classesConst.SET_POSTS,
              payload: data,
            });
            setLastPostId(data[data.length - 1].id);
            if (data?.length < 10) {
              setHasMore(false);
            }
          } else {
            postsDispatch({
              type: classesConst.SET_POSTS,
              payload: [],
            });
          }
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .finally(() => {
          setLoadingPosts(false);
        });

      gradeService
        .getGradeStructureClass(classId)
        .then((data) => {
          // console.log("data grade:", data);
          let sortedData = [];
          if (data.length !== 0) {
            sortedData = [...data]
              .sort((a, b) => a.order - b.order)
              .map((grade) => ({
                ...grade,
                isDeleted: false,
              }));
          }
          setGradeStructure(sortedData);
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    }
  }, [classId, classes]);

  function onReceiveComment(payload, postList) {
    if (payload?.comment && payload?.type) {
      if (payload.type === "class-post") {
        const postIdToUpdate = payload.comment.class_post_id;
        // Create a new array with the updated posts
        const updatedPosts = postList.map((post) => {
          if (post.id === postIdToUpdate) {
            return {
              ...post,
              comments: [...post.comments, payload.comment],
            };
          }
          return post; // Keep other posts unchanged
        });
        postsDispatch({
          type: classesConst.SET_POSTS,
          payload: updatedPosts,
        });
      }
    }
  }
  useEffect(() => {
    function setupSocketEvent() {
      socket.on("post_comment_noti", (msg) => onReceiveComment(msg, posts));
      if (socket.connected) {
        // socket.emit("associateUser", user.id || -1);
      } else {
        setTimeout(() => {
          setupSocketEvent(); // Re-setup event listeners
        }, 1000); // Retry after 3 seconds (adjust as needed)
      }
    }
    if (authState.user.id !== -1) {
      setupSocketEvent();
    }

    return () => {
      socket.off("post_comment_noti", (msg) => onReceiveComment(msg, posts));
    };
  }, [posts]);

  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "flex-end",
          width: { xs: "90%", sm: "90%", md: "90%", lg: "75%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        {/* board */}
        <Box
          sx={{
            width: "100%",
            height: { xs: "11rem", sm: "15rem", md: "15rem" },
            backgroundColor: "wheat",
            borderRadius: 3,
            marginBottom: 3,
            backgroundImage: backgroundImage,
          }}
        ></Box>
        {/* Infor class  */}
        {currentClass && <InforClass gradeStructure={gradeStucture} />}

        {/* content */}
        <Grid container>
          <Grid item xs={12} display={"flex"}>
            <FormControl fullWidth sx={{ my: 2 }}>
              <InputLabel htmlFor="outlined-adornment-amount">
                Announce something to your class
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                fullWidth
                multiline
                label="Announce something to your class"
                disabled={creatingPost}
                value={newContent}
                onChange={(e) => setNewContent(e.target.value)}
                startAdornment={
                  <InputAdornment position="start">
                    <Campaign />
                  </InputAdornment>
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      disabled={creatingPost || newContent.length === 0}
                      onClick={handleCreatePost}
                    >
                      {creatingPost ? <CircularProgress /> : <Send />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>

          <Grid item xs={12} display={"flex"}>
            <Box flexGrow={1}>
              {loadingPosts ? (
                <>
                  <Skeleton />
                  <Skeleton />
                  <Skeleton />
                </>
              ) : (
                <InfiniteScroll
                  dataLength={posts.length}
                  next={() => getClassPosts(currentClass.id, lastPostId)}
                  hasMore={hasMore}
                  style={{ overflow: "hidden" }}
                  loader={
                    loadingPosts || posts.length !== 0 ? (
                      <Box display={"flex"} justifyContent={"center"}>
                        <CircularProgress />
                      </Box>
                    ) : (
                      <Typography textAlign="center">
                        Yay! You have seen it all
                      </Typography>
                    )
                  }
                  endMessage={
                    loadingPosts ? (
                      <Box display={"flex"} justifyContent={"center"}>
                        <CircularProgress />
                      </Box>
                    ) : (
                      <Typography textAlign="center">
                        Yay! You have seen it all
                      </Typography>
                    )
                  }
                >
                  {posts.map((post) => {
                    return (
                      <ClassPost
                        key={post.id}
                        post={post}
                        isTeacher={
                          currentClass.role !== classesConst.ROLE_STUDENT
                            ? true
                            : false
                        }
                      />
                    );
                  })}
                </InfiniteScroll>
              )}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

export default NewsPage;
