import {
  Avatar,
  Card,
  Typography,
  CardActionArea,
  CardContent,
  Box,
  Divider,
  Button,
  TextField,
  IconButton,
  CircularProgress,
  Skeleton,
} from "@mui/material";
import { Send } from "@mui/icons-material";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import { formatVietnamDate } from "../../utils/convertDateFormat";
import {
  AuthContext,
  ClassesContext,
  PostsContext,
  UserContext,
} from "../../store/context";
import { classService } from "../../services";
import { severityConst, classesConst, authConst } from "../../const";
import { socket } from "../../socket";
import PostMenu from "../../components/post/postMenu";

function getCurrentClassInClassesById(classes, id) {
  let curClass = null;
  const indexTeaching = classes.teaching.findIndex(
    (item) => item.class_id === id
  );
  if (indexTeaching !== -1) {
    curClass = classes.teaching[indexTeaching].class_entity;
    curClass.role = classes.teaching[indexTeaching].role;
  }

  const indexEnrolled = classes.enrolled.findIndex(
    (item) => item.class_id === id
  );
  if (indexEnrolled !== -1) {
    curClass = classes.enrolled[indexEnrolled].class_entity;
    curClass.role = classes.enrolled[indexEnrolled].role;
  }
  return curClass;
}

export default function PostPage() {
  const [user] = useContext(UserContext);
  const [posts, postsDispatch] = useContext(PostsContext);
  const suffixes = posts[0]?.comments?.length > 1 ? "s" : "";
  const [creatingComment, setCreatingComment] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [showAllCmt, setShowAllCmt] = useState(true);
  const { classId, postId } = useParams();
  const [loading, setLoading] = useState(true);
  const [authState, authDispatch] = useContext(AuthContext);
  const [classes] = useContext(ClassesContext);
  const [curClass, setCurClass] = useState();
  const navigate = useNavigate();

  const handleCreateComment = () => {
    setCreatingComment(true);
    if (newComment.length > 0) {
      classService
        .createPostComment({
          postId: postId,
          content: newComment,
        })
        .then((data) => {
          const postIdToUpdate = data.class_post_id;
          // Create a new array with the updated posts
          const updatedPosts = posts.map((post) => {
            if (post.id === postIdToUpdate) {
              return {
                ...post,
                comments: [...post.comments, data],
              };
            }
            return post; // Keep other posts unchanged
          });
          postsDispatch({
            type: classesConst.SET_POSTS,
            payload: updatedPosts,
          });
          setNewComment("");
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .then(() => {
          setCreatingComment(false);
        });
    }
  };

  const handleToggleCmts = () => {
    setShowAllCmt((prev) => !prev);
  };

  useEffect(() => {
    if (classId?.length > 0 && postId?.length > 0) {
      setLoading(true);
      classService
        .getClassPost(postId)
        .then((data) => {
          postsDispatch({
            type: classesConst.SET_POSTS,
            payload: [data],
          });
          setCurClass(getCurrentClassInClassesById(classes, classId));
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          postsDispatch({
            type: classesConst.SET_POSTS,
            payload: [],
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          } else {
            navigate("/home");
          }
        })
        .then(() => {
          setLoading(false);
        });
    }
  }, [classId, postId]);

  function onReceiveComment(payload, postList) {
    if (payload?.comment && payload?.type) {
      if (payload.type === "class-post") {
        const postIdToUpdate = payload.comment.class_post_id;
        // Create a new array with the updated posts
        const updatedPosts = postList.map((post) => {
          if (post.id === postIdToUpdate) {
            return {
              ...post,
              comments: [...post.comments, payload.comment],
            };
          }
          return post; // Keep other posts unchanged
        });
        postsDispatch({
          type: classesConst.SET_POSTS,
          payload: updatedPosts,
        });
      }
    }
  }
  useEffect(() => {
    function setupSocketEvent() {
      socket.on("post_comment_noti", (msg) => onReceiveComment(msg, posts));
      if (socket.connected) {
        // socket.emit("associateUser", user.id || -1);
      } else {
        setTimeout(() => {
          setupSocketEvent(); // Re-setup event listeners
        }, 1000); // Retry after 3 seconds (adjust as needed)
      }
    }
    if (authState.user.id !== -1) {
      setupSocketEvent();
    }

    return () => {
      socket.off("post_comment_noti", (msg) => onReceiveComment(msg, posts));
    };
  }, [posts]);

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          mb: 2,
          width: {
            sm: "100%",
            md: "70%",
            lg: "60%",
            xl: "50%",
          },
        }}
      >
        <CardContent sx={{ p: 2 }}>
          <Box display={"flex"}>
            <Avatar>{posts[0]?.author?.first_name?.charAt(0)}</Avatar>
            <Box display={"flex"} flexGrow={1} flexDirection={"column"} px={2}>
              {loading || posts.length === 0 ? (
                <Skeleton />
              ) : (
                <Typography>{`${posts[0]?.author?.last_name} ${posts[0]?.author?.first_name}`}</Typography>
              )}
              {loading || posts.length === 0 ? (
                <Skeleton />
              ) : (
                <Typography variant="caption" sx={{ opacity: 0.5 }}>
                  {formatVietnamDate(posts[0]?.created_at)}
                </Typography>
              )}
            </Box>
            {user.id === posts[0]?.author?.id ||
            curClass?.role !== classesConst.ROLE_STUDENT ? (
              <PostMenu postId={posts[0]?.id} content={posts[0]?.content} />
            ) : (
              false
            )}
          </Box>
          <Typography variant="body2" pt={1}>
            {loading || posts.length === 0 ? <Skeleton /> : posts[0]?.content}
          </Typography>
        </CardContent>
        <Divider />
        <Box
          sx={{ px: 2, pb: 2, pt: 1 }}
          display={"flex"}
          flexDirection={"column"}
        >
          <Button
            color="standardGrey"
            size="small"
            sx={{ alignSelf: "self-start", mb: 1 }}
            onClick={handleToggleCmts}
          >
            {loading || posts.length === 0
              ? "0 comment"
              : `${posts[0]?.comments?.length} comment${suffixes}`}
          </Button>
          {showAllCmt && !loading && posts.length > 0 ? (
            posts[0].comments.map((cmt) => (
              <Box key={cmt.id} display={"flex"} alignItems={"center"} mb={1}>
                <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                  {cmt?.user?.first_name?.charAt(0)}
                </Avatar>
                <Box display={"flex"} flexDirection={"column"}>
                  <Box display={"flex"}>
                    <Typography variant="body2">{`${cmt?.user?.last_name} ${cmt?.user?.first_name}`}</Typography>
                    <Typography ml={1} variant="caption">
                      {formatVietnamDate(cmt.created_at)}
                    </Typography>
                  </Box>
                  <Typography variant="body2">{cmt.content}</Typography>
                </Box>
              </Box>
            ))
          ) : !loading && posts.length > 0 && posts[0].comments.length > 0 ? (
            <Box
              key={posts[0].comments[posts[0]?.comments?.length - 1].id}
              display={"flex"}
              alignItems={"center"}
              mb={1}
            >
              <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                {posts[0].comments[
                  posts[0]?.comments?.length - 1
                ]?.user?.first_name?.charAt(0)}
              </Avatar>
              <Box display={"flex"} flexDirection={"column"}>
                <Box display={"flex"}>
                  <Typography variant="body2">{`${
                    posts[0].comments[posts[0]?.comments?.length - 1]?.user
                      ?.last_name
                  } ${
                    posts[0].comments[posts[0]?.comments?.length - 1]?.user
                      ?.first_name
                  }`}</Typography>
                  <Typography ml={1} variant="caption">
                    {formatVietnamDate(
                      posts[0].comments[posts[0]?.comments?.length - 1]
                        .created_at
                    )}
                  </Typography>
                </Box>
                <Typography variant="body2">
                  {posts[0].comments[posts[0]?.comments?.length - 1].content}
                </Typography>
              </Box>
            </Box>
          ) : null}
          <Box display={"flex"} alignItems={"center"}>
            <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
              {user.firstName.charAt(0)}
            </Avatar>
            <TextField
              id="outlined-basic"
              label="Add a class comment"
              variant="outlined"
              size="small"
              value={newComment}
              onChange={(e) => setNewComment(e.target.value)}
              fullWidth
            />
            <IconButton
              disabled={creatingComment || newComment.length === 0}
              onClick={handleCreateComment}
            >
              {creatingComment ? <CircularProgress /> : <Send />}
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
