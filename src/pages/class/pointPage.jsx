import {
  Alert,
  Box,
  Button,
  Card,
  CardContent,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useNavigate, useParams } from "react-router-dom";
import React, {
  useEffect,
  useState,
  useContext,
  useMemo,
  useCallback,
} from "react";
import { enqueueSnackbar } from "notistack";
import CloseIcon from "@mui/icons-material/Close";
import CampaignIcon from "@mui/icons-material/Campaign";

import {
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";

import {
  severityConst,
  classesConst,
  authConst,
  currentClassConst,
} from "../../const";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  UserContext,
} from "../../store/context";
import { classService, gradeService } from "../../services";
import TabBar from "../../components/tab/tabbar";
import ButtonProgress from "../../components/button/buttonProgess";

function PointsPage() {
  const { classId } = useParams();
  const [, authDispatch] = useContext(AuthContext);
  const [currentClass, currentClassDispatch] = useContext(CurrentClassContext);
  const [user, userDispatch] = useContext(UserContext);
  const [classes] = useContext(ClassesContext);

  //   console.log("user:", user);

  const navigate = useNavigate();

  //State for table
  const [rowsTable, setRowsTable] = useState([]);
  const [columHeaderTableData, setColumHeaderTableData] = useState([]);
  const [columnOrder, setColumnOrder] = useState([]);

  const [openDialog, setOpenDialog] = useState(false);
  const [studentGrade, setStudentGrade] = useState({});

  const [requestState, setRequestState] = useState();
  const [requestLoading, setRequestLoading] = useState(false);

  function getCurrentClassInClassesById(classes, id) {
    let curClass = null;
    const indexTeaching = classes.teaching.findIndex(
      (item) => item.class_id === id
    );
    if (indexTeaching !== -1) {
      curClass = classes.teaching[indexTeaching].class_entity;
      curClass.role = classes.teaching[indexTeaching].role;
    }

    const indexEnrolled = classes.enrolled.findIndex(
      (item) => item.class_id === id
    );
    if (indexEnrolled !== -1) {
      curClass = classes.enrolled[indexEnrolled].class_entity;
      curClass.role = classes.enrolled[indexEnrolled].role;
    }
    return curClass;
  }

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setRequestState();
  };

  const handleRequestReview = (e) => {
    e.preventDefault();

    const { studentId } = user;
    const { currentGradeId, currentMaxGrade } = studentGrade;
    const { explaination, expectedGrade } = requestState;

    // Validate expected grade
    if (expectedGrade > currentMaxGrade) {
      enqueueSnackbar(
        "Expected grade must be less than or equal to exam max grade!",
        {
          variant: severityConst.ERROR,
        }
      );
      return;
    }

    setRequestLoading(true);

    const payload = {
      studentId,
      examId: currentGradeId,
      explaination,
      expectedGrade,
    };

    gradeService
      .requestReviewGradeExam(payload)
      .then(() => {
        enqueueSnackbar("Request review successfully!", {
          variant: severityConst.SUCCESS,
        });
        setOpenDialog(false);
        setRequestState();
      })
      .catch(handleApiError)
      .finally(() => {
        setRequestLoading(false);
      });
  };

  const handleNavigateToMappingTab = () => {
    navigate(`/class/${classId}/mapping`);
  };

  const handleChange = (e) => {
    let { name, value, type, checked } = e.target;
    let newValue;
    if (name === "explaination") {
      newValue = type === "checkbox" ? checked : value;
    }
    if (name === "expectedGrade") {
      if (isNaN(value)) {
        // If not a number, return without making any changes
        return;
      }

      // If the value is an empty string, default to 0
      newValue = type === "checkbox" ? checked : parseInt(value, 10);
    }
    setRequestState((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleClickReviewGrade = (
    gradeExamId,
    gradeExamMaxGrade,
    gradeExamTitle
  ) => {
    // console.log("column", rowsTable);
    const currentGrade = rowsTable[0][gradeExamTitle];
    setStudentGrade({
      currentGrade: currentGrade,
      currentTitle: gradeExamTitle,
      currentMaxGrade: gradeExamMaxGrade,
      currentGradeId: gradeExamId,
    });
    setOpenDialog(true);
  };

  const data = rowsTable || [];

  const columns = useMemo(() => {
    if (Array.isArray(data) && data.length > 0) {
      //check when have data
      if (columHeaderTableData === undefined) {
        Object.keys(data[0]).map((item) => {
          const column = {
            header: item,
            accessorKey: item,
            id: item,
          };
          return column;
        });
      }
      // console.log("columHeaderTableData", columHeaderTableData);
      return Object.keys(data[0]).map((item) => {
        const matchingHeader = columHeaderTableData.find(
          (headerData) => headerData.gradeExamTitle === item
        );

        if (matchingHeader) {
          const {
            gradeExamTitle,
            gradeExamId,
            gradeExamPercentage,
            gradeExamMaxGrade,
          } = matchingHeader;
          // console.log("first", item);
          return {
            enableSorting: false,
            accessorKey: item,
            size: 200,
            header: item,
            Header: ({ column }) => (
              <>
                <span style={{ display: "block" }}>
                  {column.columnDef.header} ({gradeExamPercentage}%)
                  <br />
                  <i style={{ fontSize: 10 }}>out of {gradeExamMaxGrade}</i>
                </span>

                <IconButton
                  sx={{}}
                  onClick={() =>
                    handleClickReviewGrade(
                      gradeExamId,
                      gradeExamMaxGrade,
                      gradeExamTitle
                    )
                  }
                >
                  <CampaignIcon />
                </IconButton>
              </>
            ),
          };
        }
        if (item === "StudentId" || item === "Total") {
          return {
            header: item,
            accessorKey: item,
            id: item,
            size: 150,
          };
        }

        // console.log("second:", item);
        return {
          header: item,
          accessorKey: item,
          id: item,
        };
      });
    }
    return [];
  }, [data]);

  const table = useMaterialReactTable({
    columns,
    data,
    enableColumnOrdering: false, //enable some features
    enableRowSelection: false,
    enablePagination: false, //disable a default feature

    enableColumnPinning: true,
    initialState: {
      columnPinning: { left: ["StudentId", "FullName"], right: ["Total"] },
    },

    enableBottomToolbar: false,
    enableStickyHeader: true,
    muiTableBodyCellProps: {
      sx: (theme) => ({
        backgroundColor:
          theme.palette.mode === "dark"
            ? theme.palette.grey[900]
            : theme.palette.grey[50],
      }),
    },

    enableGlobalFilter: false,
    enableFullScreenToggle: false,
    enableHiding: false,
    onColumnOrderChange: setColumnOrder,
    state: {
      columnOrder,
    },
    muiTableContainerProps: { sx: { maxHeight: "400px" } }, //set height table
  });

  const handleApiError = (err) => {
    enqueueSnackbar(err.response?.data.message || err.message, {
      variant: severityConst.ERROR,
    });
    if (err.response?.data.statusCode === 403) {
      navigate("/home");
    }
    if (err.response?.data.statusCode === 401) {
      authDispatch({
        type: authConst.LOG_OUT,
      });
      navigate("/login");
    }
  };

  useEffect(() => {
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
      if (!classId) {
        return;
      }
    }
  }, [classId, classes]);

  useEffect(() => {
    const payload = {
      classId,
      studentId: user.studentId,
    };
    //  console.log("payload", payload);

    gradeService.getGradeStructureClass(classId).then((data) => {
      let sortedData = [];
      if (data.length !== 0) {
        sortedData = [...data]
          .sort((a, b) => a.order - b.order)
          .map((grade) => ({
            ...grade,
            isDeleted: false,
          }));
      }

      gradeService.getAllExamMarkForStudent(payload).then((data) => {
        //   console.log("data", data);
        const dataGradeStudent = { ...data };
        //set rowstable
        if (!!data) {
          const newArray = {
            StudentId: data["studentId"],
            FullName: data["fullName"],
            Total: 0,
          };
          setRowsTable([{ ...newArray }]);
          gradeService
            .getExamGradeByClassId(classId)
            .then((data) => {
              //   console.log("data get all exam:", data);
              if (data.length > 0) {
                // console.log("data exam by class:", data);
                //Chỗ này là tính số lượng điểm thành phần của một gradeCategory
                const countMap = data.reduce((dataComposition, obj) => {
                  dataComposition[obj.grade_composition_id] =
                    (dataComposition[obj.grade_composition_id] || 0) +
                    (sortedData.find(
                      (gradeCategory) =>
                        gradeCategory.id === obj.grade_composition_id
                    )
                      ? 1
                      : 0);
                  return dataComposition;
                }, {});

                // Chỗ này là tính phần trăm của grade exam = gradeCategory.weight / số điểm thành phần
                //Và lấy thêm các thuộc tính khác
                const gradeExamInfoTable = data.map((item) => {
                  const count = countMap[item.grade_composition_id] || 0;
                  const percentageOfExam =
                    count > 0
                      ? (sortedData.find(
                          (gradeStructure) =>
                            gradeStructure.id === item.grade_composition_id
                        )?.weight || 0) / count
                      : 0;

                  return {
                    gradeExamId: item.id,
                    gradeExamTitle: item.title,
                    gradeExamMaxGrade: item.max_grade,
                    gradeExamPercentage: percentageOfExam,
                  };
                });
                //  console.log("gradeExamInfoTable", gradeExamInfoTable);
                setColumHeaderTableData(gradeExamInfoTable);
                const gradeExamsMap = new Map(
                  gradeExamInfoTable.map(({ gradeExamTitle, ...rest }) => [
                    gradeExamTitle,
                    rest,
                  ])
                );
                const gradeExamsInfo = {};

                gradeExamInfoTable.forEach((gradeExam) => {
                  const { gradeExamTitle, gradeExamMaxGrade } = gradeExam;
                  gradeExamsInfo[gradeExamTitle] = 0; // Default value
                });

                // Create a new array with combined information
                const studentObject = {
                  StudentId: newArray.StudentId,
                  FullName: newArray.FullName,
                  ...gradeExamsInfo,
                  Total: newArray.Total,
                };

                let newStudentObject = { ...studentObject };

                dataGradeStudent.compositionList.forEach((composition) => {
                  composition.examStudentList.forEach((exam) => {
                    newStudentObject[exam.title] = exam.grade;
                  });
                });

                let total = 0;

                gradeExamInfoTable.forEach((grade) => {
                  total +=
                    (newStudentObject[grade.gradeExamTitle] *
                      grade.gradeExamPercentage) /
                    (10 * grade.gradeExamMaxGrade);
                });

                // Update the Total in the studentInfo object
                newStudentObject.Total = parseFloat(total.toFixed(2));

                //  console.log("newStudentObject", newStudentObject);

                setRowsTable([{ ...newStudentObject }]);
              }
            })
            .catch((err) => handleApiError(err));
        }
      });
      // .catch((err) => handleApiError(err));
    });

    setColumnOrder(columns.map((column) => column.id));
  }, []);

  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "start",
          width: { xs: "90%", sm: "95%", md: "95%", lg: "95%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            width: 1120,
            mt: 6,
            mx: "auto",
          }}
        >
          {user.studentId ? (
            <MaterialReactTable table={table} layoutMode="fixed-width" />
          ) : (
            <Box sx={{ mx: "auto", mt: -2 }}>
              <Alert severity="warning">
                You must have a studentId mapped with your account in order to
                view grade.
              </Alert>
              <Button onClick={handleNavigateToMappingTab} sx={{ mt: 4 }}>
                Click here to map Student ID
              </Button>
            </Box>
          )}
        </Box>

        <Dialog
          onClose={handleCloseDialog}
          aria-labelledby="customized-dialog-title"
          open={openDialog}
        >
          <DialogTitle
            sx={{
              display: "flex",
              justifyContent: "space-between",
            }}
            id="customized-dialog-title"
          >
            <Typography align="center">
              Request review for {studentGrade.currentTitle}
            </Typography>
            <IconButton
              aria-label="close"
              onClick={handleCloseDialog}
              sx={{
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>

          <DialogContent
            sx={{
              paddingBottom: 0,
              minWidth: {
                xl: 600,
              },
            }}
          >
            <Box
              component="form"
              sx={{ mx: "auto" }}
              onSubmit={(e) => handleRequestReview(e)}
            >
              {/* <Grid
              container
              justifyContent="center"
              alignItems="center"
              sx={{
                width: {
                  sm: "60%",
                  md: "60%",
                  lg: "50%",
                  xl: "50%",
                },
                mt: 6,
                mx: "auto",
              }}
            > */}
              <Card sx={{ minWidth: 380 }}>
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="currentGrade"
                        label="Current Grade"
                        name="curentGrade"
                        value={studentGrade.currentGrade}
                        disabled
                      />
                    </Grid>

                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="expectedGrade"
                        label={`Expected Grade (out of ${studentGrade.currentMaxGrade})`}
                        name="expectedGrade"
                        //  autoComplete="name"
                        value={
                          requestState?.expectedGrade
                            ? requestState.expectedGrade
                            : 0
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>

                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="explaination"
                        label="Explaination"
                        name="explaination"
                        //  autoComplete="name"
                        value={
                          requestState?.explaination
                            ? requestState.explaination
                            : ""
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              {/* </Grid> */}

              <ButtonProgress
                loading={requestLoading}
                text={"Save"}
                onClick={(e) => {
                  handleRequestReview(e);
                }}
              />
            </Box>
          </DialogContent>
          {/* <DialogActions>
        <Button onClick={handleCloseDialog}>Cancel</Button>
      </DialogActions> */}
        </Dialog>
      </Box>
    </Box>
  );
}

export default PointsPage;
