import {
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useNavigate, useParams } from "react-router-dom";
import React, {
  useEffect,
  useState,
  useContext,
  useMemo,
  useCallback,
} from "react";
import { enqueueSnackbar } from "notistack";
import DownloadIcon from "@mui/icons-material/Download";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import DoneIcon from "@mui/icons-material/Done";

import {
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import { mkConfig, generateCsv, download } from "export-to-csv";

import {
  severityConst,
  classesConst,
  authConst,
  currentClassConst,
} from "../../const";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
} from "../../store/context";
import { classService, gradeService } from "../../services";
import TabBar from "../../components/tab/tabbar";
import ButtonProgress from "../../components/button/buttonProgess";
import ExamGradeDialog from "../../components/dialog/addExamGradeDialog";
import ImportFileDialog from "../../components/dialog/importFileDialog";
import {
  handleGroupData,
  newArrayWhenCaculateGradeTotal,
} from "../../utils/grade";
import AlertNotFinalizedDialog from "../../components/dialog/alertNotFinalizedDialog";

const csvConfig = mkConfig({
  fieldSeparator: ",",
  decimalSeparator: ".",
  useKeysAsHeaders: true,
});

function GradesPage() {
  const { classId } = useParams();
  const [, authDispatch] = useContext(AuthContext);

  const navigate = useNavigate();
  //state for create new exam grade
  const [gradeStructure, setGradeStructure] = useState();
  const [currentExam, setCurrentExam] = useState({
    grade_exam_category: "",
    grade_exam_title: "",
    grade_exam_max_grade: "",
  }); //state store value exam want to edit

  //State for dialog
  const [open, setOpen] = useState(false);
  const [anchorElClass, setAnchorElClass] = useState(null);
  const [notFinalizedList, setNotFinalizedList] = useState([]);
  const [gradeExamTitleForDialog, setGradeExamTitleForDialog] = useState();
  const [openDialogAlertListNotFinalized, setOpenDialogAlertListNotFinalized] =
    useState(false);
  const openClass = Boolean(anchorElClass);

  const [openImportDialog, setOpenImportDialog] = useState(false);

  const handleExportData = () => {
    const csv = generateCsv(csvConfig)(data);
    download(csvConfig)(csv);
  };

  //State for table
  const [rowsTable, setRowsTable] = useState([]);
  const [columHeaderTableData, setColumHeaderTableData] = useState([]);
  const [columnOrder, setColumnOrder] = useState([]);
  const [validationErrors, setValidationErrors] = useState({});
  //keep track of rows that have been edited
  const [editedGrades, setEditedGrades] = useState([]);

  const [examId, setExamId] = useState();
  const [flagChange, setFlagChange] = useState(false);

  const handleClickImportGradeFile = (e) => {
    // console.log("target", e);
    setOpenImportDialog(true);
    setExamId(e);
  };
  const handleClickDownLoadFile = () => {
    gradeService
      .downLoadTemplateMapped(classId)
      .catch((err) => handleApiError(err));
  };
  const handleClickFinalized = (gradeExamId, gradeExamTitle) => {
    // console.log("target", gradeExamId);
    gradeService
      .finalizedForExamGrade({ examId: gradeExamId })
      .then((data) => {
        if (data["notFinalized"].length > 0) {
          setNotFinalizedList([...data["notFinalized"]]);
          setGradeExamTitleForDialog(gradeExamTitle);
          setOpenDialogAlertListNotFinalized(true);
        } else {
          enqueueSnackbar(
            `Finalized grade exam for ${gradeExamTitle} successfully!`,
            {
              variant: severityConst.SUCCESS,
            }
          );
        }
      })
      .catch((err) => handleApiError(err));
  };
  const validateGradeUpdate = (value, maxGradeExam) => {
    if (value.trim().length === 0) {
      return "Grade is not null";
    }

    const parsedValue = parseInt(value);
    if (isNaN(parsedValue)) {
      return "Grade must be a number";
    }

    if (parsedValue > maxGradeExam) {
      return "Grade out of max grade";
    }

    return null;
  };

  const data = rowsTable || [];

  const handleSaveGrade = () => {
    // console.log("editedGrades:", editedGrades);
    const payload = {
      studentExamList: [...editedGrades],
      classId,
    };
    gradeService
      .saveTable(payload)
      .then((data) => {
        setValidationErrors({});
        setEditedGrades({});
        setFlagChange((prevFlag) => !prevFlag);
      })
      .catch((err) => handleApiError(err));
  };

  const columns = useMemo(() => {
    if (Array.isArray(data) && data.length > 0) {
      //check when have data

      if (columHeaderTableData === undefined) {
        Object.keys(data[0]).map((item) => {
          const column = {
            header: item,
            accessorKey: item,
            id: item,
            enableEditing: false,
          };
          return column;
        });
        // return [];
      }
      // console.log("columHeaderTableData", columHeaderTableData);
      return Object.keys(data[0]).map((item) => {
        const matchingHeader = columHeaderTableData.find(
          (headerData) => headerData.gradeExamTitle === item
        );

        if (matchingHeader) {
          const {
            gradeExamTitle,
            gradeExamId,
            gradeExamPercentage,
            gradeExamMaxGrade,
          } = matchingHeader;
          // console.log("first", item);
          return {
            enableSorting: false,
            enableEditing: true,
            accessorKey: item,
            size: 200,
            header: item,
            Header: ({ column }) => (
              <>
                <span style={{ display: "block" }}>
                  {column.columnDef.header} ({gradeExamPercentage}%)
                  <br />
                  out of {gradeExamMaxGrade}
                </span>

                <IconButton sx={{}} onClick={handleClickDownLoadFile}>
                  <DownloadIcon />
                </IconButton>

                <IconButton
                  onClick={() => handleClickImportGradeFile(gradeExamId)}
                >
                  <FileUploadIcon />
                </IconButton>
                <IconButton
                  onClick={() =>
                    handleClickFinalized(gradeExamId, gradeExamTitle)
                  }
                >
                  <DoneIcon />
                </IconButton>
              </>
            ),
            muiEditTextFieldProps: ({ cell, row }) => ({
              type: "text",
              required: true,
              // error: !!validationErrors?.[cell.id],
              // helperText: validationErrors?.[cell.id],
              //store edited user in state to be saved later
              onBlur: (event) => {
                const newValue = event.currentTarget.value;
                const oldValue = row.original[cell.column.id];
                const validationError = validateGradeUpdate(
                  newValue,
                  gradeExamMaxGrade
                );
                // console.log("loi o day:", validationError);
                if (validationError) {
                  setValidationErrors({
                    ...validationErrors,
                    [cell.id]:
                      validationError +
                      " in StudentID: " +
                      row.original["StudentId"] +
                      " column " +
                      gradeExamTitle,
                  });
                } else {
                  //if old value != new value
                  // No validation error, value is different from the old value, update editedGrades
                  if (oldValue !== parseInt(newValue)) {
                    // setValidationErrors({
                    //   ...validationErrors,
                    //   [cell.id]: undefined, // Clear the validation error for this cell
                    // });
                    setValidationErrors((prevValidationErrors) => {
                      const updatedErrors = { ...prevValidationErrors };
                      delete updatedErrors[cell.id];

                      return updatedErrors;
                    });

                    const studentId = row.original["StudentId"];
                    const examId = gradeExamId;

                    // Check if the object with the same studentId and examId already exists
                    const existingIndex = editedGrades.findIndex(
                      (grade) =>
                        grade.studentId === studentId && grade.examId === examId
                    );

                    if (existingIndex !== -1) {
                      // If exists, update the existing object's grade property
                      setEditedGrades((prevEditedGrades) => {
                        const updatedGrades = [...prevEditedGrades];
                        updatedGrades[existingIndex] = {
                          ...updatedGrades[existingIndex],
                          grade: parseInt(newValue),
                        };
                        return updatedGrades;
                      });
                    } else {
                      // If not exists, add a new object to editedGrades
                      const newGradeUpdate = {
                        studentId: studentId,
                        grade: parseInt(newValue),
                        isFinalized: false,
                        examId: examId,
                      };
                      setEditedGrades((prevEditedGrades) => [
                        ...prevEditedGrades,
                        newGradeUpdate,
                      ]);
                    }
                  } else {
                    //if oldValue = newValu, check in editedGrade
                    //if exist object have this studentId and examId, remove it
                    setValidationErrors({
                      ...validationErrors,
                      [cell.id]: undefined, // Clear the validation error for this cell
                    });

                    const studentId = row.original["StudentId"];
                    const examId = gradeExamId;

                    // Check if the object with the same studentId and examId already exists
                    const existingIndex = editedGrades.findIndex(
                      (grade) =>
                        grade.studentId === studentId && grade.examId === examId
                    );

                    if (existingIndex !== -1) {
                      // If exists, remove the existing object from editedGrades
                      setEditedGrades((prevEditedGrades) => {
                        const updatedGrades = [...prevEditedGrades];
                        updatedGrades.splice(existingIndex, 1);
                        return updatedGrades;
                      });
                    }
                  }
                }
              },
            }),
          };
        }
        if (item === "StudentId" || item === "Total") {
          return {
            header: item,
            accessorKey: item,
            id: item,
            size: 150,
            enableEditing: false,
          };
        }

        // console.log("second:", item);
        return {
          header: item,
          accessorKey: item,
          id: item,
          enableEditing: false,
        };
      });
    }
    return [];
  }, [data, editedGrades, validationErrors]);

  const table = useMaterialReactTable({
    columns,
    data,
    enableColumnOrdering: false, //enable some features
    enableRowSelection: false,
    enablePagination: false, //disable a default feature

    createDisplayMode: "row",
    editDisplayMode: "cell",
    enableEditing: true,
    getRowId: (row) => row.id,
    renderBottomToolbarCustomActions: ({ cell, row }) => (
      <Box sx={{ display: "flex", gap: "1rem", alignItems: "center" }}>
        <Button
          color="success"
          variant="contained"
          onClick={handleSaveGrade}
          disabled={
            Object.keys(editedGrades).length === 0 ||
            Object.values(validationErrors).some((error) => !!error)
          }
        >
          {/* {isUpdatingUsers ? <CircularProgress size={25} /> : "Save"} */}
          Save
        </Button>
        {Object.values(validationErrors).some((error) => !!error) && (
          <Typography color="error" fontStyle="italic">
            Fix errors before submitting: {Object.values(validationErrors)[0]}
          </Typography>
        )}
      </Box>
    ),

    enableColumnPinning: true,
    initialState: {
      columnPinning: { left: ["StudentId", "FullName"], right: ["Total"] },
    },

    enableBottomToolbar: true,
    enableStickyHeader: true,
    muiTableBodyCellProps: {
      sx: (theme) => ({
        backgroundColor:
          theme.palette.mode === "dark"
            ? theme.palette.grey[900]
            : theme.palette.grey[50],
      }),
    },

    enableGlobalFilter: false,
    enableFullScreenToggle: false,
    enableHiding: false,
    onColumnOrderChange: setColumnOrder,
    state: {
      columnOrder,
      // showAlertBanner: isLoadingUsersError,
      // showProgressBars: isFetchingUsers,
    },
    layoutMode: "grid-no-grow",
    muiTableContainerProps: { sx: { maxHeight: "400px" } }, //set height table
    renderTopToolbarCustomActions: ({ table }) => (
      <Box
        sx={{
          display: "flex",
          gap: "16px",
          padding: "8px",
          flexWrap: "wrap",
        }}
      >
        <Button onClick={handleExportData} startIcon={<DownloadIcon />}>
          Export All Data
        </Button>
      </Box>
    ),
  });

  const someEventHandler = () => {
    //read the table state during an event from the table instance
    // console.log(table.getState().sorting);
  };

  const hanldeClickOpenFullscreenDialog = () => {
    setOpen(true);
  };

  const handleClickClass = (event) => {
    setAnchorElClass(event.currentTarget);
  };

  const handleCloseClass = () => {
    setAnchorElClass(null);
  };

  const handleDownloadTemplate = () => {
    window.location.href =
      "https://fitclassroom.buudadawg.online/v1/api/gateway/mapping/download-template?isEmailIncluded=true";
  };

  const handleImportGradeClass = () => {
    setOpenImportDialog(true);
  };

  const handleApiError = (err) => {
    enqueueSnackbar(err.response?.data.message || err.message, {
      variant: severityConst.ERROR,
    });
    if (err.response?.data.statusCode === 403) {
      navigate("/home");
    }
    if (err.response?.data.statusCode === 401) {
      authDispatch({
        type: authConst.LOG_OUT,
      });
      navigate("/login");
    }
  };

  const [, currentClassDispatch] = useContext(CurrentClassContext);
  const [classes] = useContext(ClassesContext);
  function getCurrentClassInClassesById(classes, id) {
    let curClass = null;
    const indexTeaching = classes.teaching.findIndex(
      (item) => item.class_id === id
    );
    if (indexTeaching !== -1) {
      curClass = classes.teaching[indexTeaching].class_entity;
      curClass.role = classes.teaching[indexTeaching].role;
    }

    const indexEnrolled = classes.enrolled.findIndex(
      (item) => item.class_id === id
    );
    if (indexEnrolled !== -1) {
      curClass = classes.enrolled[indexEnrolled].class_entity;
      curClass.role = classes.enrolled[indexEnrolled].role;
    }
    return curClass;
  }

  useEffect(() => {
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
    }
  }, [classId, classes]);

  useEffect(() => {
    gradeService
      .getGradeStructureClass(classId)
      .then((data) => {
        let sortedData = [];
        if (data.length !== 0) {
          sortedData = [...data]
            .sort((a, b) => a.order - b.order)
            .map((grade) => ({
              ...grade,
              isDeleted: false,
            }));
        }
        // console.log("sort", sortedData);

        setGradeStructure([...sortedData]);
        gradeService
          .getStudentList(classId)
          .then((data) => {
            if (data.length > 0) {
              const newArray = data.map((item) => {
                return {
                  StudentId: item.student_id,
                  FullName: item.name,
                  Total: 0,
                };
              });
              setRowsTable([...newArray]);

              //Get all grade composition then add to key of rowsTable (in each element add all key)
              gradeService
                .getExamGradeByClassId(classId)
                .then((data) => {
                  if (data.length > 0) {
                    // console.log("data exam by class:", data);
                    //Chỗ này là tính số lượng điểm thành phần của một gradeCategory
                    const countMap = data.reduce((dataComposition, obj) => {
                      dataComposition[obj.grade_composition_id] =
                        (dataComposition[obj.grade_composition_id] || 0) +
                        (sortedData.find(
                          (gradeCategory) =>
                            gradeCategory.id === obj.grade_composition_id
                        )
                          ? 1
                          : 0);
                      return dataComposition;
                    }, {});

                    // Chỗ này là tính phần trăm của grade exam = gradeCategory.weight / số điểm thành phần
                    //Và lấy thêm các thuộc tính khác
                    const gradeExamInfoTable = data.map((item) => {
                      const count = countMap[item.grade_composition_id] || 0;
                      const percentageOfExam =
                        count > 0
                          ? (sortedData.find(
                              (gradeStructure) =>
                                gradeStructure.id === item.grade_composition_id
                            )?.weight || 0) / count
                          : 0;

                      return {
                        gradeExamId: item.id,
                        gradeExamTitle: item.title,
                        gradeExamMaxGrade: item.max_grade,
                        gradeExamPercentage: percentageOfExam,
                      };
                    });
                    // console.log("gradeExamInfoTable:", gradeExamInfoTable);

                    //get key to add to student
                    // const columnHeader = data.map((item) => {
                    //   return {
                    //     gradeExamId: item.id,
                    //     gradeExamTitle: item.title,
                    //     gradeExamMaxGrade: item.max_grade,
                    //   };
                    // });
                    setColumHeaderTableData(gradeExamInfoTable);
                    const gradeExamsMap = new Map(
                      gradeExamInfoTable.map(({ gradeExamTitle, ...rest }) => [
                        gradeExamTitle,
                        rest,
                      ])
                    );

                    // Create a new array with combined information
                    const studentsArray = newArray.map((student) => {
                      const { StudentId, FullName, Total } = student;
                      const gradeExamsInfo = {};

                      gradeExamInfoTable.forEach((gradeExam) => {
                        const { gradeExamTitle, gradeExamMaxGrade } = gradeExam;
                        gradeExamsInfo[gradeExamTitle] = 0; // Default value
                      });

                      return {
                        StudentId,
                        FullName,
                        ...gradeExamsInfo,
                        Total,
                      };
                    });
                    // console.log("student array:", studentsArray);

                    gradeService
                      .getAllExamMarkForClass(classId)
                      .then((data) => {
                        const newStudentsArray = [...studentsArray];
                        // console.log("studentId:", studentsArray);
                        // console.log("data grade:", data);
                        // console.log("newStudentsArray:", newStudentsArray);
                        //update value of grade exam
                        newStudentsArray.forEach((student) => {
                          const { StudentId } = student;
                          const studentComposition = data.find(
                            (comp) => comp.studentId === StudentId
                          );

                          if (studentComposition) {
                            studentComposition.compositionList.forEach(
                              (composition) => {
                                const { name, examStudentList } = composition;

                                examStudentList.forEach((exam) => {
                                  const { title, grade } = exam;

                                  const gradeKey = title;

                                  // Check if the exam title exists in the student object
                                  if (student.hasOwnProperty(gradeKey)) {
                                    // If grade is null, keep it as is; otherwise, update the value
                                    student[gradeKey] =
                                      grade !== null
                                        ? grade
                                        : student[gradeKey];
                                  }
                                });
                              }
                            );
                          }
                        });

                        //caculate total, if total > 10 return 10
                        const updatedStudentArray =
                          newArrayWhenCaculateGradeTotal(
                            newStudentsArray,
                            gradeExamInfoTable
                          );

                        setRowsTable(updatedStudentArray);
                        // console.log("abc:", updatedStudentArray);
                      })
                      .catch((err) => handleApiError(err));
                  }
                })
                .catch((err) => handleApiError(err));
            }
          })
          .catch((err) => handleApiError(err));
      })
      .catch((err) => handleApiError(err));

    //Get row: get all student id, coreresponding student id have a column grade composition
    setColumnOrder(columns.map((column) => column.id));
  }, [flagChange]);

  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "start",
          width: { xs: "90%", sm: "95%", md: "95%", lg: "95%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            width: "100%",
            mb: 5,
          }}
        >
          <Button onClick={hanldeClickOpenFullscreenDialog} variant="contained">
            Create new exam grade
          </Button>

          <ExamGradeDialog
            openDialog={open}
            setOpen={setOpen}
            gradeStructure={gradeStructure}
            gradeExamExisted={columHeaderTableData}
            currentExamDialog={currentExam}
            setFlagChange={setFlagChange}
            flagChange={flagChange}
          />

          <AlertNotFinalizedDialog
            setOpen={setOpenDialogAlertListNotFinalized}
            openDialog={openDialogAlertListNotFinalized}
            gradeExamTitle={gradeExamTitleForDialog}
            notFinalizedList={notFinalizedList}
          />

          <Button
            id="class"
            aria-controls={openClass ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={openClass ? "true" : undefined}
            onClick={handleClickClass}
            variant="outlined"
            endIcon={<MenuIcon />}
            sx={{ borderRadius: 3, color: "#5783db" }}
          >
            Class
          </Button>
          <Menu
            id="basic-menu"
            anchorEl={anchorElClass}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            open={openClass}
            onClose={handleCloseClass}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <MenuItem onClick={handleDownloadTemplate}>
              Download file template for student list
            </MenuItem>
            <MenuItem onClick={handleImportGradeClass}>
              Upload file student list
            </MenuItem>
          </Menu>
        </Box>

        <ImportFileDialog
          openDialog={openImportDialog}
          setOpen={setOpenImportDialog}
          setFlagChange={setFlagChange}
          flagChange={flagChange}
          gradeExamIdToSend={examId}
        />

        <Box
          sx={{
            width: 1120,
            mx: "auto",
          }}
        >
          <MaterialReactTable table={table} layoutMode="fixed-width" />
        </Box>
      </Box>
    </Box>
  );
}

export default GradesPage;
