import {
  Avatar,
  Card,
  Typography,
  CardActionArea,
  CardContent,
  Box,
  Divider,
  Button,
  TextField,
  IconButton,
  CircularProgress,
  CardActions,
  Collapse,
} from "@mui/material";
import {
  Announcement,
  Check,
  CheckCircleOutline,
  HourglassBottom,
  MoreVert,
  Pending,
  Send,
} from "@mui/icons-material";
import { useContext, useEffect, useState } from "react";
import { enqueueSnackbar } from "notistack";

import { formatVietnamDate } from "../../utils/convertDateFormat";
import {
  AuthContext,
  PostsContext,
  ReviewsContext,
  UserContext,
  ClassesContext,
} from "../../store/context";
import { classService } from "../../services";
import { classesConst, authConst, severityConst } from "../../const";
import { useNavigate, useParams } from "react-router-dom";
import ReviewMenu from "../../components/post/reviewMenu";
import { socket } from "../../socket";

const convertStatus = (s) => {
  if (s === classesConst.NEW_REQUEST) {
    return "new";
  } else if (s === classesConst.INPROG_REQUEST) {
    return "processing";
  } else {
    return "resolved";
  }
};

function getCurrentClassInClassesById(classes, id) {
  let curClass = null;
  const indexTeaching = classes.teaching.findIndex(
    (item) => item.class_id === id
  );
  if (indexTeaching !== -1) {
    curClass = classes.teaching[indexTeaching].class_entity;
    curClass.role = classes.teaching[indexTeaching].role;
  }

  const indexEnrolled = classes.enrolled.findIndex(
    (item) => item.class_id === id
  );
  if (indexEnrolled !== -1) {
    curClass = classes.enrolled[indexEnrolled].class_entity;
    curClass.role = classes.enrolled[indexEnrolled].role;
  }
  return curClass;
}

export default function GradeReviewPage() {
  const [user] = useContext(UserContext);
  const [reviews, reviewsDispatch] = useContext(ReviewsContext);
  const suffixes = reviews[0]?.ReviewComment?.length > 1 ? "s" : "";
  const [creatingComment, setCreatingComment] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [showAllCmt, setShowAllCmt] = useState(true);
  const [loading, setLoading] = useState(true);
  const [curClass, setCurClass] = useState();
  const [authState, authDispatch] = useContext(AuthContext);
  const [classes] = useContext(ClassesContext);
  const { classId, examId, examReviewId } = useParams();
  const navigate = useNavigate();

  const handleCreateComment = () => {
    setCreatingComment(true);
    if (newComment.length > 0) {
      classService
        .createReviewPostComment({
          examId: reviews[0].ExamStudent?.Exam?.id,
          classId: reviews[0].ExamStudent?.Exam?.GradeComposition?.class_id,
          examReviewId: reviews[0].id,
          content: newComment,
        })
        .then((data) => {
          const postIdToUpdate = data.exam_review_id;
          // Create a new array with the updated posts
          const updatedReviewPosts = reviews.map((post) => {
            if (post.id === postIdToUpdate) {
              return {
                ...post,
                ReviewComment: [...post.ReviewComment, data],
              };
            }
            return post; // Keep other posts unchanged
          });
          reviewsDispatch({
            type: classesConst.SET_REVIEWS,
            payload: updatedReviewPosts,
          });
          setNewComment("");
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .then(() => {
          setCreatingComment(false);
        });
    }
  };

  const handleToggleCmts = () => {
    setShowAllCmt((prev) => !prev);
  };

  useEffect(() => {
    if (classId?.length > 0 && examReviewId?.length > 0) {
      setLoading(true);
      classService
        .getReviewPost(examReviewId)
        .then((data) => {
          reviewsDispatch({
            type: classesConst.SET_REVIEWS,
            payload: [data],
          });
          setCurClass(getCurrentClassInClassesById(classes, classId));
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          reviewsDispatch({
            type: classesConst.SET_POSTS,
            payload: [],
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          } else {
            navigate("/home");
          }
        })
        .then(() => {
          setLoading(false);
        });
    }
  }, [classId, examId, examReviewId]);

  function onReceiveComment(payload, postList) {
    if (payload?.comment && payload?.type) {
      if (payload.type === "exam-review") {
        const postIdToUpdate = payload.comment.exam_review_id;
        // Create a new array with the updated posts
        const updatedPosts = postList.map((post) => {
          if (post.id === postIdToUpdate) {
            return {
              ...post,
              ReviewComment: [...post.ReviewComment, payload.comment],
            };
          }
          return post; // Keep other posts unchanged
        });
        reviewsDispatch({
          type: classesConst.SET_REVIEWS,
          payload: updatedPosts,
        });
      }
    }
  }
  useEffect(() => {
    setTimeout(() => {
      function setupSocketEvent() {
        socket.on("post_comment_noti", (msg) => onReceiveComment(msg, reviews));
        if (socket.connected) {
          // socket.emit("associateUser", user.id || -1);
        } else {
          setTimeout(() => {
            setupSocketEvent(); // Re-setup event listeners
          }, 1000); // Retry after 3 seconds (adjust as needed)
        }
      }
      if (authState.user.id !== -1) {
        setupSocketEvent();
      }
    }, 500);
    return () => {
      socket.off("post_comment_noti", (msg) => onReceiveComment(msg, reviews));
    };
  }, [reviews]);

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "70%",
          borderRadius: 1,
          my: 1,
        }}
      >
        <CardContent>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              alignContent: "center",
            }}
          >
            <Announcement />
            <Typography
              sx={{ flexGrow: 1 }}
            >{`Grade review for exam ${reviews[0]?.ExamStudent?.Exam?.title} of student ID: ${reviews[0]?.ExamStudent?.student_class_student_id}`}</Typography>
            <Typography variant="caption" sx={{ opacity: 0.5 }}>
              {`Requested ${formatVietnamDate(reviews[0]?.created_at)}`}
            </Typography>
          </Box>
          <Box display={"flex"}>
            <Typography sx={{ mr: 1 }}>{`Status: ${convertStatus(
              reviews[0]?.state
            )}`}</Typography>
            {reviews[0]?.state === classesConst.NEW_REQUEST ? (
              <Pending color="error" />
            ) : reviews[0]?.state === classesConst.INPROG_REQUEST ? (
              <HourglassBottom color="warning" />
            ) : (
              <CheckCircleOutline color="success" />
            )}
          </Box>
        </CardContent>
        <Divider />
        <Box sx={{ display: "flex", px: 2, pb: 2, pt: 1 }}>
          <Box sx={{ display: "flex", flexDirection: "column", flexGrow: 1 }}>
            <Typography variant="body2">{`Explaination:   ${reviews[0]?.explaination}`}</Typography>
            <Typography variant="body2">{`Expected grade: ${reviews[0]?.expected_grade}/${reviews[0]?.ExamStudent.Exam.max_grade}`}</Typography>
            <Typography variant="body2">{`Updated grade: ${
              reviews[0]?.updated_grade ? reviews[0]?.updated_grade : ""
            }`}</Typography>
            {reviews[0]?.updated_at ? (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "start",
                }}
              >
                <Typography variant="body2">{`Updated by: ${
                  reviews[0]?.User.last_name + " " + reviews[0]?.User.first_name
                } at ${formatVietnamDate(reviews[0]?.updated_at)}`}</Typography>
              </Box>
            ) : null}
          </Box>
          {curClass?.role !== classesConst.ROLE_STUDENT ||
          user?.id === reviews[0]?.author?.id ? (
            <ReviewMenu reviewPost={reviews[0]} />
          ) : null}
        </Box>
        <Divider />
        <Box
          sx={{ px: 2, pb: 2, pt: 1 }}
          display={"flex"}
          flexDirection={"column"}
        >
          <Button
            color="standardGrey"
            size="small"
            sx={{ alignSelf: "self-start", mb: 1 }}
            onClick={handleToggleCmts}
          >
            {`${reviews[0]?.ReviewComment?.length} comment${suffixes}`}
          </Button>
          {showAllCmt ? (
            reviews[0]?.ReviewComment?.map((cmt) => (
              <Box key={cmt.id} display={"flex"} alignItems={"center"} mb={1}>
                <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                  {cmt?.User?.first_name?.charAt(0)}
                </Avatar>
                <Box display={"flex"} flexDirection={"column"}>
                  <Box display={"flex"}>
                    <Typography variant="body2">{`${cmt?.User?.last_name} ${cmt?.User?.first_name}`}</Typography>
                    <Typography ml={1} variant="caption">
                      {formatVietnamDate(cmt?.created_at)}
                    </Typography>
                  </Box>
                  <Typography variant="body2">{cmt?.content}</Typography>
                </Box>
              </Box>
            ))
          ) : reviews[0]?.ReviewComment?.length > 0 ? (
            <Box
              key={
                reviews[0]?.ReviewComment[reviews[0]?.ReviewComment?.length - 1]
                  .id
              }
              display={"flex"}
              alignItems={"center"}
              mb={1}
            >
              <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                {reviews[0]?.ReviewComment[
                  reviews[0]?.ReviewComment?.length - 1
                ]?.User?.first_name?.charAt(0)}
              </Avatar>
              <Box display={"flex"} flexDirection={"column"}>
                <Box display={"flex"}>
                  <Typography variant="body2">{`${
                    reviews[0]?.ReviewComment[
                      reviews[0]?.ReviewComment?.length - 1
                    ]?.User?.last_name
                  } ${
                    reviews[0]?.ReviewComment[
                      reviews[0]?.ReviewComment?.length - 1
                    ]?.User?.first_name
                  }`}</Typography>
                  <Typography ml={1} variant="caption">
                    {formatVietnamDate(
                      reviews[0]?.ReviewComment[
                        reviews[0]?.ReviewComment?.length - 1
                      ].created_at
                    )}
                  </Typography>
                </Box>
                <Typography variant="body2">
                  {
                    reviews[0]?.ReviewComment[
                      reviews[0]?.ReviewComment?.length - 1
                    ].content
                  }
                </Typography>
              </Box>
            </Box>
          ) : null}
          <Box display={"flex"} alignItems={"center"}>
            <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
              {user.firstName.charAt(0)}
            </Avatar>
            <TextField
              id="outlined-basic"
              label="Add a class comment"
              variant="outlined"
              size="small"
              value={newComment}
              onChange={(e) => setNewComment(e.target.value)}
              fullWidth
            />
            <IconButton
              disabled={creatingComment || newComment.length === 0}
              onClick={handleCreateComment}
            >
              {creatingComment ? <CircularProgress /> : <Send />}
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
