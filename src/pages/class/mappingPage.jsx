import React, { useState, useContext, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Grid,
  Avatar,
  TextField,
  Card,
  CardHeader,
  CardContent,
  Alert,
  AlertTitle,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";
import { Campaign, Send } from "@mui/icons-material";

import InforClass from "../../components/newsTabComponent/inforClass";
import TabBar from "../../components/tab/tabbar";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  PostsContext,
  UserContext,
} from "../../store/context";
import { authService, classService, gradeService } from "../../services";
import {
  authConst,
  classesConst,
  currentClassConst,
  severityConst,
  userConst,
} from "../../const";
import ButtonProgress from "../../components/button/buttonProgess";
import { convertTimestampToDate } from "../../utils/convertDateFormat";

function getCurrentClassInClassesById(classes, id) {
  let curClass = null;
  const indexTeaching = classes.teaching.findIndex(
    (item) => item.class_id === id
  );
  if (indexTeaching !== -1) {
    curClass = classes.teaching[indexTeaching].class_entity;
    curClass.role = classes.teaching[indexTeaching].role;
  }

  const indexEnrolled = classes.enrolled.findIndex(
    (item) => item.class_id === id
  );
  if (indexEnrolled !== -1) {
    curClass = classes.enrolled[indexEnrolled].class_entity;
    curClass.role = classes.enrolled[indexEnrolled].role;
  }
  return curClass;
}

const handleApiError = (err) => {
  enqueueSnackbar(err.response?.data.message || err.message, {
    variant: severityConst.ERROR,
  });
  if (err.response?.data.statusCode === 403) {
    navigate("/home");
  }
  if (err.response?.data.statusCode === 401) {
    authDispatch({
      type: authConst.LOG_OUT,
    });
    navigate("/login");
  }
};

function MappingPage() {
  const [currentClass, currentClassDispatch] = useContext(CurrentClassContext);
  const [authState, authDispatch] = useContext(AuthContext);
  const [classes] = useContext(ClassesContext);
  const { classId } = useParams();

  const navigate = useNavigate();
  const [user, userDispatch] = useContext(UserContext);
  const [studentIdState, setStudentIdState] = useState(user.studentId || "");
  const [saveStudentId, setSaveStudentId] = useState(false);

  const handleSaveStudentId = (e) => {
    e.preventDefault();
    const payload = {
      classId,
      studentId: studentIdState,
    };
    setSaveStudentId(true);
    gradeService
      .mapStudent(payload)
      .then((data) => {
        // console.log("data", data);
        enqueueSnackbar("Your Student ID had mapped!", {
          variant: severityConst.SUCCESS,
        });
        userDispatch({
          type: userConst.EDIT_PROFILE,
          payload: {
            studentId: studentIdState,
            firstName: user.first_name,
            lastName: user.last_name,
            email: user.email,
            username: user.username,
            phoneNumber: user.phone_number,
            dateOfBirth: user.birthdate,
          },
        });
      })
      .catch((err) => handleApiError(err))
      .finally(() => {
        setSaveStudentId(false);
      });
  };

  const handleChangeStudentId = (e) => {
    const { name, value, type } = e.target;
    let newValue;
    if (name === "studentId") {
      newValue = value.toString();
    }
    setStudentIdState(newValue);
  };
  // console.log("first", user);
  useEffect(() => {
    // setStudentIdState(user.studentId || "");
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
    }
  }, [classId, classes]);

  useEffect(() => {
    authService
      .getProfile()
      .then((data) => {
        // console.log("data", data);
        const payload = {
          studentId: data.student_id,
          firstName: data.first_name,
          lastName: data.last_name,
          email: data.email,
          dateOfBirth: data.birthdate
            ? convertTimestampToDate(data.birthdate)
            : null,
          username: data.username,
          phoneNumber: data.phone_number,
          isActivated: data.is_activated,
        };
        userDispatch({ type: userConst.EDIT_PROFILE, payload });
        setStudentIdState(data.student_id || "");
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);
  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "flex-end",
          width: { xs: "90%", sm: "90%", md: "90%", lg: "75%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            m: "0 auto",
          }}
        >
          {user.studentId === null ? (
            <Alert severity="warning">
              <AlertTitle>You don't have a student id yet</AlertTitle>
              If you don't map student id, you couldn't see your grade
            </Alert>
          ) : (
            <Alert severity="info">
              <AlertTitle>Your Student ID had mapped</AlertTitle>
              This Student ID can use for other class
            </Alert>
          )}

          <Box
            component="form"
            sx={{ mx: "auto" }}
            onSubmit={(e) => handleSaveStudentId(e)}
          >
            <Grid
              container
              justifyContent="center"
              alignItems="center"
              sx={{
                width: {
                  sm: "60%",
                  md: "60%",
                  lg: "50%",
                  xl: "50%",
                },
                mt: 6,
                mx: "auto",
              }}
            >
              {/* Card contain title and description of exam grade */}
              <Card sx={{ minWidth: 380 }}>
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="studentId"
                        label="Student ID"
                        name="studentId"
                        //  autoComplete="name"
                        value={studentIdState}
                        onChange={(e) => handleChangeStudentId(e)}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Grid>

            <ButtonProgress
              loading={saveStudentId}
              text={"Save"}
              disable={true}
              onClick={(e) => {
                handleSaveStudentId(e);
              }}
            />
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default MappingPage;
