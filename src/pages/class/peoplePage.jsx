import {
  Box,
  Grid,
  IconButton,
  Typography,
  Avatar,
  Divider,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Skeleton,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import { useEffect, useState, useContext } from "react";
import { enqueueSnackbar } from "notistack";
import { ReactMultiEmail, isEmail } from "react-multi-email";
import "react-multi-email/dist/style.css";

import {
  severityConst,
  classesConst,
  authConst,
  currentClassConst,
} from "../../const";
import { NormalDataTable } from "../../components/tab/dataTable";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
} from "../../store/context";
import { classService } from "../../services";
import TabBar from "../../components/tab/tabbar";

function PeoplePage() {
  const { classId } = useParams();
  const [teacherList, setTeacherList] = useState([]);
  const [studentList, setStudentList] = useState([]);
  const [emails, setEmails] = useState([]);
  const [isTeacher, setIsTeacher] = useState(false);
  const [isCreator, setIsCreator] = useState(false);
  const [loading, setLoading] = useState(false);
  const [currentClass, setCurrentClass] = useState(false);
  const [openInviteByMailForm, setOpenInviteByMailForm] = useState(false);
  const [isInviting, setIsInviting] = useState(false);
  const [isInviteTeacher, setIsInviteTeacher] = useState(false);
  const [isInviteStudent, setIsInviteStudent] = useState(false);
  const [classes] = useContext(ClassesContext);
  const [, authDispatch] = useContext(AuthContext);
  const [currentDetailClass, currentClassDispatch] =
    useContext(CurrentClassContext);
  const navigate = useNavigate();

  let suffixStudent;
  if (studentList?.length === 1) {
    suffixStudent = "student";
  } else {
    suffixStudent = "students";
  }

  function getCurrentClassInClassesById(classes, id) {
    let curClass = null;
    const indexTeaching = classes.teaching.findIndex(
      (item) => item.class_id === id
    );
    if (indexTeaching !== -1) {
      curClass = classes.teaching[indexTeaching].class_entity;
      curClass.role = classes.teaching[indexTeaching].role;
    }

    const indexEnrolled = classes.enrolled.findIndex(
      (item) => item.class_id === id
    );
    if (indexEnrolled !== -1) {
      curClass = classes.enrolled[indexEnrolled].class_entity;
      curClass.role = classes.enrolled[indexEnrolled].role;
    }
    return curClass;
  }

  const colorForHeader = "#1967d2";

  const handleCloseInviteByMailForm = (isInvite) => {
    if (isInvite) {
      setIsInviting(true);
      const data = {
        mail: emails,
        class_id: currentClass.id,
      };
      if (isInviteTeacher) {
        classService
          .inviteTeachersByMaiil(data)
          .then((data) => {
            enqueueSnackbar("Invite teachers by mail successfully", {
              variant: severityConst.SUCCESS,
            });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 403) {
              navigate("/home");
            }
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          });
      }
      if (isInviteStudent) {
        classService
          .inviteStudentsByMaiil(data)
          .then((data) => {
            enqueueSnackbar("Invite students by mail successfully", {
              variant: severityConst.SUCCESS,
            });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 403) {
              navigate("/home");
            }
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          });
      }
      setIsInviting(false);
    }

    setOpenInviteByMailForm(false);
    setEmails([]);
    setIsInviteTeacher(false);
    setIsInviteStudent(false);
  };

  useEffect(() => {
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (!curClass.is_activate) {
      enqueueSnackbar("Class is inactive", {
        variant: severityConst.ERROR,
      });
      navigate("/home");
      return;
    }
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
    }
  }, [classId, classes]);

  useEffect(() => {
    setLoading(true);
    classService
      .getClassInfo(classId)
      .then((data) => {
        setCurrentClass({
          ...data,
          backgroundImage:
            "url(https://www.gstatic.com/classroom/themes/img_learnlanguage.jpg)",
        });
        const curClass = classes.teaching.find(
          (item) => item.class_id === classId
        );

        if (curClass?.role === classesConst.ROLE_CREATOR) {
          setIsCreator(true);
          setIsTeacher(true);
        }
        if (curClass?.role === classesConst.ROLE_TEACHER) {
          setIsTeacher(true);
        }
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
    classService
      .getAllUserOfClass(classId)
      .then((data) => {
        const teacher = data
          .filter((item) => item.role === "teacher" || item.role === "creator")
          .map(({ user_entity, role, class_id }) => ({
            name: `${user_entity.last_name} ${user_entity.first_name}`,
            avatar: user_entity.image_url,
            role: role,
            user_id: user_entity.id,
            class_id: class_id,
          }));

        const student = data
          .filter((item) => item.role === "student")
          .map(({ user_entity, role, class_id }) => ({
            name: `${user_entity.last_name} ${user_entity.first_name}`,
            avatar: user_entity.image_url,
            role: role,
            user_id: user_entity.id,
            class_id: class_id,
          }));
        setTeacherList(teacher);
        setStudentList(student);
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      })
      .finally(() => setLoading(false));
  }, [classId, classes]);
  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: { xs: "95%", sm: "95%", md: "80%", lg: "70%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        {/* Teachers component */}
        <Grid container>
          {/* Header Teacher */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box
              sx={{ display: "flex", justifyContent: "space-between", mb: 1 }}
            >
              <Box sx={{ marginLeft: 2 }}>
                <Typography component="h2" variant="h5" color={colorForHeader}>
                  Teachers
                </Typography>
              </Box>
              {isTeacher && (
                <Box>
                  <IconButton
                    onClick={() => {
                      setOpenInviteByMailForm(true);
                      setIsInviteTeacher(true);
                    }}
                  >
                    <PersonAddAltIcon style={{ color: colorForHeader }} />
                  </IconButton>
                </Box>
              )}
            </Box>
            <Divider sx={{ borderColor: colorForHeader }} />
          </Grid>

          {/* Table teachers */}
          <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1 }}>
            {loading ? (
              <Skeleton />
            ) : (
              <NormalDataTable
                list={teacherList}
                setList={setTeacherList}
                morevert="false"
                isTeacher={isTeacher}
                isCreator={isCreator}
              />
            )}
          </Grid>
        </Grid>

        {/* Students component */}
        <Grid container>
          {/* Header Student */}
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box
              sx={{ display: "flex", justifyContent: "space-between", mb: 1 }}
            >
              <Box sx={{ marginLeft: 2 }}>
                <Typography component="h2" variant="h5" color={colorForHeader}>
                  Students
                </Typography>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {studentList.length >= 1 ? (
                  <Typography
                    variant="div"
                    color={colorForHeader}
                    sx={{ mr: 2 }}
                  >
                    {studentList.length} {suffixStudent}
                  </Typography>
                ) : null}
                {isTeacher && (
                  <IconButton
                    onClick={() => {
                      setOpenInviteByMailForm(true);
                      setIsInviteStudent(true);
                    }}
                  >
                    <PersonAddAltIcon style={{ color: colorForHeader }} />
                  </IconButton>
                )}
              </Box>
            </Box>
            <Divider sx={{ borderColor: colorForHeader }} />
          </Grid>
          {/* Table Students */}
          <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1 }}>
            {loading ? (
              <Skeleton />
            ) : (
              <NormalDataTable
                list={studentList}
                setList={setStudentList}
                morevert="true"
                isTeacher={isTeacher}
                isCreator={isCreator}
              />
            )}
            {/* {classStudentWaitConfirm && (
               <>
                 <Divider />
                 <NormalDataTable list={classStudentWaitConfirm} />
               </>
             )} */}
          </Grid>
        </Grid>
      </Box>
      {/* invite teacher */}
      <Dialog
        open={openInviteByMailForm}
        onClose={() => handleCloseInviteByMailForm(false)}
      >
        <DialogTitle>
          Invite {isInviteStudent ? " students" : " teachers"}
        </DialogTitle>
        <DialogContent
          sx={{
            width: {
              xs: "300px",
              md: "500px",
            },
            display: "flex",
            flexDirection: "column",
          }}
        >
          <DialogContentText>Multiple emails can be imported</DialogContentText>
          <ReactMultiEmail
            placeholder="Type a email"
            emails={emails}
            onChange={(_emails) => {
              setEmails(_emails);
            }}
            autoFocus={true}
            style={{
              flexGrow: 1,
            }}
            getLabel={(email, index, removeEmail) => {
              return (
                <div data-tag key={index}>
                  <div data-tag-item>{email}</div>
                  <span data-tag-handle onClick={() => removeEmail(index)}>
                    X
                  </span>
                </div>
              );
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => handleCloseInviteByMailForm(false)}
            disabled={isInviting ? true : false}
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleCloseInviteByMailForm(true)}
            disabled={emails.length > 0 ? (isInviting ? true : false) : true}
          >
            Invite
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}

export default PeoplePage;
