import React, { useState, useContext, useEffect, useMemo } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Switch,
  TextField,
  Button,
  IconButton,
  Tooltip,
} from "@mui/material";
import {
  DndContext,
  closestCenter,
  MouseSensor,
  PointerSensor,
  KeyboardSensor,
  TouchSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";
import {
  ContentCopy,
  Fullscreen as FullscreenIcon,
  UndoRounded,
} from "@mui/icons-material";
import { enqueueSnackbar } from "notistack";
import { v4 as uuidv4 } from "uuid";

import ButtonProgress from "../../components/button/buttonProgess";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  GradeStructureContext,
} from "../../store/context";
import { classService, gradeService } from "../../services";
import {
  authConst,
  classesConst,
  currentClassConst,
  severityConst,
} from "../../const";
import TabBar from "../../components/tab/tabbar";
import SortableItem from "../../components/sortable/sortableItem";
import ClassInfoDialog from "../../components/dialog/classInfoDialog";

function SettingPage() {
  const { classId } = useParams();
  const [, authDispatch] = useContext(AuthContext);
  const [, classesDispatch] = useContext(ClassesContext);
  const [currentClass, setCurrentClass] = useState();
  const [updateShowState, setUpdateShowState] = useState(false);
  const [updatingInforClass, setUpdatingInforClass] = useState(false);
  const [saveGradeStructure, setSaveGradeStructure] = useState(false);

  const [enableInviteCodes, setEnableInviteCodes] = useState(false);
  const [loadingEnableInviteCodes, setLoadingEnableInviteCodes] =
    useState(false);
  const [openDialog, setOpenDialog] = useState(false);

  const [gradeStructure, setGradeStructure] = useState();
  const [oldGradeStructure, setOldGradeStructure] = useState();
  const [deletedGrade, setDeletedGrade] = useState([]);

  const sensors = useSensors(
    useSensor(MouseSensor, {
      activationConstraint: {
        distance: 8,
      },
    }),
    useSensor(TouchSensor, {
      activationConstraint: {
        delay: 200,
        tolerance: 6,
      },
    }),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  const navigate = useNavigate();

  const handleDragEnd = (event) => {
    const { active, over } = event;
    // console.log("event: ", event);
    // console.log("ACTIVE: " + active.id);
    // console.log("OVER :" + over.id);
    if (over === null) {
      return;
    }
    if (active.id === over.id) {
      // console.log("test true");
      return;
    }
    setGradeStructure((gradeStructure) => {
      const oldIndex = gradeStructure.findIndex(
        (grade) => grade.id === active.id
      );

      const newIndex = gradeStructure.findIndex(
        (grade) => grade.id === over.id
      );
      return arrayMove(gradeStructure, oldIndex, newIndex);
    });
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    let newValue = type === "checkbox" ? checked : value;

    setCurrentClass((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleChangeNameInDragable = (e) => {
    const { name, value, type, checked } = e.target;
    setGradeStructure((prevGrades) => {
      // Find the item the change belongs to
      const item = prevGrades.find((item) => item.id === name);
      // console.log("item:", item);
      // Create updated item
      const updatedItem = {
        ...item,
        name: type === "checkbox" ? checked : value,
      };

      // Create updated grades array with updated item
      return prevGrades.map((item) => {
        if (item.id === name) {
          return updatedItem;
        }
        return item;
      });
    });
  };
  const handleChangeWeightInDragable = (e) => {
    let { name, value, type, checked } = e.target;
    if (value === "") {
      value = 0;
    }
    setGradeStructure((prevGrades) => {
      // Find the item the change belongs to
      const item = prevGrades.find((item) => item.id === name);
      // Create updated item
      const updatedItem = {
        ...item,
        weight: type === "checkbox" ? checked : parseInt(value),
      };

      // Create updated grades array with updated item
      return prevGrades.map((item) => {
        if (item.id === name) {
          return updatedItem;
        }
        return item;
      });
    });
  };

  const handleClickAddGradeStructure = () => {
    setGradeStructure((prevGrades) => {
      // Get index for new item
      const newId = uuidv4();
      // console.log("here:", prevGrades);
      const nextIndex = prevGrades.length + 1;

      // Create new item
      const newItem = {
        id: newId,
        name: "",
        order: nextIndex,
        weight: 100,
        class_id: classId,
        isDeleted: false,
      };

      // Add to existing grades
      return [...prevGrades, newItem];
    });
  };

  const handleClickSaveGradeStructure = (e) => {
    e.preventDefault();
    setSaveGradeStructure(true);
    //if have name is duplicated, notify have duplicated name

    const sortData = gradeStructure.map((grade, index) => ({
      ...grade,
      order: index + 1,
    }));

    // console.log("sort:", sortData);
    // console.log("oldgrade:", oldGradeStructure);
    // console.log("delete:", deletedGrade);
    const listGrade = [...sortData, ...deletedGrade];
    const payload = {
      classId,
      gradeCompositionList: listGrade,
    };
    // console.log("payload:", payload);
    //gradeService
    gradeService
      .updateGradeStructureListClass(payload)
      .then((data) => {
        let sortedData = [];
        if (data.length !== 0) {
          sortedData = [...data]
            .sort((a, b) => a.order - b.order)
            .map((grade) => ({
              ...grade,
              isDeleted: false,
            }));
        }
        enqueueSnackbar("Update grade structure successfully!", {
          variant: severityConst.SUCCESS,
        });
        setGradeStructure(sortedData);
        setOldGradeStructure(sortedData);
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      })
      .finally(() => {
        setSaveGradeStructure(false);
      });
  };

  const handleDeleteGrade = (indexRowDelete) => {
    // console.log(deletedGrade);
    //when delete grade category, add it to deleted grade
    const currentDeleteGrade = gradeStructure
      .filter((grade) => grade.order === indexRowDelete)
      .map((grade) => {
        //if it not in old grade, dont need to add in deleted grade
        const checkInOldGrade = oldGradeStructure.filter(
          (oldGrade) => oldGrade.id === grade.id
        );
        if (checkInOldGrade.length === 0) {
          return {};
        }
        return {
          ...grade,
          isDeleted: true,
        };
      });
    // console.log("delete element:", currentDeleteGrade[0].length);
    // console.log("delete grade:", currentDeleteGrade);

    if (Object.keys(currentDeleteGrade[0]).length !== 0) {
      let newData;
      if (Object.keys(deletedGrade).length === 0) {
        newData = [currentDeleteGrade[0]];
      } else {
        newData = [...deletedGrade, currentDeleteGrade[0]];
      }
      // console.log("new data:", newData);

      setDeletedGrade(newData);
    }

    setGradeStructure((prevGrades) => {
      return prevGrades
        .filter((grade) => grade.order !== indexRowDelete)
        .map((grade) => {
          if (grade.order > indexRowDelete) {
            return { ...grade, order: grade.order - 1 };
          }
          return grade;
        });
    });
  };

  const handleSubmitChangeInfo = (event) => {
    event.preventDefault();
    setUpdatingInforClass(true);
    const payload = currentClass;

    classService
      .updateClass(payload)
      .then((data) => {
        const dataClass = {
          ...data,
          class_id: data.id,
        };
        const { id, created_at, users, ...cClass } = dataClass;

        setCurrentClass(cClass);
        //edit successfully - alert
        enqueueSnackbar("Edit information class successfully!", {
          variant: severityConst.SUCCESS,
        });

        classService
          .getAllClassOfUser()
          .then((classesList) => {
            // console.log(data);
            classesDispatch({
              type: classesConst.SET_CLASSES,
              payload: classesList,
            });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          });
      })
      .catch((err) => {
        if (err.response?.data.statusCode === 401) {
          //edit failed - alert
          enqueueSnackbar(err.response.data.message + "! Sign in again!", {
            variant: severityConst.ERROR,
          });
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        } else if (typeof err.response?.data.message !== "string") {
          if (err.response?.data.statusCode === 400) {
            //edit failed - alert
            enqueueSnackbar("Input wrong type!", {
              variant: severityConst.ERROR,
            });
          } else {
            //edit failed - alert
            enqueueSnackbar(err.message, {
              variant: severityConst.ERROR,
            });
          }
        } else {
          enqueueSnackbar(err.response?.data.message, {
            variant: severityConst.ERROR,
          });
        }
      })
      .finally(() => {
        setUpdatingInforClass(false);
      });
  };

  const handleChangeShowGrade = (event) => {
    event.preventDefault();
    setUpdateShowState(true);
    const oldValue = currentClass.is_watchable_another_student_grade;
    setCurrentClass({
      ...currentClass,
      is_watchable_another_student_grade: !oldValue,
    });
    const payload = {
      is_watchable_another_student_grade: !oldValue,
      class_id: classId,
    };
    classService
      .updateClass(payload)
      .then((data) => {
        const dataClass = {
          ...data,
          class_id: data.id,
        };
        const { id, created_at, users, ...cClass } = dataClass;

        setCurrentClass(cClass);
        //edit successfully - alert
        enqueueSnackbar("Update show grade successfully!", {
          variant: severityConst.SUCCESS,
        });

        classService
          .getAllClassOfUser()
          .then((classesList) => {
            // console.log(data);
            classesDispatch({
              type: classesConst.SET_CLASSES,
              payload: classesList,
            });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            setCurrentClass({
              ...currentClass,
              is_watchable_another_student_grade: oldValue,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          });
      })
      .catch((err) => {
        if (err.response?.data.statusCode === 401) {
          //edit failed - alert
          enqueueSnackbar(err.response.data.message + "! Sign in again!", {
            variant: severityConst.ERROR,
          });
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        } else if (typeof err.response?.data.message !== "string") {
          if (err.response?.data.statusCode === 400) {
            //edit failed - alert
            enqueueSnackbar("Input wrong type!", {
              variant: severityConst.ERROR,
            });
          } else {
            //edit failed - alert
            enqueueSnackbar(err.message, {
              variant: severityConst.ERROR,
            });
          }
        } else {
          enqueueSnackbar(err.response?.data.message, {
            variant: severityConst.ERROR,
          });
        }
      })
      .finally(() => {
        setUpdateShowState(false);
      });
  };

  const handleChangeEnableInviteCodes = () => {
    setLoadingEnableInviteCodes(true);
    classService
      .toggleEnableInviteCodes(classId)
      .then((data) => {
        setCurrentClass((preClass) => {
          return { ...preClass, invite_code: data?.inviteCode };
        });
        if (data?.inviteCode === null) {
          setEnableInviteCodes(false);
        } else {
          setEnableInviteCodes(true);
        }
        classesDispatch({
          type: classesConst.UPDATE_INVITE_CODE,
          payload: {
            id: currentClass.class_id,
            code: data?.inviteCode,
          },
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      })
      .finally(() => {
        setLoadingEnableInviteCodes(false);
      });
  };

  const handleCopyLinkInvite = () => {
    navigator.clipboard
      .writeText(
        `${window.location.origin}/invitation/by-link?code=${currentClass.invite_code}`
      )
      .then(() => {
        setOpenDialog(false);
        enqueueSnackbar("Link copied");
      })
      .catch((error) => {});
  };

  const [, currentClassDispatch] = useContext(CurrentClassContext);
  const [classes] = useContext(ClassesContext);
  function getCurrentClassInClassesById(classes, id) {
    let curClass = null;
    const indexTeaching = classes.teaching.findIndex(
      (item) => item.class_id === id
    );
    if (indexTeaching !== -1) {
      curClass = classes.teaching[indexTeaching].class_entity;
      curClass.role = classes.teaching[indexTeaching].role;
    }

    const indexEnrolled = classes.enrolled.findIndex(
      (item) => item.class_id === id
    );
    if (indexEnrolled !== -1) {
      curClass = classes.enrolled[indexEnrolled].class_entity;
      curClass.role = classes.enrolled[indexEnrolled].role;
    }
    return curClass;
  }

  useEffect(() => {
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
    }
  }, [classId, classes]);

  useEffect(() => {
    classService
      .getClassInfo(classId)
      .then((data) => {
        if (data?.is_activate === false) {
          enqueueSnackbar("Class is inactive", {
            variant: severityConst.ERROR,
          });
          navigate("/home");
          return;
        }
        const dataClass = {
          ...data,
          class_id: data.id,
        };
        const { id, created_at, users, ...cClass } = dataClass;
        if (data.invite_code === null) {
          setEnableInviteCodes(false);
        } else {
          setEnableInviteCodes(true);
        }
        setCurrentClass(cClass);
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });

    gradeService
      .getGradeStructureClass(classId)
      .then((data) => {
        // console.log("data grade:", data);
        let sortedData = [];
        if (data.length !== 0) {
          sortedData = [...data]
            .sort((a, b) => a.order - b.order)
            .map((grade) => ({
              ...grade,
              isDeleted: false,
            }));
        }
        setGradeStructure(sortedData);
        setOldGradeStructure(sortedData);
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, [classId]);

  return (
    <Box sx={{}}>
      <TabBar />
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Grid
          container
          spacing={2}
          sx={{
            justifyContent: "center",
            justifySelf: "center",
            width: {
              sm: "100%",
              md: "70%",
              lg: "60%",
              xl: "55%",
            },
            mt: 6,
          }}
        >
          <Grid item xs={12}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",
                padding: "10px",
                height: "100%",
              }}
            >
              <CardHeader title="Class Details" />
              <CardContent>
                <Box
                  component="form"
                  onSubmit={(e) => handleSubmitChangeInfo(e)}
                >
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <TextField
                        autoFocus
                        required
                        fullWidth
                        id="Class name"
                        label="Class name (required)"
                        name="name"
                        autoComplete="name"
                        value={currentClass?.name ? currentClass.name : ""}
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        autoComplete="given-name"
                        name="description"
                        fullWidth
                        id="class description"
                        label="Class description"
                        defaultValue={currentClass?.description}
                        value={
                          currentClass?.description
                            ? currentClass.description
                            : ""
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="section"
                        label="Section"
                        name="section"
                        defaultValue={currentClass?.section}
                        autoComplete="family-name"
                        value={
                          currentClass?.section ? currentClass.section : ""
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="room"
                        label="Room"
                        name="room"
                        defaultValue={currentClass?.room}
                        autoComplete="family-name"
                        value={currentClass?.room ? currentClass.room : ""}
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="subject"
                        label="Subject"
                        name="subject"
                        defaultValue={currentClass?.subject}
                        autoComplete="family-name"
                        value={currentClass ? currentClass.subject : ""}
                        onChange={(e) => handleChange(e)}
                      />
                    </Grid>
                  </Grid>
                  <ButtonProgress
                    loading={updatingInforClass}
                    text={"Update Info"}
                    onClick={(e) => {
                      handleSubmitChangeInfo(e);
                    }}
                  />
                </Box>
              </CardContent>
            </Card>
          </Grid>

          {/* Update invite codes */}
          <Grid item xs={12}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",
                padding: "10px",
                height: "100%",
              }}
            >
              <CardHeader title="General" />
              <CardContent sx={{ width: "100%" }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <Box>
                    <Typography
                      variant="body1"
                      sx={{
                        opacity: loadingEnableInviteCodes ? 0.5 : 1,
                      }}
                    >
                      Enable invite codes
                    </Typography>
                  </Box>
                  <Switch
                    name="enable_invite_codes"
                    checked={enableInviteCodes}
                    disabled={loadingEnableInviteCodes}
                    onChange={(e) => {
                      handleChangeEnableInviteCodes();
                    }}
                  />
                </Box>
                {enableInviteCodes && (
                  <>
                    <Box display={"flex"} alignItems={"center"}>
                      <Typography variant="body1" flexGrow={1}>
                        Invite link
                      </Typography>
                      <Box>
                        <Typography variant="caption" sx={{ opacity: 0.7 }}>
                          {`${window.location.origin}/invitation/by-link?code=${currentClass.invite_code}`}
                        </Typography>
                        <Tooltip title={"Copy invite link"}>
                          <IconButton onClick={handleCopyLinkInvite}>
                            <ContentCopy />
                          </IconButton>
                        </Tooltip>
                      </Box>
                    </Box>
                    <Box display={"flex"} alignItems={"center"}>
                      <Typography variant="body1" flexGrow={1}>
                        Class code
                      </Typography>
                      <Typography variant="body2" p={2} sx={{ opacity: 0.7 }}>
                        {currentClass.invite_code}
                      </Typography>
                    </Box>
                    <Box display={"flex"} alignItems={"center"}>
                      <Typography variant="body1" flexGrow={1}>
                        Class view
                      </Typography>
                      <Button onClick={() => setOpenDialog(true)}>
                        <Box>
                          <Typography variant="caption" p>
                            Display class code
                          </Typography>
                        </Box>
                        <FullscreenIcon />
                      </Button>
                    </Box>
                  </>
                )}
              </CardContent>
            </Card>
          </Grid>

          {/* Update Grade */}
          {/* <Grid item xs={12}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",
                padding: "10px",
                height: "100%",
              }}
            >
              <CardHeader title="Grade view" />
              <CardContent sx={{ width: "100%" }}>
                <Box
                  component="form"
                  onSubmit={(e) => handleChangeShowGrade(e)}
                >
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      width: "100%",
                    }}
                  >
                    <Box>
                      <Typography
                        variant="body1"
                        sx={{
                          opacity: updateShowState ? 0.5 : 1,
                        }}
                      >
                        Show overall grade to students
                      </Typography>
                    </Box>
                    <Box>
                      <Switch
                        name="is_watchable_another_student_grade"
                        checked={
                          currentClass
                            ? currentClass.is_watchable_another_student_grade
                            : false
                        }
                        onChange={(e) => {
                          handleChange(e);
                          handleChangeShowGrade(e);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
              </CardContent>
            </Card>
          </Grid> */}

          {/* Grade structure */}
          <Grid item xs={12}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",
                padding: "10px",
                height: "100%",
              }}
            >
              <CardHeader title="Grade structure" />
              <CardContent sx={{ width: "100%" }}>
                <DndContext
                  collisionDetection={closestCenter}
                  onDragEnd={handleDragEnd}
                  sensors={sensors}
                >
                  <Box
                    component="form"
                    onSubmit={(e) => handleClickSaveGradeStructure(e)}
                  >
                    {gradeStructure && (
                      <SortableContext
                        items={gradeStructure}
                        strategy={verticalListSortingStrategy}
                      >
                        {gradeStructure.map((grade) => (
                          <SortableItem
                            key={grade.id}
                            id={grade}
                            handleChangeName={handleChangeNameInDragable}
                            handleChangeWeight={handleChangeWeightInDragable}
                            onDelete={() => handleDeleteGrade(grade.order)}
                          />
                        ))}
                      </SortableContext>
                    )}
                    <CardContent sx={{ width: "100%", pt: 0 }}>
                      <Button onClick={handleClickAddGradeStructure}>
                        Add category
                      </Button>
                    </CardContent>

                    <ButtonProgress
                      loading={saveGradeStructure}
                      text={"Save grade structure"}
                      onClick={(e) => {
                        handleClickSaveGradeStructure(e);
                      }}
                    />
                  </Box>
                </DndContext>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Box>
      <ClassInfoDialog
        openDialog={openDialog}
        setOpenDialog={setOpenDialog}
        classInfor={currentClass}
      />
    </Box>
  );
}

export default SettingPage;
