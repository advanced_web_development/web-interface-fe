import React, { useState, useContext, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Grid,
  Avatar,
  TextField,
  FormControl,
  InputLabel,
  InputAdornment,
  Input,
  OutlinedInput,
  IconButton,
  CircularProgress,
  Typography,
  Skeleton,
  Button,
  Collapse,
  ListItemButton,
  ListItemText,
  Divider,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";
import { Campaign, Send, ExpandMore, ExpandLess } from "@mui/icons-material";

import InforClass from "../../components/newsTabComponent/inforClass";
import TabBar from "../../components/tab/tabbar";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  PostsContext,
  ReviewsContext,
  UserContext,
} from "../../store/context";
import { classService, gradeService } from "../../services";
import {
  authConst,
  classesConst,
  currentClassConst,
  severityConst,
} from "../../const";
import InfiniteScroll from "react-infinite-scroll-component";
import ClassPost from "../../components/post/classPost";
import { socket } from "../../socket";
import { useTheme } from "@emotion/react";
import ReviewPost from "../../components/post/reviewPost";

function getCurrentClassInClassesById(classes, id) {
  let curClass = null;
  const indexTeaching = classes.teaching.findIndex(
    (item) => item.class_id === id
  );
  if (indexTeaching !== -1) {
    curClass = classes.teaching[indexTeaching].class_entity;
    curClass.role = classes.teaching[indexTeaching].role;
  }

  const indexEnrolled = classes.enrolled.findIndex(
    (item) => item.class_id === id
  );
  if (indexEnrolled !== -1) {
    curClass = classes.enrolled[indexEnrolled].class_entity;
    curClass.role = classes.enrolled[indexEnrolled].role;
  }
  return curClass;
}

export default function GradeReviewsPage() {
  const [currentClass, currentClassDispatch] = useContext(CurrentClassContext);
  const [authState, authDispatch] = useContext(AuthContext);
  const [classes] = useContext(ClassesContext);
  const { classId } = useParams();
  const navigate = useNavigate();
  const [loadingReviews, setLoadingReviews] = useState(false);
  const [user] = useContext(UserContext);
  const [reviews, reviewsDispatch] = useContext(ReviewsContext);
  const [gradeCompositions, setGradeCompositions] = useState([]);
  const theme = useTheme();

  const handleCollapseGradeComposition = (index) => {
    const compositions = gradeCompositions.map((x, i) => {
      if (i === index) {
        let composition = x;
        composition.open = !x.open;
        return composition;
      }
      return x;
    });
    setGradeCompositions(compositions);
  };

  useEffect(() => {
    reviewsDispatch({
      type: classesConst.SET_REVIEWS,
      payload: [],
    });
    const curClass = getCurrentClassInClassesById(classes, classId);
    if (curClass && !curClass.is_activate) {
      enqueueSnackbar("Class is inactive", {
        variant: severityConst.ERROR,
      });
      navigate("/home");
      return;
    }
    if (
      curClass === null &&
      classes?.teaching?.length !== 0 &&
      classes?.enrolled?.length !== 0
    ) {
      navigate("/home");
    } else {
      currentClassDispatch({
        type: currentClassConst.SET_CURRENT_CLASS,
        payload: curClass,
      });
      setLoadingReviews(true);
      if (!classId) {
        return;
      }
      if (currentClass.role !== classesConst.ROLE_STUDENT) {
        gradeService
          .getAllClassGradeReviews(classId)
          .then((data) => {
            if (data?.length > 0) {
              reviewsDispatch({
                type: classesConst.SET_REVIEWS,
                payload: data,
              });
            } else {
              reviewsDispatch({
                type: classesConst.SET_REVIEWS,
                payload: [],
              });
            }

            gradeService
              .getGradeStructureClass(classId)
              .then((data) => {
                if (data?.length > 0) {
                  const gradeStructure = data.map((x) => {
                    return { name: x.name, open: true };
                  });
                  setGradeCompositions(gradeStructure);
                }
              })
              .catch((err) => {
                //axios failed - alert
                enqueueSnackbar(err.response?.data.message || err.message, {
                  variant: severityConst.ERROR,
                });
                if (err.response?.data.statusCode === 401) {
                  authDispatch({
                    type: authConst.LOG_OUT,
                  });
                  navigate("/login");
                }
              })
              .finally(() => {
                setLoadingReviews(false);
              });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
            setLoadingReviews(false);
          });
      } else {
        gradeService
          .getGradeReviewsByClassAndUser(classId)
          .then((data) => {
            if (data?.length > 0) {
              reviewsDispatch({
                type: classesConst.SET_REVIEWS,
                payload: data,
              });
            } else {
              reviewsDispatch({
                type: classesConst.SET_REVIEWS,
                payload: [],
              });
            }

            gradeService
              .getGradeStructureClass(classId)
              .then((data) => {
                if (data?.length > 0) {
                  const gradeStructure = data.map((x) => {
                    return { name: x.name, open: true };
                  });
                  setGradeCompositions(gradeStructure);
                }
              })
              .catch((err) => {
                //axios failed - alert
                enqueueSnackbar(err.response?.data.message || err.message, {
                  variant: severityConst.ERROR,
                });
                if (err.response?.data.statusCode === 401) {
                  authDispatch({
                    type: authConst.LOG_OUT,
                  });
                  navigate("/login");
                }
              })
              .finally(() => {
                setLoadingReviews(false);
              });
          })
          .catch((err) => {
            //axios failed - alert
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
            setLoadingReviews(false);
          });
      }
    }
  }, [classId, classes]);

  function onReceiveComment(payload, postList) {
    if (payload?.comment && payload?.type) {
      if (payload.type === "exam-review") {
        const postIdToUpdate = payload.comment.exam_review_id;
        // Create a new array with the updated posts
        const updatedPosts = postList.map((post) => {
          if (post.id === postIdToUpdate) {
            return {
              ...post,
              ReviewComment: [...post.ReviewComment, payload.comment],
            };
          }
          return post; // Keep other posts unchanged
        });
        reviewsDispatch({
          type: classesConst.SET_REVIEWS,
          payload: updatedPosts,
        });
      }
    }
  }
  useEffect(() => {
    function setupSocketEvent() {
      socket.on("post_comment_noti", (msg) => onReceiveComment(msg, reviews));
      if (socket.connected) {
        // socket.emit("associateUser", user.id || -1);
      } else {
        setTimeout(() => {
          setupSocketEvent(); // Re-setup event listeners
        }, 1000); // Retry after 3 seconds (adjust as needed)
      }
    }
    if (authState.user.id !== -1) {
      setupSocketEvent();
    }
    return () => {
      socket.off("post_comment_noti", (msg) => onReceiveComment(msg, reviews));
    };
  }, [reviews]);

  return (
    <Box sx={{ position: "relative" }}>
      <TabBar />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "flex-end",
          width: { xs: "90%", sm: "90%", md: "90%", lg: "75%" },
          pt: 4,
          margin: "2rem auto 0",
          flexDirection: "column",
        }}
      >
        {/* content */}
        <Grid container>
          <Grid item xs={12} display={"flex"}>
            <Box flexGrow={1}>
              {loadingReviews ? (
                <>
                  <Skeleton />
                </>
              ) : (
                gradeCompositions.map((g, index) => {
                  let requests = reviews.map((r) => {
                    if (
                      r.ExamStudent?.Exam?.GradeComposition?.name === g.name
                    ) {
                      return (
                        <ReviewPost
                          reviewPost={r}
                          key={r.id}
                          isTeacher={
                            currentClass.role !== classesConst.ROLE_STUDENT
                              ? true
                              : false
                          }
                        />
                      );
                    }
                    return null;
                  });
                  let reviewRequests = requests.filter((r) => r !== null);
                  return (
                    <Box
                      key={g.name}
                      sx={{ display: "flex", flexDirection: "column", mb: 3 }}
                    >
                      <ListItemButton
                        onClick={() => {
                          handleCollapseGradeComposition(index);
                        }}
                      >
                        <ListItemText
                          children={
                            <Typography variant="h5">{g.name}</Typography>
                          }
                        />
                        {g.open ? <ExpandLess /> : <ExpandMore />}
                      </ListItemButton>
                      <Divider
                        sx={{
                          borderBottomColor:
                            theme.palette.mode === "dark" ? "white" : "black",
                        }}
                      />
                      <Collapse in={g.open} unmountOnExit>
                        {reviewRequests.length > 0 ? (
                          reviewRequests
                        ) : (
                          <Typography variant="caption">
                            Do not have any grade review requests.
                          </Typography>
                        )}
                      </Collapse>
                    </Box>
                  );
                })
              )}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
