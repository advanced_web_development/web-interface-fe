import { Box, Card, CardContent, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { enqueueSnackbar } from "notistack";

import ButtonProgress from "../../components/button/buttonProgess";
import { classService } from "../../services";
import { AuthContext, UserContext, ClassesContext } from "../../store/context";
import { severityConst, authConst, classesConst } from "../../const";
import { useLocation, useNavigate } from "react-router-dom";

export default function LinkInvitationPage() {
  const [loading, setLoading] = useState(false);
  const [user] = useContext(UserContext);
  const [, authDispatch] = useContext(AuthContext);
  const [, classesDispatch] = useContext(ClassesContext);
  const navigate = useNavigate();
  const location = useLocation();
  const params = new URLSearchParams(location.search);

  const handleAcceptInvitation = (event) => {
    event.preventDefault();

    setLoading(true);

    const payload = {
      code: params.get("code") || " ",
      user_id: user.id,
    };

    classService
      .addStudent(payload)
      .then((data) => {
        //redirect class url
        enqueueSnackbar("Join class successfully!", {
          variant: severityConst.SUCCESS,
        });
        classesDispatch({
          type: classesConst.ADD_CLASS,
          payload: data,
        });
        navigate(`/class/${data.class_id}`);
      })
      .catch((err) => {
        //user in class
        if (err.response?.data.statusCode === 409) {
          //redirect class url
          enqueueSnackbar("User already exists in class", {
            variant: severityConst.ERROR,
          });
          navigate(`/class/${err.response?.data.message}`);
        } else {
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        }
      });

    setLoading(false);
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center", paddingTop: 3 }}>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          padding: "30px",
          height: "100%",
        }}
      >
        <CardContent>
          <Typography
            sx={{ p: 0 }}
            variant="h5"
            component="div"
            textAlign="center"
          >
            You are participating in the class as a student.
          </Typography>
          <Box
            component="form"
            onSubmit={(e) => handleAcceptInvitation(e)}
            sx={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <ButtonProgress loading={loading} text="Participate the class" />
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
}
