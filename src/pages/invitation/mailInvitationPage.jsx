import { Box, Card, CardContent, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { enqueueSnackbar } from "notistack";

import ButtonProgress from "../../components/button/buttonProgess";
import { classService } from "../../services";
import { UserContext, AuthContext, ClassesContext } from "../../store/context";
import { severityConst, authConst, classesConst } from "../../const";
import { useLocation, useNavigate, useParams } from "react-router-dom";

export default function MailInvitationPage() {
  const [loading, setLoading] = useState(false);
  const [role, setRole] = useState("");
  const [user] = useContext(UserContext);
  const [, authDispatch] = useContext(AuthContext);
  const [, classesDispatch] = useContext(ClassesContext);
  const navigate = useNavigate();
  const location = useLocation();
  const params = new URLSearchParams(location.search);

  const handleAcceptInvitation = (event) => {
    event.preventDefault();

    setLoading(true);
    //send request and redirect
    const data = {
      token: params.get("token") || " ",
    };
    if (role === classesConst.ROLE_TEACHER) {
      classService
        .addTeacherByToken(data)
        .then(() => {
          enqueueSnackbar("Join class with teacher role successfully", {
            variant: severityConst.SUCCESS,
          });
          classService
            .getAllClassOfUser()
            .then((data) => {
              // console.log(data);
              classesDispatch({
                type: classesConst.SET_CLASSES,
                payload: data,
              });
              navigate(`/class/${data[0].class_id}`);
            })
            .catch((err) => {
              //axios failed - alert
              enqueueSnackbar(err.response?.data.message || err.message, {
                variant: severityConst.ERROR,
              });
              if (err.response?.data.statusCode === 401) {
                authDispatch({
                  type: authConst.LOG_OUT,
                });
                navigate("/login");
              }
            });
        })
        .catch((err) => {
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    }
    if (role === classesConst.ROLE_STUDENT) {
      classService
        .addStudentByToken(data)
        .then(() => {
          enqueueSnackbar("Join class with student role successfully", {
            variant: severityConst.SUCCESS,
          });

          classService
            .getAllClassOfUser()
            .then((data) => {
              // console.log(data);
              classesDispatch({
                type: classesConst.SET_CLASSES,
                payload: data,
              });
              navigate(`/class/${data[0].class_id}`);
            })
            .catch((err) => {
              //axios failed - alert
              enqueueSnackbar(err.response?.data.message || err.message, {
                variant: severityConst.ERROR,
              });
              if (err.response?.data.statusCode === 401) {
                authDispatch({
                  type: authConst.LOG_OUT,
                });
                navigate("/login");
              }
            });
        })
        .catch((err) => {
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    }

    setLoading(false);
  };

  useEffect(() => {
    setRole(params.get("role") || " ");
    classService
      .getClassInfo(params.get("class_id") || " ")
      .then((data) => {
        navigate(`/class/${params.get("class_id")}`);
      })
      .catch((err) => {
        if (err.response?.data.statusCode === 401) {
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);
  return (
    <Box sx={{ display: "flex", justifyContent: "center", paddingTop: 3 }}>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          padding: "30px",
          height: "100%",
        }}
      >
        <CardContent>
          <Typography
            sx={{ p: 0 }}
            variant="h5"
            component="div"
            textAlign="center"
          >
            You are participating in the class as a{" "}
            {params.get("role") ? params.get("role") : "student"}.
          </Typography>
          <Box
            component="form"
            onSubmit={(e) => handleAcceptInvitation(e)}
            sx={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <ButtonProgress loading={loading} text="Participate the class" />
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
}
