import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  Grid,
  TextField,
  InputAdornment,
  IconButton,
  Box,
  Skeleton,
} from "@mui/material";
import { useContext } from "react";

import { UserContext } from "../../store/context";
import UserAvatar from "../../assets/user.png";
import UpdateProfileForm from "../../components/form/updateProfileForm";
import UpdatePasswordForm from "../../components/form/updatePasswordForm";
import UpdateAccountForm from "../../components/form/updateAccountForm";

export default function EditUserProfilePage() {
  const [user] = useContext(UserContext);
  return (
    <>
      <Typography align="center" p={3} variant="h3">
        Edit Information
      </Typography>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Grid
          container
          spacing={4}
          sx={{
            alignItems: "stretch",
            justifyContent: "center",
            width: {
              sm: "100%",
              md: "90%",
              xl: "70%",
            },
          }}
        >
          <Grid item xs={12} lg={4}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                padding: "30px",
                height: "100%",
              }}
            >
              <CardMedia
                component="img"
                sx={{
                  maxHeight: "230px",
                  width: "auto",
                }}
                alt={user.lastName?.charAt(0) || "Null"}
                image={UserAvatar}
              />
              <CardContent>
                <Typography
                  sx={{ p: 0 }}
                  variant="h5"
                  component="div"
                  textAlign="center"
                >
                  {user.lastName + " " + user.firstName}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} lg={8}>
            <Grid container spacing={4}>
              <Grid item xs={12} lg={12}>
                <Card
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "30px",
                    height: "100%",
                  }}
                >
                  <UpdateProfileForm />
                </Card>
              </Grid>
              <Grid item xs={12} lg={12}>
                <Card
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "30px",
                    height: "100%",
                  }}
                >
                  {user.userName ? (
                    <UpdatePasswordForm />
                  ) : (
                    <UpdateAccountForm />
                  )}
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
