import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  Grid,
  Box,
  Skeleton,
  CardActionArea,
  Avatar,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { AuthContext, ClassesContext } from "../../store/context";
import { useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import UserAvatar from "../../assets/user.png";
import { authService } from "../../services";
import { authConst, severityConst } from "../../const";
import { convertTimestampToDate } from "../../utils/convertDateFormat";
import ClassCard from "../../components/card/classCard";

export default function UserHome() {
  const [account] = useContext(AuthContext);
  //const [user, userDispatch] = useContext(UserContext);
  const [user, setUser] = useState();
  const [, authDispatch] = useContext(AuthContext);
  const [classesState] = useContext(ClassesContext);
  const [classes, setClasses] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const newClasses = (classesState.enrolled || []).concat(
      classesState.teaching || []
    );
    const sortedDescClasses = newClasses.sort((a, b) => {
      const dateA = new Date(a.created_at);
      const dateB = new Date(b.created_at);
      // Compare the dates in descending order
      return dateB - dateA;
    });
    setClasses(sortedDescClasses);
  }, [classesState]);

  useEffect(() => {
    authService
      .getProfile()
      .then((data) => {
        setUser({
          id: data.student_id,
          firstName: data.first_name,
          lastName: data.last_name,
          dateOfBirth: data.birthdate
            ? convertTimestampToDate(data.birthdate)
            : null,
          email: data.email,
          username: data.username,
          phoneNumber: data.phone_number,
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);

  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "center", paddingTop: 3 }}>
        <Grid
          container
          spacing={4}
          sx={{
            alignItems: "stretch",
            justifyContent: "center",
            width: {
              sm: "100%",
              md: "90%",
              xl: "70%",
            },
          }}
        >
          <Grid item xs={12} md={4} xl={4}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                padding: "30px",
                height: "100%",
              }}
            >
              <CardMedia
                component="img"
                sx={{
                  maxHeight: "150px",
                  width: "auto",
                }}
                alt={user ? user.lastName.charAt(0) : ""}
                image={UserAvatar}
              />
              <CardContent>
                <Typography
                  sx={{ p: 0 }}
                  variant="h5"
                  component="div"
                  textAlign="center"
                >
                  {user ? user.lastName + " " + user.firstName : <Skeleton />}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} md={8} xl={8}>
            <Card
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                padding: "30px",
                height: "100%",
                paddingBottom: "0px !important",
              }}
            >
              <Grid marginLeft="0px" container spacing={4}>
                <Grid
                  item
                  xs={4}
                  lg={4}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography variant="div">Student ID</Typography>
                </Grid>
                <Grid
                  item
                  xs={8}
                  lg={8}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography>
                    {user ? (
                      user.id || (
                        <Typography
                          variant="caption"
                          sx={{
                            color: "gray",
                          }}
                        >
                          Unmapped
                        </Typography>
                      )
                    ) : (
                      <Skeleton />
                    )}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={4}
                  lg={4}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false}>First name</Typography>
                </Grid>
                <Grid
                  item
                  xs={8}
                  lg={8}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false}>
                    {user ? (
                      user.firstName || (
                        <Typography
                          variant="caption"
                          sx={{
                            color: "gray",
                          }}
                        >
                          Unknown
                        </Typography>
                      )
                    ) : (
                      <Skeleton />
                    )}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={4}
                  lg={4}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false}>Last name</Typography>
                </Grid>
                <Grid
                  item
                  xs={8}
                  lg={8}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false}>
                    {user ? (
                      user.lastName || (
                        <Typography
                          variant="caption"
                          sx={{
                            color: "gray",
                          }}
                        >
                          Unknown
                        </Typography>
                      )
                    ) : (
                      <Skeleton />
                    )}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={4}
                  lg={4}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography>Email</Typography>
                </Grid>
                <Grid
                  item
                  xs={8}
                  lg={8}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false} sx={{ overflow: "auto" }}>
                    {user ? user.email : <Skeleton />}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={4}
                  lg={4}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography>Phone number</Typography>
                </Grid>
                <Grid
                  item
                  xs={8}
                  lg={8}
                  sx={{
                    borderBottom: "1px solid #bdbdbd",
                    paddingY: "10px !important",
                  }}
                >
                  <Typography noWrap={false} sx={{ overflow: "auto" }}>
                    {user ? (
                      user.phoneNumber || (
                        <Typography
                          variant="caption"
                          sx={{
                            color: "gray",
                          }}
                        >
                          Unknown
                        </Typography>
                      )
                    ) : (
                      <Skeleton />
                    )}
                  </Typography>
                </Grid>
                <Grid item xs={4} lg={4} sx={{ paddingY: "10px !important" }}>
                  <Typography noWrap={false}>Date of birth</Typography>
                </Grid>
                <Grid item xs={8} lg={8} sx={{ paddingY: "10px !important" }}>
                  <Typography noWrap={false}>
                    {user ? (
                      user.dateOfBirth || (
                        <Typography
                          variant="caption"
                          sx={{
                            color: "gray",
                          }}
                        >
                          Unknown
                        </Typography>
                      )
                    ) : (
                      <Skeleton />
                    )}
                  </Typography>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </Box>
      <Box sx={{ display: "flex", justifyContent: "center", paddingTop: 3 }}>
        <Grid
          container
          spacing={4}
          sx={{
            alignItems: "stretch",
            justifyContent: "start",
            width: {
              sm: "100%",
              md: "95%",
            },
          }}
        >
          {classes.map((classItem) => (
            <Grid key={classItem.class_id} item xs={12} sm={6} md={4} lg={3}>
              <ClassCard classItem={classItem} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </>
  );
}
