import { Typography, Button, Box } from "@mui/material";
import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { authService } from "../../services";
import { enqueueSnackbar } from "notistack";
import { AuthContext } from "../../store/context";
import { authConst, severityConst } from "../../const";

export default function ActivatePage() {
  const [counter, setCounter] = useState(0);

  const [user, setUser] = useState();
  const [, authDispatch] = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (counter > 0) {
      const timer = setTimeout(() => setCounter(counter - 1), 1000);
      return () => clearTimeout(timer);
    }
  }, [counter]);
  useEffect(() => {
    authService
      .getProfile()
      .then((data) => {
        setUser({
          email: data.email,
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);

  function handleGoToMailBox() {
    window.open("https://mail.google.com/mail/", "_blank");
  }

  function handleReSendClick() {
    setCounter(60);
    authService
      .postReSendEmail()
      .then(() => {
        enqueueSnackbar("Resend Verify To Your Email Successfully!", {
          variant: severityConst.SUCCESS,
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }

  return (
    <Box
      sx={{
        mt: 8,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          width: "100%",
          maxWidth: 500,
          borderRadius: 2,
          p: 4,
          boxShadow: 10,
        }}
      >
        <Box>
          <Typography
            variant="h5"
            component="h1"
            sx={{
              pt: 0.5,
              textAlign: "center",
              mb: 4,
            }}
          >
            Verify your email to continue
          </Typography>
        </Box>

        <Box>
          <Typography>We have sent an email to the adress:</Typography>
          <Box
            component={"span"}
            sx={{ fontStyle: "oblique", fontWeight: "light" }}
          >
            {user?.email || null}
          </Box>

          <Typography
            sx={{
              mt: 2,
            }}
          >
            Please check your email and select the link provided to verify your
            address.
          </Typography>

          <Box
            sx={{
              m: 1,
              position: "relative",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 2, maxWidth: "300px" }}
              onClick={handleReSendClick}
              disabled={counter !== 0}
            >
              {counter === 0
                ? "Resend Verifycation Email"
                : `Wait for: ${counter}`}
            </Button>
          </Box>
          <Box
            sx={{
              m: 1,
              position: "relative",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 2, maxWidth: "300px" }}
              onClick={handleGoToMailBox}
            >
              Go to mail box
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
