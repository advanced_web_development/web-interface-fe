import { Typography, Box } from "@mui/material";
import React, { useEffect, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import { AuthContext, UserContext } from "../../store/context";
import { authService } from "../../services";
import { authConst, severityConst, userConst } from "../../const";
import { convertTimestampToDate } from "../../utils/convertDateFormat";

export default function VerifyPage() {
  const [auth, authDispatch] = useContext(AuthContext);
  const [user, userDispatch] = useContext(UserContext);
  const navigate = useNavigate();
  const { token } = useParams();

  useEffect(() => {
    authService
      .putTokenConfirm({ token: token })
      .then((data) => {
        enqueueSnackbar("Your account is verified successfully!", {
          variant: severityConst.SUCCESS,
        });
        authService
          .getProfile()
          .then((userInfo) => {
            authDispatch({
              type: authConst.LOGIN,
              payload: {
                accessToken: auth.accessToken,
                refreshToken: auth.accessToken,
                user: {
                  id: userInfo.id,
                  firstName: userInfo.first_name,
                  lastName: userInfo.last_name,
                  dateOfBirth: userInfo.birthdate
                    ? convertTimestampToDate(userInfo.birthdate)
                    : null,
                  email: userInfo.email,
                  username: userInfo.username,
                  phoneNumber: userInfo.phone_number,
                  isActivate: true,
                },
              },
            });
            userDispatch({
              type: userConst.EDIT_PROFILE,
              payload: {
                id: userInfo.id,
                firstName: userInfo.first_name,
                lastName: userInfo.last_name,
                dateOfBirth: userInfo.birthdate
                  ? convertTimestampToDate(userInfo.birthdate)
                  : null,
                email: userInfo.email,
                username: userInfo.username,
                phoneNumber: userInfo.phone_number,
                isActivate: true,
                studentId: userInfo.student_id,
              },
            });
            navigate("/home");
          })
          .catch((err) => {
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          });
      })
      .catch((err) => {
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);

  return (
    <Box
      sx={{
        mt: 8,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          width: "100%",
          maxWidth: 500,
          borderRadius: 2,
          p: 4,
          boxShadow: 10,
        }}
      >
        <Typography variant="h4">
          Wait a second for verifying your email!
        </Typography>
      </Box>
    </Box>
  );
}
