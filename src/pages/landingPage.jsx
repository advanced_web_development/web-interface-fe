import {
  Typography,
  Card,
  CardActions,
  CardMedia,
  CardContent,
  Button,
  Grid,
} from "@mui/material";

import Luat from "../assets/Luat.jpg";
import Khanh from "../assets/Khanh.jpg";
import Khoa from "../assets/Khoa.jpg";

const authors = [
  {
    name: "Huỳnh Tấn Khánh",
    id: "20120508",
    avatar: Khanh,
    contribution: "Participating in the project as a frontend developer",
    facebook: "https://www.facebook.com/htkhanh02",
  },
  {
    name: "Nguyễn Quốc Khoa",
    id: "20120511",
    avatar: Khoa,
    contribution: "Participating in the project as a backend developer",
    facebook: "https://www.facebook.com/khoaterminator",
  },
  {
    name: "Huỳnh Luật",
    id: "20120529",
    avatar: Luat,
    contribution:
      "Participating in the project as a frontend and backend developer",
    facebook: "https://www.facebook.com/profile.php?id=100009389821771",
  },
];

export default function LandingPage() {
  const handleFacebookClick = (link) => {
    window.open(link, "_blank");
  };

  return (
    <>
      <Typography align="center" p={3} variant="h3">
        Contributors
      </Typography>
      <Grid container justifyContent={"center"} spacing={4}>
        {authors.map((author) => {
          return (
            <Grid
              key={author.id}
              item
              xs={12}
              sm={6}
              md={4}
              lg={3}
              alignItems="stretch"
            >
              <Card
                sx={{
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Typography
                  display="flex"
                  component="div"
                  justifyContent={"center"}
                >
                  <CardMedia
                    component="img"
                    sx={{
                      maxHeight: "450px",
                      width: "auto",
                    }}
                    alt={author.name + " avatar"}
                    image={author.avatar}
                  />
                </Typography>
                <CardContent
                  sx={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography gutterBottom variant="h5" component="div">
                    {author.name + " - " + author.id}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {author.contribution}
                  </Typography>
                  <CardActions sx={{ p: 0, marginTop: "auto" }}>
                    <Button
                      size="small"
                      onClick={() => handleFacebookClick(author.facebook)}
                      sx={{ px: 0 }}
                    >
                      Facebook
                    </Button>
                  </CardActions>
                </CardContent>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
