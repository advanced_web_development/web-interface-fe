import { Box, Typography } from "@mui/material";
import { useContext, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

import readCookie from "../../utils/readCookie";
import { AuthContext, UserContext } from "../../store/context";
import { authConst, userConst } from "../../const";

export default function SetUpLoginThirdPartyPage() {
  const [, authDispatch] = useContext(AuthContext);
  const [, userDispatch] = useContext(UserContext);
  const { auth } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const data = JSON.parse(auth);
    if (data) {
      authDispatch({
        type: authConst.LOGIN,
        payload: {
          accessToken: data.access_token,
          refreshToken: data.refresh_token,
          user: {
            firstName: data.first_name,
            lastName: data.last_name,
            email: data.email,
            id: data.user_id,
            isActivate: data.is_activated,
            phoneNumber: data.phone_number,
            userName: data.username,
          },
        },
      });
      userDispatch({
        type: userConst.EDIT_PROFILE,
        payload: {
          firstName: data.first_name,
          lastName: data.last_name,
          email: data.email,
          id: data.user_id,
          isActivate: data.is_activated,
          phoneNumber: data.phone_number,
          userName: data.username,
        },
      });
      document.cookie = `auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
    }
    navigate("/");
  }, []);
  return (
    <Box
      sx={{
        marginTop: 8,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Typography variant="h3" noWrap={false}>
        Wait a second for setting up user!
      </Typography>
    </Box>
  );
}
