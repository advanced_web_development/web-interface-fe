import { useState } from "react";
import { LockOutlined, Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Container,
  Typography,
  Box,
  Grid,
  TextField,
  Avatar,
  InputAdornment,
  IconButton,
  Button,
  Divider,
} from "@mui/material";
import FacebookIcon from "@mui/icons-material/Facebook";
import GoogleIcon from "@mui/icons-material/Google";
import { Link, useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import { convertToTimestamp } from "../../utils/convertDateFormat";
import { severityConst } from "../../const";
import { authService } from "../../services/index";
import ButtonProgress from "../../components/button/buttonProgess";
import ThirdPartyLogin from "../../components/signInComponent/thirdPartyLogin";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link to="/">Fit class room</Link> {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default function SignUpPage() {
  const [loading, setLoading] = useState(false);
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    dateOfBirth: "2002-01-01",
    email: "",
    phoneNumber: "",
    userName: "",
    password: "",
    confirmedPassword: "",
  });
  const [errMessage, setErrMessage] = useState({
    userName: "",
    confirmedPassword: "",
    phoneNumber: "",
  });
  const [showPassword, setShowPassword] = useState({
    password: false,
    confirmedPassword: false,
  });
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();

    //check if password
    if (
      formData.confirmedPassword.length > 0 &&
      formData.password !== formData.confirmedPassword
    ) {
      setErrMessage({
        ...errMessage,
        confirmedPassword: "Confirmed password must equal to password!",
      });
      return;
    }

    setLoading(true);
    const payload = {
      email: formData.email,
      phone_number: formData.phoneNumber,
      username: formData.userName,
      first_name: formData.firstName,
      last_name: formData.lastName,
      birthdate: convertToTimestamp(formData.dateOfBirth),
      password: formData.password,
    };
    authService
      .signUp(payload)
      .then((data) => {
        //sign up successfully - alert
        enqueueSnackbar("Sign up successfully!", {
          variant: severityConst.SUCCESS,
        });
        navigate("/login");
      })
      .catch((err) => {
        //sign up failed - alert and show message
        if (err.response) {
          if (typeof err.response.data.message === "string") {
            enqueueSnackbar(err.response.data.message, {
              variant: severityConst.ERROR,
            });
          } else {
            if (Array.isArray(err.response.data.message)) {
              for (const index in err.response.data.message) {
                enqueueSnackbar(
                  "Invalid " + err.response.data.message[index].property,
                  {
                    variant: severityConst.ERROR,
                  }
                );
              }
            }
          }
        } else {
          enqueueSnackbar(err.message, { variant: severityConst.ERROR });
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const RegexPhoneNumber = (phoneNumber) => {
    const regexPhoneNumber = /(0[3|5|7|8|9])+([0-9]{8})\b/g;

    return phoneNumber.match(regexPhoneNumber) ? true : false;
  };
  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;

    // Xử lý trường kiểu checkbox riêng biệt
    const newValue = type === "checkbox" ? checked : value;

    //check phone number
    if (name === "phoneNumber") {
      if (RegexPhoneNumber(value) == true || value === "") {
        setErrMessage({
          ...errMessage,
          phoneNumber: "",
        });
      } else {
        setErrMessage({
          ...errMessage,
          phoneNumber: "Phone number is wrong!",
        });
      }
    }

    if (name === "confirmedPassword") {
      if (
        formData.confirmedPassword.length > 0 &&
        formData.password === value
      ) {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "",
        });
      } else {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "Confirmed password must equal to password!",
        });
      }
    }

    setFormData((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleTogglePassword = (type) => {
    setShowPassword((prevData) => ({
      ...prevData,
      [type]: !prevData[type],
    }));
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" onSubmit={(e) => handleSubmit(e)} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="family-name"
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="dateOfBirth"
                label="Date of birth"
                name="dateOfBirth"
                type="date"
                autoComplete="dateOfBirth"
                value={formData.dateOfBirth}
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={formData.email}
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                error={errMessage.phoneNumber.length > 0 ? true : false}
                helperText={
                  errMessage.phoneNumber.length > 0
                    ? errMessage.phoneNumber
                    : ""
                }
                id="phoneNumber"
                label="Phone Number"
                name="phoneNumber"
                autoComplete="phoneNumber"
                value={formData.phoneNumber}
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="userName"
                label="Username"
                name="userName"
                autoComplete="userName"
                value={formData.userName}
                onChange={(e) => handleChange(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Password"
                type={showPassword.password ? "text" : "password"}
                id="password"
                autoComplete="new-password"
                value={formData.password}
                onChange={(e) => handleChange(e)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => handleTogglePassword("password")}
                        edge="end"
                      >
                        {showPassword.password ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                error={errMessage.confirmedPassword.length > 0 ? true : false}
                helperText={
                  errMessage.confirmedPassword.length > 0
                    ? errMessage.confirmedPassword
                    : ""
                }
                name="confirmedPassword"
                label="Confirmed password"
                type={showPassword.confirmedPassword ? "text" : "password"}
                id="confirmedpassword"
                autoComplete="confirmed-password"
                value={formData.confirmedPassword}
                onChange={(e) => handleChange(e)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() =>
                          handleTogglePassword("confirmedPassword")
                        }
                        edge="end"
                      >
                        {showPassword.confirmedPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
            {/* <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="I want to receive inspiration, marketing promotions and updates via email."
                />
              </Grid> */}
          </Grid>
          <ButtonProgress loading={loading} text={"Sign Up"} />
          <Grid container justifyContent="center">
            <Grid item>
              <Link to="/login">Already have an account? Sign in</Link>
            </Grid>
          </Grid>

          <Divider sx={{ color: "#bbb", mt: 2 }} textAlign="center">
            Or
          </Divider>

          <ThirdPartyLogin />
        </Box>
      </Box>
      <Copyright sx={{ mt: 5 }} />
    </Container>
  );
}
