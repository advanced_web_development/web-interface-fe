import {
  Box,
  Typography,
  FormControl,
  TextField,
  IconButton,
  InputAdornment,
  Button,
  Grid,
} from "@mui/material";
import { ArrowBack, Visibility, VisibilityOff } from "@mui/icons-material";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";
import { MuiOtpInput } from "mui-one-time-password-input";

import ButtonProgress from "../../components/button/buttonProgess";
import { authService } from "../../services";
import { severityConst } from "../../const";

export default function ForgotPasswordPage() {
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loadingVerifyCode, setLoadingVerifyCode] = useState(false);
  const [loadingUpdatePassword, setLoadingUpdatePassword] = useState(false);
  const [email, setEmail] = useState("");
  const [verifyCode, setVerifyCode] = useState("");
  const [verifySuccefully, setVerifySuccefully] = useState(false);
  const [username, setUsername] = useState("");
  const [counter, setCounter] = useState(0);
  const [formData, setFormData] = useState({
    newPassword: "",
    confirmedPassword: "",
  });
  const [errMessage, setErrMessage] = useState({
    confirmedPassword: "",
  });
  const [showPassword, setShowPassword] = useState({
    newPassword: false,
    confirmedPassword: false,
  });
  const navigate = useNavigate();

  const handleTogglePassword = (type) => {
    setShowPassword((prevData) => ({
      ...prevData,
      [type]: !prevData[type],
    }));
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    const newValue = type === "checkbox" ? checked : value;
    //check if password equals to confirmed password
    if (name === "confirmedPassword") {
      if (
        formData.confirmedPassword.length > 0 &&
        formData.newPassword === value
      ) {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "",
        });
      } else {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "Confirmed password must equal to password!",
        });
      }
    }

    setFormData((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoadingSubmit(true);
    authService
      .submitUsernameForgetPassword({ username: username })
      .then((data) => {
        // console.log(data);
        setEmail(data.email);
      })
      .catch((err) => {
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
      })
      .finally(() => {
        setLoadingSubmit(false);
      });
  };

  const handleVerifyCode = (value) => {
    const code = isNaN(Number.parseInt(value)) ? 1 : Number.parseInt(value);
    setLoadingVerifyCode(true);
    authService
      .verifyCodeForgetPassword({ username: username, verify_code: code })
      .then((data) => {
        //transit entering new pass word state
        setVerifySuccefully(true);
        enqueueSnackbar("Verify code successfully", {
          variant: severityConst.SUCCESS,
        });
      })
      .catch((err) => {
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
      })
      .finally(() => {
        setLoadingVerifyCode(false);
      });
  };

  const handleUpdateForgetPassword = (event) => {
    event.preventDefault();
    setLoadingUpdatePassword(true);

    if (
      formData.confirmedPassword.length > 0 &&
      formData.newPassword !== formData.confirmedPassword
    ) {
      setErrMessage({
        ...errMessage,
        confirmedPassword: "Confirmed password must equal to password!",
      });
      return;
    }

    authService
      .updateForgetPassword({
        username: username,
        verify_code: Number.parseInt(verifyCode),
        password: formData.newPassword,
      })
      .then((data) => {
        enqueueSnackbar("Update password successfully!", {
          variant: severityConst.SUCCESS,
        });
        navigate("/login");
      })
      .catch((err) => {
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
      })
      .finally(() => {
        setLoadingUpdatePassword(false);
      });
  };

  function handleBackClick() {
    navigate("/login");
  }

  function handleGoToMailBox() {
    window.open("https://mail.google.com/mail/", "_blank");
  }

  function handleReSendClick() {
    setCounter(60);
    //Handle send request here
    authService
      .submitUsernameForgetPassword({ username: username })
      .then((data) => {
        enqueueSnackbar("Resend verify code successfully", {
          variant: severityConst.SUCCESS,
        });
        setEmail(data.email);
      })
      .catch((err) => {
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
      });
  }

  useEffect(() => {
    if (counter > 0) {
      const timer = setTimeout(() => setCounter(counter - 1), 1000);
      return () => clearTimeout(timer);
    }
  }, [counter]);

  const RegexNumber = (str) => {
    const regexNumber = /^\d+$/;
    return str.match(regexNumber) ? true : false;
  };

  const onChangeVerifyCode = (value) => {
    if (RegexNumber(value)) {
      setVerifyCode(value);
    } else {
      setVerifyCode("");
    }
  };

  return (
    <Box
      sx={{
        mt: 8,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          width: "100%",
          maxWidth: 500,
          borderRadius: 2,
          p: 4,
          boxShadow: 10,
        }}
      >
        <Box
          sx={{
            display: "flex",
          }}
        >
          <Box sx={{ ml: 2 }}>
            <IconButton onClick={handleBackClick}>
              <ArrowBack />
            </IconButton>
          </Box>

          <Box sx={{ ml: 9 }}>
            <Typography
              variant="h5"
              component="h1"
              sx={{
                pt: 0.5,
                textAlign: "center",
                mb: 4,
              }}
            >
              Forgot Password
            </Typography>
          </Box>
        </Box>

        {email.length > 0 ? (
          verifySuccefully ? (
            <></>
          ) : (
            <Box
              component="form"
              sx={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
              }}
              onSubmit={(e) => {
                e.preventDefault();
                handleVerifyCode(verifyCode);
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexDirection: "column",
                  mb: 3,
                }}
              >
                <Typography variant="caption">{`An email with a code has been sent to ${email}, enter this code to proceed next step`}</Typography>
                <MuiOtpInput
                  value={verifyCode}
                  onComplete={(value) => handleVerifyCode(value)}
                  onChange={(value) => onChangeVerifyCode(value)}
                  TextFieldsProps={{
                    autoComplete: "",
                  }}
                />
              </Box>
              <Box sx={{ display: "flex", justifyContent: "space-around" }}>
                <Button
                  onClick={handleReSendClick}
                  type="button"
                  variant="text"
                >
                  {counter === 0 ? `Resend code` : `Wait for: ${counter}`}
                </Button>
                <Button
                  onClick={handleGoToMailBox}
                  type="button"
                  variant="text"
                >
                  Go to mail box
                </Button>
              </Box>
              <ButtonProgress loading={loadingVerifyCode} text={"Verify"} />
            </Box>
          )
        ) : (
          <Box component="form" onSubmit={(e) => handleSubmit(e)}>
            <FormControl fullWidth sx={{ px: 3 }}>
              <TextField
                required
                label="Username"
                name="username"
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </FormControl>
            <ButtonProgress loading={loadingSubmit} text={"Submit"} />
          </Box>
        )}

        {verifySuccefully && (
          <Box
            component="form"
            onSubmit={(e) => handleUpdateForgetPassword(e)}
            sx={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  required
                  name="newPassword"
                  label="New password"
                  type={showPassword.newPassword ? "text" : "password"}
                  id="newPassword"
                  value={formData ? formData.newPassword : ""}
                  onChange={(e) => handleChange(e)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => handleTogglePassword("newPassword")}
                          edge="end"
                        >
                          {showPassword.newPassword ? (
                            <VisibilityOff />
                          ) : (
                            <Visibility />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  error={errMessage.confirmedPassword.length > 0 ? true : false}
                  helperText={
                    errMessage.confirmedPassword.length > 0
                      ? errMessage.confirmedPassword
                      : ""
                  }
                  name="confirmedPassword"
                  label="Confirm new password"
                  type={showPassword.confirmedPassword ? "text" : "password"}
                  id="confirmedpassword"
                  value={formData ? formData.confirmedPassword : ""}
                  onChange={(e) => handleChange(e)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            handleTogglePassword("confirmedPassword")
                          }
                          edge="end"
                        >
                          {showPassword.confirmedPassword ? (
                            <VisibilityOff />
                          ) : (
                            <Visibility />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
            </Grid>
            <ButtonProgress
              loading={loadingUpdatePassword}
              text={"Update password"}
            />
          </Box>
        )}
      </Box>
    </Box>
  );
}
