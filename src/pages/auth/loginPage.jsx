import { useContext, useState } from "react";
import {
  Container,
  Typography,
  Box,
  Grid,
  TextField,
  Avatar,
  IconButton,
  InputAdornment,
  Button,
  Divider,
  CircularProgress,
} from "@mui/material";
import { LockOutlined, Visibility, VisibilityOff } from "@mui/icons-material";
import { Link, useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import {
  AuthContext,
  NotificationsContext,
  UserContext,
} from "../../store/context";
import { severityConst, authConst, userConst, notiConst } from "../../const";
import { authService } from "../../services";
import ButtonProgress from "../../components/button/buttonProgess";
import ThirdPartyLogin from "../../components/signInComponent/thirdPartyLogin";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link to="/">Fit class room</Link> {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default function LoginPage() {
  const [showPassword, setShowPassword] = useState(false);
  const [, authDispatch] = useContext(AuthContext);
  const [, userDispatch] = useContext(UserContext);
  const [, notisDispatch] = useContext(NotificationsContext);
  const [loadingLoginJwt, setLoadingLoginJwt] = useState(false);
  const navigate = useNavigate();

  const handleSubmitLoginJwt = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const payload = {
      username: data.get("userName"),
      password: data.get("password"),
    };

    setLoadingLoginJwt(true);

    authService
      .loginJwt(payload)
      .then((data) => {
        authDispatch({
          type: authConst.LOGIN,
          payload: {
            accessToken: data.access_token,
            refreshToken: data.refresh_token,
            user: {
              firstName: data.first_name,
              lastName: data.last_name,
              email: data.email,
              id: data.user_id,
              isActivate: data.is_activated,
              phoneNumber: data.phone_number,
              userName: data.username,
            },
          },
        });
        userDispatch({
          type: userConst.EDIT_PROFILE,
          payload: {
            firstName: data.first_name,
            lastName: data.last_name,
            email: data.email,
            id: data.user_id,
            isActivate: data.is_activated,
            phoneNumber: data.phone_number,
            userName: data.username,
            studentId: data.student_id,
          },
        });
        notisDispatch({
          type: notiConst.SET_NOTIS,
        });
        navigate("/");
      })
      .catch((err) => {
        if (err.response.data.statusCode === 401) {
          //login failed - alert
          enqueueSnackbar("Invalid username or wrong password!", {
            variant: severityConst.ERROR,
          });
        } else {
          //login failed - alert
          enqueueSnackbar(err.response.data.message, {
            variant: severityConst.ERROR,
          });
        }
      })
      .finally(() => {
        setLoadingLoginJwt(false);
      });
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box
          component="form"
          onSubmit={(e) => handleSubmitLoginJwt(e)}
          sx={{ mt: 1 }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="userName"
            label="User name"
            name="userName"
            autoComplete="userName"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type={showPassword ? "text" : "password"}
            id="password"
            autoComplete="current-password"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPassword(!showPassword)}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
          <ButtonProgress loading={loadingLoginJwt} text={"sign in"} />
          <Grid container justifyContent={"center"} mt={2}>
            <Grid item xs>
              <Link to="/forgot-password" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link to="/signup">{"Don't have an account? Sign Up"}</Link>
            </Grid>
          </Grid>

          <Divider sx={{ color: "#bbb", mt: 4 }} textAlign="center">
            Or Login by
          </Divider>

          <ThirdPartyLogin />
        </Box>
      </Box>
      <Copyright sx={{ mt: 8, mb: 4 }} />
    </Container>
  );
}
