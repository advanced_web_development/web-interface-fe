import { useContext } from "react";
import { NotificationContext } from "../store/context";

export const useNotification = () => {
  const context = useContext(NotificationContext);
  return context;
};
