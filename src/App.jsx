import { Routes, Route } from "react-router-dom";

import LandingPage from "./pages/landingPage";
import UserHome from "./pages/user/userHome";
import LoginPage from "./pages/auth/loginPage";
import SignUpPage from "./pages/auth/signUpPage";
import ProtectedRoute from "./routes/protected-route";
import UnProtectedRoute from "./routes/unProtected-route";
import EditUserProfilePage from "./pages/user/editProfilePage";
import ForgotPasswordPage from "./pages/auth/forgotPasswordPage";
import ActivatePage from "./pages/user/activatePage";
import SetUpLoginThirdPartyPage from "./pages/auth/setUpLoginThirdPartyPage";
import VerifyRoute from "./routes/verify-route";
import VerifyPage from "./pages/user/verifyPage";
import PageNotFound from "./pages/notFoundPage";
import LinkInvitationPage from "./pages/invitation/linkInvitationPage";
import InvitationRoute from "./routes/invitation-route";
import MailInvitationPage from "./pages/invitation/mailInvitationPage";
import NewsPage from "./pages/class/newsPage";
import PeoplePage from "./pages/class/peoplePage";
import SettingPage from "./pages/class/settingPage";
import GradesPage from "./pages/class/gradesPage";
import PostPage from "./pages/class/postPage";
import MappingPage from "./pages/class/mappingPage";
import PointsPage from "./pages/class/pointPage";
import GradeReviewsPage from "./pages/class/gradeReviewsPage";
import GradeReviewPage from "./pages/class/gradeReviewPage";

function App() {
  return (
    <Routes>
      <Route path="/home" element={<ProtectedRoute />}>
        <Route index element={<UserHome />} />
        <Route path="user/edit" element={<EditUserProfilePage />} />
      </Route>

      <Route path="/class" element={<ProtectedRoute />}>
        <Route path=":classId" element={<NewsPage />} />
        <Route path=":classId/people" element={<PeoplePage />} />
        <Route path=":classId/setting" element={<SettingPage />} />
        <Route path=":classId/grades" element={<GradesPage />} />
        <Route path=":classId/mapping" element={<MappingPage />} />
        <Route path=":classId/points" element={<PointsPage />} />
        <Route path=":classId/grade-reviews" element={<GradeReviewsPage />} />
        <Route path=":classId/post/:postId" element={<PostPage />} />
        <Route
          path=":classId/exam/:examId/review/:examReviewId"
          element={<GradeReviewPage />}
        />
      </Route>

      <Route path="/email" element={<VerifyRoute />}>
        <Route path="activate" element={<ActivatePage />} />
        <Route path="verify/:token" element={<VerifyPage />} />
      </Route>

      <Route path="/invitation" element={<InvitationRoute />}>
        <Route path="by-link" element={<LinkInvitationPage />} />
        <Route path="by-mail" element={<MailInvitationPage />} />
      </Route>

      <Route path="/" element={<UnProtectedRoute />}>
        <Route index element={<LandingPage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/signup" element={<SignUpPage />} />
        <Route path="/forgot-password" element={<ForgotPasswordPage />} />
        <Route
          path="/user/setup/:auth"
          element={<SetUpLoginThirdPartyPage />}
        />
      </Route>
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
}

export default App;
