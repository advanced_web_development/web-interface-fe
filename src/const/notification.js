export const AUTO_HIDE_DURATION = import.meta.env.AUTO_HIDE_DURATION;
export const MAX_SNACK = import.meta.env.MAX_SNACK;
export const ANCHOR_ORIGIN = {
  vertical: "top",
  horizontal: "right",
};
export const ADD_NOTI = "add a notification";
export const ADD_NOTIS = "add notifications";
export const UPDATE_NOTI = "update a notification";
export const SET_NOTIS = "set notifications";
