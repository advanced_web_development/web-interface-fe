import { Outlet } from "react-router-dom";
import UnSignedInNavBar from "../components/navBar/unSignedInNavBar";
import SignedInNavBar from "../components/navBar/signedInNavBar";
import Footer from "../footer/footer";
import NotiStackProvider from "../store/provider/notiStack-provider";
import { Box, Toolbar } from "@mui/material";
import SideBar from "../components/sideBar/sideBar";
import { useContext } from "react";
import { SideBarContext } from "../store/context";

export function UnSignedInLayout() {
  return (
    <>
      <NotiStackProvider />
      <UnSignedInNavBar />
      <Outlet />
      <Footer />
    </>
  );
}

export function SignedInLayout() {
  return (
    <>
      <NotiStackProvider />
      <Box sx={{ display: "flex" }}>
        <SignedInNavBar />
        <SideBar />
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            display: "flex",
            flexDirection: "column",
            height: "100vh",
          }}
        >
          <Toolbar />
          <Outlet />
          <Footer />
        </Box>
      </Box>
    </>
  );
}
