import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import App from "./App";
import AuthProvider from "./store/provider/auth-provider";
import UserProvider from "./store/provider/user-provider";
import ColorThemeProvider from "./store/provider/theme-provider";
import SideBarProvider from "./store/provider/sideBar-provider";
import ClassProvider from "./store/provider/class-provider";
import InvitationProvider from "./store/provider/invitation-provider";
import CurrentClassProvider from "./store/provider/currentClass-provider";
import GradeStructureCurrentProvider from "./store/provider/grade-provider";
import NotificationsProvider from "./store/provider/notifications-provider";
import PostProvider from "./store/provider/post-provider";
import ReviewProvider from "./store/provider/review-provider";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <ColorThemeProvider
      children={
        <AuthProvider
          children={
            <UserProvider
              children={
                <ClassProvider
                  children={
                    <CurrentClassProvider
                      children={
                        <InvitationProvider
                          children={
                            <SideBarProvider
                              children={
                                <NotificationsProvider
                                  children={
                                    <PostProvider
                                      children={
                                        <ReviewProvider children={<App />} />
                                      }
                                    />
                                  }
                                />
                              }
                            />
                          }
                        />
                      }
                    />
                  }
                />
              }
            />
          }
        />
      }
    />
  </BrowserRouter>
);
