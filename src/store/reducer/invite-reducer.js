import { invitationConst } from "../../const/index";

const invitationInitialState = {
  url: localStorage.getItem("invitation") || "",
};

function InvitationReducer(state, action) {
  switch (action.type) {
    case invitationConst.SET_INVITATION:
      localStorage.setItem("invitation", action.payload.url);
      return { url: action.payload.url };
    case invitationConst.REMOVE_INVITATION:
      localStorage.removeItem("invitation");
      return { url: "" };
    default:
      return { ...state };
  }
}

export { invitationInitialState };
export default InvitationReducer;
