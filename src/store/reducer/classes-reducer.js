import { classesConst } from "../../const/index";

const classesInitialState = {
  teaching: [],
  enrolled: [],
};

function ClassesReducer(state, action) {
  switch (action.type) {
    case classesConst.SET_CLASSES:
      // console.log(action.payload);
      const teachingArr = action.payload.filter(
        (enrollment) =>
          enrollment.role === classesConst.ROLE_CREATOR ||
          enrollment.role === classesConst.ROLE_TEACHER
      );
      const enrolledArr = action.payload.filter(
        (enrollment) => enrollment.role === classesConst.ROLE_STUDENT
      );
      return {
        teaching: teachingArr,
        enrolled: enrolledArr,
      };
    case classesConst.ADD_CLASS:
      const newEnrollment = action.payload;
      if (
        newEnrollment.role === classesConst.ROLE_CREATOR ||
        newEnrollment.role === classesConst.ROLE_TEACHER
      ) {
        state.teaching.unshift(newEnrollment);
        return {
          teaching: state.teaching,
          enrrolled: state.enrolled,
        };
      } else {
        state.enrolled.unshift(newEnrollment);
        return {
          teaching: state.teaching,
          enrolled: state.enrolled,
        };
      }
    case classesConst.UPDATE_INVITE_CODE:
      const indexTeaching = state.teaching.findIndex(
        (item) => item.class_id === action.payload.id
      );
      if (indexTeaching !== -1) {
        state.teaching[indexTeaching].class_entity.invite_code =
          action.payload.code;
      }
      const indexEnrolled = state.enrolled.findIndex(
        (item) => item.class_id === action.payload.id
      );
      if (indexEnrolled !== -1) {
        state.enrolled[indexEnrolled].class_entity.invite_code =
          action.payload.code;
      }
      return {
        teaching: state.teaching,
        enrolled: state.enrolled,
      };
    case classesConst.EDIT_CLASS:
      return { ...state };
    default:
      return { ...state };
  }
}

export { classesInitialState };
export default ClassesReducer;
