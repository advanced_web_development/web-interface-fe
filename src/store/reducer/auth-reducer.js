import { authConst } from "../../const/index";

const storedUserString = localStorage.getItem("user");

const storedUser = storedUserString ? JSON.parse(storedUserString) : null;

const authInitialState = {
  accessToken: JSON.parse(localStorage.getItem("accessToken"))?.token || "",
  user: storedUser || {
    id: -1,
    firstName: null,
    lastName: null,
    email: null,
    dateOfBirth: null,
    userName: null,
    isActivate: false,
    phoneNumber: null,
  },
};

function AuthReducer(state, action) {
  switch (action.type) {
    case authConst.LOGIN:
      const { accessToken, refreshToken, user } = action.payload;
      localStorage.setItem(
        "accessToken",
        JSON.stringify({ token: accessToken })
      );
      localStorage.setItem(
        "refreshToken",
        JSON.stringify({ token: refreshToken })
      );
      localStorage.setItem("user", JSON.stringify(user));

      return {
        accessToken: accessToken,
        user: user,
      };
    case authConst.LOG_OUT:
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      localStorage.removeItem("user");
      return {
        accessToken: "",
        user: {
          id: -1,
          firstName: null,
          lastName: null,
          email: null,
          dateOfBirth: null,
          userName: null,
          isActivate: null,
          phoneNumber: null,
        },
      };
    default:
      return { ...state };
  }
}

export { authInitialState };
export default AuthReducer;
