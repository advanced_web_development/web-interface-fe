import { userConst } from "../../const/index";

const storedUserString = localStorage.getItem("user");

const storedUser = storedUserString ? JSON.parse(storedUserString) : null;
const userInitialState = storedUser || {
  id: -1,
  firstName: "",
  lastName: "",
  dateOfBirth: -1,
  email: "",
  username: "",
  studentId: "",
};

function UserReducer(state, action) {
  switch (action.type) {
    case userConst.EDIT_PROFILE:
      localStorage.setItem("user", JSON.stringify(action.payload));
      return { ...action.payload };
    default:
      return { ...state };
  }
}

export { userInitialState };
export default UserReducer;
