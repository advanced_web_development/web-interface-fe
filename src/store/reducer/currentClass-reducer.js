import { currentClassConst } from "../../const";

const currentClassInitialState = {
  id: null,
  name: null,
  userRole: null,
  description: null,
  image_url: null,
  is_watchable_another_student_grade: null,
  room: null,
  section: null,
  subject: null,
};

function CurrentClassReducer(state, action) {
  switch (action.type) {
    case currentClassConst.SET_CURRENT_CLASS:
      return {
        ...action.payload,
      };

    case currentClassConst.LEAVE_CURRENT_CLASS:
      return {
        id: null,
        name: null,
        role: null,
        description: null,
        image_url: null,
        is_watchable_another_student_grade: null,
        room: null,
        section: null,
        subject: null,
        invite_code: null,
      };

    default:
      return { ...state };
  }
}

export { currentClassInitialState };
export default CurrentClassReducer;
