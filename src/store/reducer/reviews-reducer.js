import { classesConst } from "../../const/index";

const reviewsInitialState = [];

function ReviewsReducer(state, action) {
  switch (action.type) {
    case classesConst.SET_REVIEWS:
      return action.payload;
    case classesConst.ADD_REVIEWS:
      return [...state, ...action.payload];
    case classesConst.CREATE_REVIEW:
      return [action.payload, ...state];
    case classesConst.DELETE_REVIEW:
      return state.filter((post) => post.id !== action.payload);
    case classesConst.EDIT_REVIEW:
      const newReviews = state.map((post) => {
        if (post.id !== action.payload.id) return post;
        return action.payload;
      });
      return newReviews;
    default:
      return { ...state };
  }
}

export { reviewsInitialState };
export default ReviewsReducer;
