import { currentClassConst } from "../../const";

const gradeStructureInitialState = [];
//array of list like
// [
//    { name: "seminar", order: 1, weight: 20 },
//    { name: "mid term", order: 3, weight: 30 },
//    { name: "final", order: 2, weight: 50 },
//  ];

function GradeStructureCurrentReducer(state, action) {
  switch (action.type) {
    case currentClassConst.SET_CURRENT_CLASS:
      const data = action.payload;
      if (data.length !== 0) {
        const result = data.map(({ id, class_id, ...rest }) => rest);

        let sortedData = [];
        sortedData = [...result].sort((a, b) => a.indexRow - b.indexRow);
        return sortedData;
      } else {
        return [];
      }

    case currentClassConst.LEAVE_CURRENT_CLASS:
      return [];
    default:
      return { ...state };
  }
}

export { gradeStructureInitialState };

export default GradeStructureCurrentReducer;
