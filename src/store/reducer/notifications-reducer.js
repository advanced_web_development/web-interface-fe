import { notiConst, userConst } from "../../const/index";

const notisInitialState = [];

function NotificationsReducer(state, action) {
  switch (action.type) {
    case notiConst.ADD_NOTI:
      const newNoti = {
        is_seen: action.payload.is_seen,
        notification_id: action.payload.notification_id,
        user_id: action.payload.user_id,
        notification: action.payload.Notification,
      };
      const newNotiss = state.filter(
        (noti) => noti.notification_id !== newNoti.notification_id
      );
      newNotiss.unshift(newNoti);
      return newNotiss;
    case notiConst.ADD_NOTIS:
      return [...state, ...action.payload];
    case notiConst.SET_NOTIS:
      return [];
    case notiConst.UPDATE_NOTI:
      const newNotis = state.map((noti) => {
        if (noti.notification_id === action.payload) {
          return {
            is_seen: true,
            notification_id: noti.notification_id,
            user_id: noti.user_id,
            notification: noti.notification,
          };
        }
        return noti;
      });
      return newNotis;
    default:
      return state;
  }
}

export { notisInitialState };
export default NotificationsReducer;
