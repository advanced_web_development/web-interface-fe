import { classesConst } from "../../const/index";

const postsInitialState = [];

function PostsReducer(state, action) {
  switch (action.type) {
    case classesConst.SET_POSTS:
      return action.payload;
    case classesConst.ADD_POSTS:
      return [...state, ...action.payload];
    case classesConst.CREATE_POST:
      return [action.payload, ...state];
    case classesConst.DELETE_POST:
      return state.filter((post) => post.id !== action.payload);
    case classesConst.EDIT_POST:
      const newPosts = state.map((post) => {
        if (post.id !== action.payload.id) return post;
        return action.payload;
      });
      return newPosts;
    default:
      return { ...state };
  }
}

export { postsInitialState };
export default PostsReducer;
