import { createContext } from "react";

export const AuthContext = createContext();
export const UserContext = createContext();
export const ColorModeContext = createContext();
export const SideBarContext = createContext();
export const ClassesContext = createContext();
export const InvitationContext = createContext();
export const CurrentClassContext = createContext();
export const GradeStructureContext = createContext();
export const NotificationsContext = createContext();
export const PostsContext = createContext();
export const ReviewsContext = createContext();
