import { NotificationsContext } from "../context";
import NotificationsReducer, {
  notisInitialState,
} from "../reducer/notifications-reducer";
import { useReducer } from "react";

export default function NotificationsProvider({ children }) {
  const [state, dispatch] = useReducer(NotificationsReducer, notisInitialState);
  return (
    <NotificationsContext.Provider value={[state, dispatch]}>
      {children}
    </NotificationsContext.Provider>
  );
}
