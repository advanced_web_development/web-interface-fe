import { useState, useMemo } from "react";

import { SideBarContext } from "../context";

export default function SideBarProvider({ children }) {
  const [mode, setMode] = useState(true);
  const sideBarMode = useMemo(
    () => ({
      toggleSideBarMode: () => {
        setMode((preMode) => {
          return !preMode;
        });
      },
    }),
    []
  );
  return (
    <SideBarContext.Provider value={[mode, sideBarMode]}>
      {children}
    </SideBarContext.Provider>
  );
}
