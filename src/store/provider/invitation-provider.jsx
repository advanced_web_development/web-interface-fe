import { InvitationContext } from "../context";
import InvitationReducer, {
  invitationInitialState,
} from "../reducer/invite-reducer";
import { useReducer } from "react";

export default function InvitationProvider({ children }) {
  const [state, dispatch] = useReducer(
    InvitationReducer,
    invitationInitialState
  );
  return (
    <InvitationContext.Provider value={[state, dispatch]}>
      {children}
    </InvitationContext.Provider>
  );
}
