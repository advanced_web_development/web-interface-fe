import { PostsContext } from "../context";
import PostReducer, { postsInitialState } from "../reducer/posts-reducer";
import { useReducer } from "react";

export default function PostProvider({ children }) {
  const [state, dispatch] = useReducer(PostReducer, postsInitialState);
  return (
    <PostsContext.Provider value={[state, dispatch]}>
      {children}
    </PostsContext.Provider>
  );
}
