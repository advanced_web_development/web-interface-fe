import { ClassesContext } from "../context";
import ClassReducer, { classesInitialState } from "../reducer/classes-reducer";
import { useReducer } from "react";

export default function ClassProvider({ children }) {
  const [state, dispatch] = useReducer(ClassReducer, classesInitialState);
  return (
    <ClassesContext.Provider value={[state, dispatch]}>
      {children}
    </ClassesContext.Provider>
  );
}
