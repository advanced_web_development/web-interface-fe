import { SnackbarProvider, useSnackbar, SnackbarContent } from "notistack";
import { closeSnackbar } from "notistack";
import { IconButton, Card, CardActions, Typography, Box } from "@mui/material";
import { Close } from "@mui/icons-material";
import { forwardRef, useCallback, useEffect, useContext } from "react";

import { notiConst, severityConst } from "../../const";
import { useLocation, useNavigate } from "react-router-dom";
import { notiService } from "../../services";
import { NotificationsContext } from "../context";

const Notification = forwardRef(({ id, ...props }, ref) => {
  const { closeSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const location = useLocation();
  const [, notisDispatch] = useContext(NotificationsContext);

  const handleDismiss = useCallback(() => {
    closeSnackbar(id);
  }, [id, closeSnackbar]);

  const goToPost = (path, notiId) => {
    if (path && notiId) {
      notiService
        .seenNotis(notiId)
        .then((data) => {
          notisDispatch({
            type: notiConst.UPDATE_NOTI,
            payload: notiId,
          });
        })
        .catch((err) => {});
      if (location.pathname !== path) navigate(path);
      handleDismiss();
    }
  };

  useEffect(() => {
    if (!(props.path && props.notiId)) {
      handleDismiss();
    }
  }, []);

  return (
    <SnackbarContent ref={ref}>
      <Card>
        <CardActions
          sx={{ cursor: "pointer", backgroundColor: "#29b6f6" }}
          onClick={() => goToPost(props.path, props.notiId)}
        >
          <Box display={"flex"} maxWidth={400}>
            <Typography flexGrow={1} variant="body2">
              {props.message}
            </Typography>
            <IconButton size="small" onClick={handleDismiss}>
              <Close fontSize="small" />
            </IconButton>
          </Box>
        </CardActions>
      </Card>
    </SnackbarContent>
  );
});

Notification.displayName = severityConst.NOTIFICATION;

function SnackbarCloseButton({ snackbarKey }) {
  return (
    <IconButton onClick={() => closeSnackbar(snackbarKey)}>
      <Close
        sx={{
          color: "white",
        }}
      />
    </IconButton>
  );
}

export default function NotiStackProvider() {
  return (
    <SnackbarProvider
      maxSnack={notiConst.MAX_SNACK}
      autoHideDuration={notiConst.AUTO_HIDE_DURATION}
      anchorOrigin={notiConst.ANCHOR_ORIGIN}
      preventDuplicate={true}
      Components={{
        notification: Notification,
      }}
      action={(snackbarKey) => (
        <SnackbarCloseButton snackbarKey={snackbarKey} />
      )}
    />
  );
}
