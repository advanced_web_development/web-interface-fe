import { ReviewsContext } from "../context";
import ReviewsReducer, {
  reviewsInitialState,
} from "../reducer/reviews-reducer";
import { useReducer } from "react";

export default function ReviewProvider({ children }) {
  const [state, dispatch] = useReducer(ReviewsReducer, reviewsInitialState);
  return (
    <ReviewsContext.Provider value={[state, dispatch]}>
      {children}
    </ReviewsContext.Provider>
  );
}
