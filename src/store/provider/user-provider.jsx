import { UserContext } from "../context";
import UserReducer, { userInitialState } from "../reducer/user-reducer";
import { useReducer } from "react";

export default function UserProvider({ children }) {
  const [state, dispatch] = useReducer(UserReducer, userInitialState);
  return (
    <UserContext.Provider value={[state, dispatch]}>
      {children}
    </UserContext.Provider>
  );
}
