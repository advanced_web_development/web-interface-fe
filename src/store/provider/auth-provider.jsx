import { AuthContext } from "../context";
import AuthReducer, { authInitialState } from "../reducer/auth-reducer";
import { useReducer } from "react";

export default function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(AuthReducer, authInitialState);
  return (
    <AuthContext.Provider value={[state, dispatch]}>
      {children}
    </AuthContext.Provider>
  );
}
