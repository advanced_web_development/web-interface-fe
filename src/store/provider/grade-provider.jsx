import { GradeStructureContext } from "../context";
import GradeStructureCurrentReducer, {
  gradeStructureInitialState,
} from "../reducer/grade-reducer";
import { useReducer } from "react";
export default function GradeStructureCurrentProvider({ children }) {
  const [state, dispatch] = useReducer(
    GradeStructureCurrentReducer,
    gradeStructureInitialState
  );
  return (
    <GradeStructureContext.Provider value={[state, dispatch]}>
      {children}
    </GradeStructureContext.Provider>
  );
}
