import { CurrentClassContext } from "../context";
import CurrentClassReducer, {
  currentClassInitialState,
} from "../reducer/currentClass-reducer";
import { useReducer } from "react";

export default function CurrentClassProvider({ children }) {
  const [state, dispatch] = useReducer(
    CurrentClassReducer,
    currentClassInitialState
  );
  return (
    <CurrentClassContext.Provider value={[state, dispatch]}>
      {children}
    </CurrentClassContext.Provider>
  );
}
