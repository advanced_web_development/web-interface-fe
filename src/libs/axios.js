import axios from "axios";

import { api } from "../const";

// export const privateAxios = () =>
//   axios.create({
//     baseURL: api.BASE_URL,
//     headers: {
//       Authorization: `Bearer ${
//         JSON.parse(localStorage.getItem("accessToken"))?.token || ""
//       }`,
//     },
//   });

const refreshAxios = () =>
  axios.create({
    baseURL: api.BASE_URL,
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("refreshToken"))?.token || ""
      }`,
    },
  });

const privateAxios = axios.create({
  baseURL: api.BASE_URL,
});
privateAxios.interceptors.request.use(
  (config) => {
    config.headers["Authorization"] = `Bearer ${
      JSON.parse(localStorage.getItem("accessToken"))?.token || ""
    }`;
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);
privateAxios.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    const originalConfig = err.config;
    if (err.response) {
      if (err.response.data.statusCode === 401 && !originalConfig._retry) {
        try {
          const rs = await refreshAxios().post("/auth/jwt/refresh");

          const { access_token, refresh_token } = rs.data;
          localStorage.setItem(
            "accessToken",
            JSON.stringify({ token: access_token })
          );
          localStorage.setItem(
            "refreshToken",
            JSON.stringify({ token: refresh_token })
          );

          return privateAxios(originalConfig);
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
      if (err.response.status === 502) {
        err.response.data = {
          statusCode: 502,
          message: "Hit the request threshold! Wait a minute",
        };
        return Promise.reject(err);
      }
    }
    return Promise.reject(err);
  }
);

export { privateAxios };

const publicAxios = axios.create({
  baseURL: api.BASE_URL,
});

publicAxios.interceptors.response.use(
  (response) => {
    // Do something with the response data
    return response;
  },
  (err) => {
    if (err.response) {
      if (err.response.status === 502) {
        err.response.data = {
          statusCode: 502,
          message: "Hit the request threshold! Wait a second",
        };
      }
    }
    return Promise.reject(err);
  }
);

export default publicAxios;
