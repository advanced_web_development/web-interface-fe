export function convertDateFormatDDMMYYYToYYYYMMDD(inputDate) {
  // Chuyển đổi ngày từ "DD/MM/YYYY" sang "YYYY-MM-DD"
  const parts = inputDate.split("/");
  const formattedDate = `${parts[2]}-${parts[1].padStart(
    2,
    "0"
  )}-${parts[0].padStart(2, "0")}`;
  return formattedDate;
}

export function convertDateFormatYYYYMMDDToDDMMYYY(inputDate) {
  const parts = inputDate.split("-");
  const formattedDate = `${parts[2].padStart(2, "0")}/${parts[1].padStart(
    2,
    "0"
  )}/${parts[0]}`;
  return formattedDate;
}

export function convertToTimestamp(dateString) {
  const dateObject = new Date(dateString);
  const timestamp = Math.floor(dateObject.getTime() / 1000);
  return timestamp;
}

export function convertTimestampToDate(timestamp) {
  const timestampMilliseconds = timestamp * 1000;
  // const hanoiDate = new Date(timestampMilliseconds).toLocaleString('en-US', { timeZone: hanoiTimezone });
  const date = new Date(timestampMilliseconds);

  // Get day, month, and year
  const day = date.getDate();
  const month = date.getMonth() + 1; // Months are zero-based, so add 1
  const year = date.getFullYear();

  // Format the date as "DD/MM/YYYY"
  const dateString = `${day}/${month}/${year}`;

  return dateString;
}

export function formatVietnamDate(utcDateString) {
  if (utcDateString) {
    const utcDate = new Date(utcDateString);
    const vietnamDate = new Intl.DateTimeFormat("en-US", {
      timeZone: "Asia/Ho_Chi_Minh",
      hour: "numeric",
      minute: "numeric",
      day: "numeric",
      month: "numeric",
      year: "numeric",
    }).format(utcDate);

    return vietnamDate;
  }
  return "";
}
