export function newArrayWhenCaculateGradeTotal(
  newStudentsArray,
  gradeExamInfoTable
) {
  return newStudentsArray.map((student) => {
    const total = Object.keys(student).reduce((totalGrade, key) => {
      if (key !== "StudentId" && key !== "FullName" && key !== "Total") {
        const examResult = student[key];
        const correspondingExam = gradeExamInfoTable.find(
          (exam) => exam.gradeExamTitle === key
        );
        if (correspondingExam) {
          totalGrade +=
            (examResult * correspondingExam.gradeExamPercentage) /
            (10 * correspondingExam.gradeExamMaxGrade);
        }
      }
      if (totalGrade > 10) {
        totalGrade = 10;
      }
      return parseFloat(totalGrade.toFixed(2));
    }, 0);

    return { ...student, Total: total };
  });
}

export function handleGroupData(data) {}
