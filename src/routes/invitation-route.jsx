import { useContext, useEffect } from "react";
import { AuthContext, InvitationContext } from "../store/context";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { SignedInLayout } from "../layouts";
import { invitationConst } from "../const";

export default function InvitationRoute() {
  const [state] = useContext(AuthContext);
  const [, invitationDispatch] = useContext(InvitationContext);
  const location = useLocation();
  const navigate = useNavigate();
  const params = new URLSearchParams(location.search);
  // if (state.accessToken.length === 0) {
  //   //save url
  //   invitationDispatch({
  //     type: invitationConst.SET_INVITATION,
  //     payload: {
  //       url: location.pathname + location.search,
  //     },
  //   });
  //   return <Navigate to="/login" state={{ from: location }} replace />;
  // }
  //If user login successfully, check user has confirmed their email
  //TODO logic
  // if (state.user.isActivate === false) {
  //   //User
  //   return <Navigate to="/email/activate" state={{ from: location }} replace />;
  // }
  useEffect(() => {
    // console.log("class_id: " + params.get("class_id"));
    // console.log("hashed token from mail: " + params.get("token"));
    // console.log("role from mail: " + params.get("role"));
    if (state.accessToken.length === 0) {
      //save url
      invitationDispatch({
        type: invitationConst.SET_INVITATION,
        payload: {
          url: location.pathname + location.search,
        },
      });
      // return <Navigate to="/login" state={{ from: location }} replace />;
      navigate("/login");
    }
    if (state.user.isActivate === false) {
      //User
      // return (
      //   <Navigate to="/email/activate" state={{ from: location }} replace />
      // );
      navigate("/email/activate");
    }
  }, []);
  return (
    <>
      <SignedInLayout />
    </>
  );
}
