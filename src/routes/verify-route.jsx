import { useContext } from "react";
import { AuthContext } from "../store/context";
import { Navigate, useLocation } from "react-router-dom";
import { SignedInLayout } from "../layouts";

export default function VerifyRoute() {
  const [state] = useContext(AuthContext);
  const location = useLocation();
  if (state.accessToken.length === 0) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  // state.user.isActivate = true;
  //If user activated, navigate to home
  if (state.user.isActivate === true) {
    //User
    return <Navigate to="/home" state={{ from: location }} replace />;
  }
  return (
    <>
      <SignedInLayout />
    </>
  );
}
