import { useContext } from "react";
import { AuthContext } from "../store/context";
import { Navigate, useLocation } from "react-router-dom";
import { UnSignedInLayout } from "../layouts";

export default function UnProtectedRoute() {
  const [state] = useContext(AuthContext);
  const location = useLocation();
  if (state.accessToken.length > 0) {
    return <Navigate to="/home" state={{ from: location }} replace />;
  }
  return (
    <>
      <UnSignedInLayout />
    </>
  );
}
