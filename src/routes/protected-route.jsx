import { useContext, useEffect } from "react";
import { AuthContext, InvitationContext } from "../store/context";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { SignedInLayout } from "../layouts";
import { invitationConst } from "../const";

export default function ProtectedRoute() {
  const [state] = useContext(AuthContext);
  const navigate = useNavigate();
  const [invitation, invitationDispatch] = useContext(InvitationContext);
  const location = useLocation();
  // console.log(invitation);
  if (state.accessToken.length === 0) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  //If user login successfully, check user has confirmed their email
  //TODO logic
  if (state.user.isActivate === false) {
    //User
    return <Navigate to="/email/activate" state={{ from: location }} replace />;
  }
  useEffect(() => {
    if (invitation.url?.length > 0) {
      if (state.accessToken.length > 0 && state.user.isActivate === true) {
        const redirectUrl = invitation.url;
        invitationDispatch({
          type: invitationConst.REMOVE_INVITATION,
        });
        navigate(redirectUrl);
      }
    }
  });
  return (
    <>
      <SignedInLayout />
    </>
  );
}
