import { Box, Typography } from "@mui/material";

export default function Footer() {
  return (
    <Box sx={{ p: 3, marginTop: "auto" }} component="footer">
      <Typography variant="h6" align="center" gutterBottom>
        fit hcmus
      </Typography>
      <Typography
        variant="subtitle1"
        align="center"
        color="text.secondary"
        component="p"
      >
        2023-2024 - Advanced Web Application Development
      </Typography>
    </Box>
  );
}
