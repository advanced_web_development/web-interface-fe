import {
  Box,
  Typography,
  List,
  ListItemButton,
  ListItemIcon,
  ListItem,
  ListItemText,
  Divider,
  Toolbar,
  Collapse,
  Tooltip,
  Skeleton,
} from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import { styled, useTheme } from "@mui/material/styles";
import {
  Inbox,
  Mail,
  Home as HomeIcon,
  ExpandLess,
  ExpandMore,
  School as SchoolIcon,
  People as PeopleIcon,
  HistoryEdu as HistoryEduIcon,
} from "@mui/icons-material";
import { enqueueSnackbar } from "notistack";
import { useContext, useEffect, useState } from "react";
import {
  AuthContext,
  ClassesContext,
  CurrentClassContext,
  GradeStructureContext,
  SideBarContext,
} from "../../store/context";
import { useLocation, useNavigate } from "react-router-dom";
import { classService, gradeService } from "../../services";
import {
  classesConst,
  severityConst,
  authConst,
  currentClassConst,
} from "../../const";

const drawerWidth = 300;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create(["width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create(["width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

export default function SideBar() {
  const [mode, sideBarMode] = useContext(SideBarContext);
  const [classes, classesDispatch] = useContext(ClassesContext);
  const [, authDispatch] = useContext(AuthContext);
  const [, currentClassDispatch] = useContext(CurrentClassContext);
  // const [, gradeCurrentClassDispatch] = useContext(GradeStructureContext);
  const [openJoinedClasses, setOpenJoinedClasses] = useState(true);
  const [openTeachingClasses, setOpenTeachingClasses] = useState(true);
  const [loading, setLoading] = useState(true);
  const theme = useTheme();
  const location = useLocation();
  const navigate = useNavigate();

  function handleClassClick(classId) {
    //navigate to class
    navigate(`/class/${classId}`);
  }

  useEffect(() => {
    setLoading(true);
    classService
      .getAllClassOfUser()
      .then((data) => {
        // console.log(data);
        classesDispatch({
          type: classesConst.SET_CLASSES,
          payload: data,
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return (
    <Drawer variant="permanent" open={mode}>
      <Toolbar />
      <List>
        <ListItem disablePadding sx={{ display: "block" }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: mode ? "initial" : "center",
              px: 2.5,
              borderRadius: "0 50px 50px 0px",
            }}
            onClick={() => {
              currentClassDispatch({
                type: currentClassConst.LEAVE_CURRENT_CLASS,
              });
              navigate("/home");
            }}
            selected={location.pathname.endsWith("home")}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: mode ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Home" sx={{ opacity: mode ? 1 : 0 }} />
          </ListItemButton>
        </ListItem>
      </List>

      <Divider />

      <List>
        <ListItemButton
          sx={{
            borderRadius: "0 50px 50px 0px",
          }}
          onClick={() => setOpenTeachingClasses(!openTeachingClasses)}
        >
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary="Teaching" />
          {openTeachingClasses ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openTeachingClasses} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {classes.teaching?.length > 0 ? (
              classes.teaching.map((item) => {
                const className = item.class_entity?.is_activate
                  ? item.class_entity?.name
                  : "[Inactive] " + item.class_entity?.name;
                return (
                  <Box key={item.class_id}>
                    {item.class_entity?.is_activate ? (
                      <ListItemButton
                        onClick={() => {
                          if (item.class_entity?.is_activate)
                            handleClassClick(item.class_id);
                        }}
                        key={item.class_id}
                        sx={{ borderRadius: "0 50px 50px 0px" }}
                        selected={location.pathname.includes(item.class_id)}
                      >
                        <ListItemIcon>
                          <HistoryEduIcon />
                        </ListItemIcon>

                        {className.length >= 25 ? (
                          <Tooltip title={className}>
                            <ListItemText
                              primary={className.substring(0, 24) + "..."}
                            />
                          </Tooltip>
                        ) : (
                          <ListItemText primary={className} />
                        )}
                      </ListItemButton>
                    ) : (
                      <ListItemButton
                        sx={{ borderRadius: "0 50px 50px 0px", opacity: 0.5 }}
                      >
                        <ListItemIcon>
                          <HistoryEduIcon />
                        </ListItemIcon>

                        {className.length >= 25 ? (
                          <Tooltip title={className}>
                            <ListItemText
                              primary={className.substring(0, 24) + "..."}
                            />
                          </Tooltip>
                        ) : (
                          <ListItemText primary={className} />
                        )}
                      </ListItemButton>
                    )}
                  </Box>
                );
              })
            ) : (
              <ListItemButton sx={{ orderRadius: "0 50px 50px 0px" }}>
                {loading ? (
                  <Skeleton sx={{ flexGrow: 1 }} />
                ) : (
                  <>
                    <ListItemIcon></ListItemIcon>
                    <ListItemText primary="No class is joined" />
                  </>
                )}
              </ListItemButton>
            )}
          </List>
        </Collapse>
      </List>

      <Divider />

      <List>
        <ListItemButton
          sx={{
            borderRadius: "0 50px 50px 0px",
          }}
          onClick={() => setOpenJoinedClasses(!openJoinedClasses)}
        >
          <ListItemIcon>
            <SchoolIcon />
          </ListItemIcon>
          <ListItemText primary="Enrolled" />
          {openJoinedClasses ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openJoinedClasses} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {classes.enrolled?.length > 0 ? (
              classes.enrolled.map((item) => {
                const className = item.class_entity?.is_activate
                  ? item.class_entity?.name
                  : "[Inactive] " + item.class_entity?.name;
                return (
                  <Box key={item.class_id}>
                    {item.class_entity?.is_activate ? (
                      <ListItemButton
                        onClick={() => {
                          if (item.class_entity?.is_activate)
                            handleClassClick(item.class_id);
                        }}
                        key={item.class_id}
                        sx={{ borderRadius: "0 50px 50px 0px" }}
                        selected={location.pathname.includes(item.class_id)}
                      >
                        <ListItemIcon>
                          <HistoryEduIcon />
                        </ListItemIcon>

                        {className.length >= 25 ? (
                          <Tooltip title={className}>
                            <ListItemText
                              primary={className.substring(0, 24) + "..."}
                            />
                          </Tooltip>
                        ) : (
                          <ListItemText primary={className} />
                        )}
                      </ListItemButton>
                    ) : (
                      <ListItemButton
                        sx={{ borderRadius: "0 50px 50px 0px", opacity: 0.5 }}
                      >
                        <ListItemIcon>
                          <HistoryEduIcon />
                        </ListItemIcon>

                        {className.length >= 25 ? (
                          <Tooltip title={className}>
                            <ListItemText
                              primary={className.substring(0, 24) + "..."}
                            />
                          </Tooltip>
                        ) : (
                          <ListItemText primary={className} />
                        )}
                      </ListItemButton>
                    )}
                  </Box>
                );
              })
            ) : (
              <ListItemButton sx={{ orderRadius: "0 50px 50px 0px" }}>
                {loading ? (
                  <Skeleton sx={{ flexGrow: 1 }} />
                ) : (
                  <>
                    <ListItemIcon></ListItemIcon>
                    <ListItemText primary="No class is joined" />
                  </>
                )}
              </ListItemButton>
            )}
          </List>
        </Collapse>
      </List>
    </Drawer>
  );
}

// export default SideBar;
