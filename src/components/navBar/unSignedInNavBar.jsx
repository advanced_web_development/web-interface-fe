import {
  AppBar,
  Container,
  Toolbar,
  Typography,
  Button,
  Box,
  Tooltip,
  IconButton,
} from "@mui/material";
import { Class } from "@mui/icons-material";
import "@fontsource/roboto/500.css";
import { useNavigate } from "react-router-dom";
import { useTheme } from "@emotion/react";
import { Brightness7, Brightness4 } from "@mui/icons-material";
import { ColorModeContext } from "../../store/context";
import { useContext, useEffect } from "react";
import { authService } from "../../services";

export default function UnSignedInNavBar() {
  const navigate = useNavigate();
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  const colorText = theme.palette.mode === "dark" ? "white" : "#424242";

  const homeButtonClick = () => {
    navigate("/");
  };

  const loginButtonClick = () => {
    navigate("/login");
  };

  const signUpButtonClick = () => {
    navigate("/signup");
  };

  // useEffect(() => {
  //   authService.wakeUpMailService();
  // }, []);

  return (
    <AppBar
      position="sticky"
      color="inherit"
      sx={{
        boxShadow: "none",
        borderBottom:
          theme.palette.mode === "dark"
            ? "1px solid #424242"
            : "1px solid #e0e0e0",
      }}
    >
      <Toolbar>
        <Box
          onClick={() => homeButtonClick()}
          sx={{
            flexGrow: 1,
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
          }}
        >
          <Button
            sx={{
              color: colorText,
              borderRadius: 1,
              ":hover": {
                textDecoration: "underline",
                backgroundColor:
                  theme.palette.mode === "dark" ? "#424242" : "#e0e0e0",
              },
            }}
          >
            <Class />
            <Typography
              sx={{
                display: "inline-block",
                typography: {
                  sm: "h6",
                  md: "h5",
                },
              }}
              noWrap={false}
            >
              Fit class room
            </Typography>
          </Button>
        </Box>
        <Tooltip title={theme.palette.mode + " mode"}>
          <IconButton
            sx={{ ml: 1 }}
            onClick={colorMode.toggleColorMode}
            color="inherit"
          >
            {theme.palette.mode === "dark" ? <Brightness7 /> : <Brightness4 />}
          </IconButton>
        </Tooltip>
        <Button color="inherit" onClick={() => loginButtonClick()}>
          Sign in
        </Button>
        <Typography>/</Typography>
        <Button color="inherit" onClick={() => signUpButtonClick()}>
          Sign up
        </Button>
      </Toolbar>
    </AppBar>
  );
}
