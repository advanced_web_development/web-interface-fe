import {
  AppBar,
  Container,
  Toolbar,
  Typography,
  MenuItem,
  Menu,
  Tooltip,
  IconButton,
  Avatar,
  Box,
  Badge,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from "@mui/material";
import {
  Class,
  Brightness7,
  Brightness4,
  Menu as MenuIcon,
  Add as AddIcon,
} from "@mui/icons-material";
import "@fontsource/roboto/500.css";
import { useNavigate } from "react-router-dom";
import { useContext, useState, useEffect } from "react";
import { enqueueSnackbar } from "notistack";

import {
  userConst,
  authConst,
  classesConst,
  severityConst,
  currentClassConst,
  notiConst,
} from "../../const";
import {
  AuthContext,
  ColorModeContext,
  SideBarContext,
  UserContext,
  ClassesContext,
  CurrentClassContext,
  GradeStructureContext,
  NotificationsContext,
} from "../../store/context";
import { authService, classService, notiService } from "../../services";
import { useTheme } from "@mui/material/styles";
import { socket } from "../../socket";
import NotificationsMenu from "./notificationsMenu";

export default function SignedInNavBar() {
  const [authState, authDispatch] = useContext(AuthContext);
  const [user] = useContext(UserContext);
  const navigate = useNavigate();
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [anchorElAddClass, setAnchorElAddClass] = useState(null);
  const [openCreateClassForm, setOpenCreateClassForm] = useState(false);
  const [openJoinClassForm, setOpenJoinClassForm] = useState(false);
  const [isCreating, setIsCreating] = useState(false);
  const [isJoining, setIsJoining] = useState(false);
  const [className, setClassName] = useState("");
  const [section, setSection] = useState("");
  const [subject, setSubject] = useState("");
  const [room, setRoom] = useState("");
  const [classCode, setClassCode] = useState("");
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  const [, classesDispatch] = useContext(ClassesContext);
  const [, currentClassDispatch] = useContext(CurrentClassContext);
  const [, sideBarMode] = useContext(SideBarContext);
  // const [, gradeCurrentClassDispatch] = useContext(GradeStructureContext);
  const [notis, notisDispatch] = useContext(NotificationsContext);
  const [numUnSeen, setNumUnSeen] = useState(0);

  useEffect(() => {
    notiService
      .countingUnSeenNotis()
      .then((data) => {
        setNumUnSeen(data);
      })
      .catch((err) => {});

    function onReceivedNotification(value) {
      // console.log("receive message");
      // console.log(value);
      enqueueSnackbar(value.noti.Notification.title, {
        variant: severityConst.NOTIFICATION,
        path: value.noti.Notification.link,
        notiId: value.noti.Notification.id,
      });
      notisDispatch({
        type: notiConst.ADD_NOTI,
        payload: value.noti,
      });
      notiService
        .countingUnSeenNotis()
        .then((data) => {
          setNumUnSeen(data);
        })
        .catch((err) => {});
    }
    function onDisconnected() {
      // console.log("Socket disconnected");
    }

    function setupSocketEvent() {
      socket.on("events", onReceivedNotification);
      socket.on("disconnect", onDisconnected);
      if (socket.connected) {
        socket.emit("associateUser", user.id || -1);
        // console.log("Connect to server successfully");
      } else {
        setTimeout(() => {
          // console.log("Connect to server failed");
          // console.log("Retrying to connect...");
          socket.off("events", onReceivedNotification);
          socket.off("disconnect", onDisconnected);
          setupSocketEvent(); // Re-setup event listeners
        }, 1000); // Retry after 3 seconds (adjust as needed)
      }
    }

    if (authState.user.id !== -1) {
      setupSocketEvent();
    }

    return () => {
      socket.off("events", onReceivedNotification);
      socket.off("disconnect", onDisconnected);
    };
  }, [authState, notis]);

  // useEffect(() => {
  //   socket.emit("associateUser", user.id || -1);
  // }, [authState]);

  const colorText = theme.palette.mode === "dark" ? "white" : "#424242";

  const handleClickOpenCreateClassForm = () => {
    setOpenCreateClassForm(true);
  };

  const handleCloseCreateClassForm = (isCreate) => {
    if (isCreate) {
      setIsCreating(true);

      const data = {
        name: className,
        section: section,
        room: room,
        subject: subject,
      };

      classService
        .createClass(data)
        .then((data) => {
          // console.log(data);
          //dispatch current class
          currentClassDispatch({
            type: currentClassConst.SET_CURRENT_CLASS,
            payload: { userRole: "creator", ...data },
          });

          // gradeCurrentClassDispatch({
          //   type: currentClassConst.SET_CURRENT_CLASS,
          //   payload: [],
          // });

          enqueueSnackbar("Create class successfully!", {
            variant: severityConst.SUCCESS,
          });
          classService
            .getAllClassOfUser()
            .then((classesList) => {
              // console.log(data);
              classesDispatch({
                type: classesConst.SET_CLASSES,
                payload: classesList,
              });
            })
            .catch((err) => {
              //axios failed - alert
              enqueueSnackbar(err.response?.data.message || err.message, {
                variant: severityConst.ERROR,
              });
              if (err.response?.data.statusCode === 401) {
                authDispatch({
                  type: authConst.LOG_OUT,
                });
                navigate("/login");
              }
            });
          navigate(`/class/${data.id}`);
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
      setIsCreating(false);
    }

    setOpenCreateClassForm(false);
    setClassName("");
    setSection("");
    setSubject("");
    setRoom("");
  };

  const handleClickOpenJoinClassForm = () => {
    setOpenJoinClassForm(true);
  };

  const handleCloseJoinClassForm = (isJoin) => {
    if (isJoin) {
      setIsJoining(true);

      const data = {
        code: classCode,
        user_id: user.id,
      };

      classService
        .addStudent(data)
        .then((data) => {
          enqueueSnackbar("Join class successfully!", {
            variant: severityConst.SUCCESS,
          });
          classesDispatch({
            type: classesConst.ADD_CLASS,
            payload: data,
          });
          navigate(`/class/${data.class_id}`);
        })
        .catch((err) => {
          //user in class
          if (err.response?.data.statusCode === 409) {
            //redirect class url
            enqueueSnackbar("User already exists in class", {
              variant: severityConst.ERROR,
            });
            navigate(`/class/${err.response?.data.message}`);
          } else {
            enqueueSnackbar(err.response?.data.message || err.message, {
              variant: severityConst.ERROR,
            });
            if (err.response?.data.statusCode === 401) {
              authDispatch({
                type: authConst.LOG_OUT,
              });
              navigate("/login");
            }
          }
        });
      setIsJoining(false);
    }

    setOpenJoinClassForm(false);
    setClassCode("");
  };

  const homeButtonClick = () => {
    navigate("/home");
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleOpenAddMenu = (event) => {
    setAnchorElAddClass(event.currentTarget);
  };

  const handleCloseUserMenu = (choice) => {
    setAnchorElUser(null);
    if (choice === userConst.HOME) {
      navigate("/home");
    }
    if (choice === userConst.EDIT_PROFILE) {
      navigate("/home/user/edit");
    }
    if (choice === userConst.LOG_OUT) {
      authService.logout().finally(() => {
        authDispatch({
          type: authConst.LOG_OUT,
        });
        navigate("/");
      });
    }
  };

  const handleCloseAddClassMenu = (choice) => {
    setAnchorElAddClass(null);
    if (choice === classesConst.JOIN_CLASS) {
      handleClickOpenJoinClassForm();
    }
    if (choice === classesConst.CREATE_CLASS) {
      handleClickOpenCreateClassForm();
    }
  };

  return (
    <AppBar
      color="inherit"
      sx={{
        zIndex: (theme) => theme.zIndex.drawer + 1,
        boxShadow: "none",
        borderBottom:
          theme.palette.mode === "dark"
            ? "1px solid #424242"
            : "1px solid #e0e0e0",
      }}
      component="nav"
    >
      <Toolbar
        sx={{
          color: colorText,
          paddingLeft: "12px !important",
        }}
      >
        <IconButton onClick={sideBarMode.toggleSideBarMode}>
          <Badge>
            <MenuIcon />
          </Badge>
        </IconButton>
        <Box
          onClick={() => homeButtonClick()}
          sx={{
            flexGrow: 1,
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
          }}
        >
          <Button
            sx={{
              color: colorText,
              borderRadius: 1,
              ":hover": {
                textDecoration: "underline",
                backgroundColor:
                  theme.palette.mode === "dark" ? "#424242" : "#e0e0e0",
              },
            }}
          >
            <Class />
            <Typography
              sx={{
                display: "inline-block",
                typography: {
                  sm: "h6",
                  md: "h5",
                },
              }}
              noWrap={false}
            >
              Fit class room
            </Typography>
          </Button>
        </Box>
        <Box sx={{ flexGrow: 0, display: "flex", alignItems: "center" }}>
          <Tooltip title="Open settings">
            <IconButton onClick={(e) => handleOpenAddMenu(e)} sx={{ p: 0 }}>
              <AddIcon sx={{ fontSize: 30 }} />
            </IconButton>
          </Tooltip>
          <Menu
            sx={{ mt: "45px" }}
            id="menu-appbar"
            anchorEl={anchorElAddClass}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorElAddClass)}
            onClose={() => handleCloseAddClassMenu()}
          >
            {classesConst.SETTINGS.map((setting) => (
              <MenuItem
                key={setting}
                onClick={() => handleCloseAddClassMenu(setting)}
              >
                <Typography textAlign="center">{setting}</Typography>
              </MenuItem>
            ))}
          </Menu>
          <NotificationsMenu
            numUnSeen={numUnSeen}
            setNumUnSeen={setNumUnSeen}
          />
          <Tooltip title={theme.palette.mode + " mode"}>
            <IconButton
              sx={{ ml: 1 }}
              onClick={colorMode.toggleColorMode}
              color="inherit"
            >
              {theme.palette.mode === "dark" ? (
                <Brightness7 />
              ) : (
                <Brightness4 />
              )}
            </IconButton>
          </Tooltip>
          <Typography component="div" sx={{ mr: "10px" }}>
            Hello, {user.firstName}!
          </Typography>
          <Tooltip title="Open settings">
            <IconButton onClick={(e) => handleOpenUserMenu(e)} sx={{ p: 0 }}>
              <Avatar>{user.firstName.charAt(0)}</Avatar>
            </IconButton>
          </Tooltip>
          <Menu
            sx={{ mt: "45px" }}
            id="menu-appbar"
            anchorEl={anchorElUser}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorElUser)}
            onClose={() => handleCloseUserMenu()}
          >
            {userConst.SETTINGS.map((setting) => (
              <MenuItem
                key={setting}
                onClick={() => handleCloseUserMenu(setting)}
              >
                <Typography textAlign="center">{setting}</Typography>
              </MenuItem>
            ))}
          </Menu>
        </Box>
      </Toolbar>
      {/* form create class */}
      <Dialog
        open={openCreateClassForm}
        onClose={() => handleCloseCreateClassForm(false)}
      >
        <DialogTitle>Create class</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Class name (require)"
            fullWidth
            variant="standard"
            value={className}
            onChange={(e) => setClassName(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Section"
            fullWidth
            variant="standard"
            autoComplete="off"
            value={section}
            onChange={(e) => setSection(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Subject"
            id="subject"
            fullWidth
            variant="standard"
            autoComplete="subject"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Room"
            fullWidth
            autoComplete="room"
            variant="standard"
            value={room}
            onChange={(e) => setRoom(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => handleCloseCreateClassForm(false)}
            disabled={isCreating ? true : false}
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleCloseCreateClassForm(true)}
            disabled={className.length > 0 ? (isCreating ? true : false) : true}
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
      {/* form join class */}
      <Dialog
        open={openJoinClassForm}
        onClose={() => handleCloseJoinClassForm(false)}
      >
        <DialogTitle>Join class</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Ask your teacher for the class code, then enter it here.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="Class code"
            fullWidth
            variant="standard"
            value={classCode}
            onChange={(e) => setClassCode(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => handleCloseJoinClassForm(false)}
            disabled={isJoining ? true : false}
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleCloseJoinClassForm(true)}
            disabled={classCode.length > 0 ? (isJoining ? true : false) : true}
          >
            Join
          </Button>
        </DialogActions>
      </Dialog>
    </AppBar>
  );
}
