import { useContext, useEffect, useState } from "react";
import {
  FiberManualRecord,
  Notifications,
  NotificationsActive,
} from "@mui/icons-material";
import {
  Badge,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  Box,
  CircularProgress,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";
import InfiniteScroll from "react-infinite-scroll-component";

import { AuthContext, NotificationsContext } from "../../store/context";
import { classService, notiService } from "../../services";
import { notiConst, severityConst } from "../../const";
import { formatVietnamDate } from "../../utils/convertDateFormat";
import { useLocation, useNavigate } from "react-router-dom";

export default function NotificationsMenu({ numUnSeen, setNumUnSeen }) {
  const [, authDispatch] = useContext(AuthContext);
  const [notis, notisDispatch] = useContext(NotificationsContext);
  const [lastNotiId, setLastNotiId] = useState(-1);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [hasMore, setHasMore] = useState(true);
  const navigate = useNavigate();
  const location = useLocation();

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = (path, notiId) => {
    setAnchorElUser(null);
    if (path && notiId) {
      notiService
        .seenNotis(notiId)
        .then((data) => {
          notisDispatch({
            type: notiConst.UPDATE_NOTI,
            payload: notiId,
          });
          setNumUnSeen(numUnSeen - 1);
        })
        .catch((err) => {});
      if (location.pathname !== path) navigate(path);
    }
  };

  const getNotis = () => {
    notiService
      .getNotis(lastNotiId)
      .then((data) => {
        if (data?.length > 0) {
          notisDispatch({
            type: notiConst.ADD_NOTIS,
            payload: data,
          });
          setLastNotiId(data[data.length - 1]?.notification_id);
        }
        if (data?.length < 10) {
          setHasMore(false);
        }
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  };

  useEffect(() => {
    if (notis.length === 0) {
      getNotis();
    }
    notiService
      .countingUnSeenNotis()
      .then((data) => {
        setNumUnSeen(data);
      })
      .catch((err) => {});
  }, [notis]);

  return (
    <>
      <IconButton onClick={handleOpenUserMenu}>
        <Badge badgeContent={numUnSeen} color="error">
          <Notifications />
        </Badge>
      </IconButton>
      <Menu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={() => handleCloseUserMenu()}
      >
        <InfiniteScroll
          dataLength={notis.length}
          next={getNotis}
          hasMore={hasMore}
          height={530}
          loader={
            <Box display={"flex"} justifyContent={"center"}>
              <CircularProgress />
            </Box>
          }
          endMessage={
            <Typography textAlign="center">
              Yay! You have seen it all
            </Typography>
          }
        >
          {notis.map((noti) => {
            return (
              <Box key={noti.notification_id}>
                <MenuItem
                  onClick={() =>
                    handleCloseUserMenu(
                      noti.notification.link,
                      noti.notification_id
                    )
                  }
                  style={{ whiteSpace: "normal" }}
                  divider
                >
                  <Box display={"flex"} flexDirection={"column"}>
                    <Box display={"flex"} boxSizing={"border-box"}>
                      <Typography variant="body2" noWrap={false} width={300}>
                        {noti.notification?.title || ""}
                      </Typography>
                      {noti.is_seen ? null : (
                        <FiberManualRecord color="primary" fontSize="small" />
                      )}
                    </Box>
                    <Box display={"flex"}>
                      <Typography variant="caption" flexGrow={1}>
                        {formatVietnamDate(noti.notification?.created_at)}
                      </Typography>
                      <Typography variant="caption">Go to the post</Typography>
                    </Box>
                  </Box>
                </MenuItem>
              </Box>
            );
          })}
        </InfiniteScroll>
      </Menu>
    </>
  );
}
