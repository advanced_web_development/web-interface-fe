import MoreVertIcon from "@mui/icons-material/MoreVert";
import FullscreenIcon from "@mui/icons-material/Fullscreen";
import {
  Box,
  Grid,
  IconButton,
  Menu,
  Typography,
  MenuItem,
  Button,
  Divider,
} from "@mui/material";
import LinkIcon from "@mui/icons-material/Link";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import { useState, useContext, useEffect } from "react";
import { enqueueSnackbar } from "notistack";
import ClassInfoDialog from "../dialog/classInfoDialog";
import { CurrentClassContext } from "../../store/context";
import { classesConst } from "../../const";

function InforClass({ gradeStructure }) {
  const [classInfor] = useContext(CurrentClassContext);
  const [isTeacher, setIsTeacher] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleOpenMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const [openDialog, setOpenDialog] = useState(false);

  const handleOpenDialogClick = () => {
    setOpenDialog(true);
  };

  const handleCopyClassCodeInMenu = () => {
    navigator.clipboard
      .writeText(`${classInfor.invite_code}`)
      .then(() => {
        setOpenDialog(false);
        enqueueSnackbar("Invite code copied");
      })
      .catch((error) => {});
    handleClose();
  };

  const handleCopyLinkInviteInMenu = () => {
    navigator.clipboard
      .writeText(
        `${window.location.origin}/invitation/by-link?code=${classInfor.invite_code}`
      )
      .then(() => {
        setOpenDialog(false);
        enqueueSnackbar("Link copied");
      })
      .catch((error) => {});
    handleClose();
  };

  useEffect(() => {
    if (
      classInfor.role === classesConst.ROLE_CREATOR ||
      classInfor.role === classesConst.ROLE_TEACHER
    ) {
      setIsTeacher(true);
    } else {
      setIsTeacher(false);
    }
  }, [classInfor]);

  return (
    <>
      <Grid container spacing={2}>
        {/* Box contain title and class code  */}
        {isTeacher === false ? null : classInfor.invite_code ? (
          <Grid
            item
            xs={12}
            sm={12}
            lg={3}
            xl={2}
            sx={{
              display: { lg: "flex" },
            }}
          >
            {/* Title and morevert */}
            <Box
              sx={{
                border: "1px solid #acacac",
                borderRadius: 1,
                width: "100%",
                height: "6rem",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginLeft: 2,
                }}
              >
                <Box>Class code</Box>
                <IconButton
                  onClick={(e) => {
                    handleOpenMenuClick(e);
                  }}
                >
                  <MoreVertIcon />
                </IconButton>

                <Menu
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  MenuListProps={{
                    "aria-labelledby": "basic-button",
                  }}
                >
                  <MenuItem
                    onClick={handleCopyLinkInviteInMenu}
                    sx={{ marginBottom: 1 }}
                  >
                    <LinkIcon sx={{ marginRight: 2 }} />
                    <Typography>Copy class invite link</Typography>
                  </MenuItem>
                  <MenuItem
                    onClick={handleCopyClassCodeInMenu}
                    sx={{ marginBottom: 1 }}
                  >
                    <ContentCopyIcon sx={{ marginRight: 2 }} />
                    <Typography>Copy class code</Typography>
                  </MenuItem>
                  {/* <MenuItem onClick={handleClose} sx={{ marginBottom: 1 }}>
                  <RestartAltIcon sx={{ marginRight: 2 }} />
                  <Typography>Reset class code</Typography>
                </MenuItem> */}
                </Menu>
              </Box>

              {/* Class code and fullscreen */}
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  marginLeft: 2,
                }}
              >
                <Typography
                  variant="body"
                  sx={{
                    marginRight: "1rem",
                    color: "#1967d2",
                    display: "inline-block",
                  }}
                >
                  {classInfor.invite_code}
                </Typography>
                <IconButton onClick={handleOpenDialogClick}>
                  <FullscreenIcon color="primary" />
                </IconButton>
                <ClassInfoDialog
                  openDialog={openDialog}
                  setOpenDialog={setOpenDialog}
                  classInfor={classInfor}
                />
              </Box>
            </Box>
          </Grid>
        ) : null}
        <Grid
          item
          xs={12}
          sm={12}
          lg={
            classInfor.invite_code === null ? 12 : isTeacher === false ? 12 : 9
          }
          xl={
            classInfor.invite_code === null ? 12 : isTeacher === false ? 12 : 10
          }
        >
          <Box
            sx={{
              border: "1px solid #acacac",
              borderRadius: 1,
              px: 2,
              py: 1,
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Typography>Name: {classInfor?.name}</Typography>
            <Typography>
              Section:{" "}
              {classInfor?.section?.length > 0 ? (
                classInfor?.section
              ) : (
                <Typography variant="caption" sx={{ opacity: 0.5 }}>
                  undefined
                </Typography>
              )}
            </Typography>
            <Typography>
              Room:{" "}
              {classInfor?.room?.length > 0 ? (
                classInfor?.room
              ) : (
                <Typography variant="caption" sx={{ opacity: 0.5 }}>
                  undefined
                </Typography>
              )}
            </Typography>
            <Typography>
              Subject:{" "}
              {classInfor?.subject?.length > 0 ? (
                classInfor?.subject
              ) : (
                <Typography variant="caption" sx={{ opacity: 0.5 }}>
                  undefined
                </Typography>
              )}
            </Typography>
            <Typography>
              Description:{" "}
              {classInfor?.description?.length > 0 ? (
                classInfor?.description
              ) : (
                <Typography variant="caption" sx={{ opacity: 0.5 }}>
                  undefined
                </Typography>
              )}
            </Typography>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12}>
          <Box
            sx={{
              border: "1px solid #acacac",
              borderRadius: 1,
              px: 2,
              py: 1,
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="body1">Grade structure</Typography>
            {gradeStructure.map((g) => (
              <Typography key={g.id}>{`${g.name}: ${g.weight}%`}</Typography>
            ))}
            {gradeStructure.length === 0 && <Typography>Unknow</Typography>}
          </Box>
        </Grid>
      </Grid>
    </>
  );
}

export default InforClass;
