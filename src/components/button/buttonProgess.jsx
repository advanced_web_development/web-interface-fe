import { Box, Button, CircularProgress } from "@mui/material";

export default function ButtonProgress({ loading, text }) {
  return (
    <Box
      sx={{
        m: 1,
        position: "relative",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Button
        type="submit"
        fullWidth
        variant="contained"
        sx={{ mt: 2, maxWidth: "300px" }}
        disabled={loading}
      >
        {text}
      </Button>
      {loading && (
        <CircularProgress
          size={24}
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            marginTop: "-6px",
            marginLeft: "-12px",
          }}
        />
      )}
    </Box>
  );
}
