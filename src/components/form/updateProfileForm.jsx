import { Box, Grid, TextField } from "@mui/material";
import { useContext, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import ButtonProgress from "../button/buttonProgess";
import { UserContext, AuthContext } from "../../store/context";
import { authService } from "../../services";
import {
  convertDateFormatDDMMYYYToYYYYMMDD,
  convertTimestampToDate,
  convertToTimestamp,
} from "../../utils/convertDateFormat";
import { severityConst, userConst, authConst } from "../../const";

export default function UpdateProfileForm() {
  const [user, userDispatch] = useContext(UserContext);
  const [account, authDispatch] = useContext(AuthContext);
  const [updatingProfile, setUpdatingProfile] = useState(false);
  const [formData, setFormData] = useState();
  const [errMessage, setErrMessage] = useState({
    phoneNumber: "",
  });
  const navigate = useNavigate();

  const RegexPhoneNumber = (phoneNumber) => {
    const regexPhoneNumber = /(0[3|5|7|8|9])+([0-9]{8})\b/g;

    return phoneNumber.match(regexPhoneNumber) ? true : false;
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    const newValue = type === "checkbox" ? checked : value;

    //check phone number
    if (name === "phoneNumber") {
      if (RegexPhoneNumber(value) == true || value === "") {
        setErrMessage({
          ...errMessage,
          phoneNumber: "",
        });
      } else {
        setErrMessage({
          ...errMessage,
          phoneNumber: "Phone number is wrong format!",
        });
      }
    }

    setFormData((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleSubmitChangeInfo = (event) => {
    event.preventDefault();
    setUpdatingProfile(true);
    const payload = {
      first_name: formData.firstName,
      last_name: formData.lastName,
      birthdate:
        formData.dateOfBirth.length === 0
          ? null
          : convertToTimestamp(formData.dateOfBirth),
      email: formData.email,
      phone_number: formData.phoneNumber,
      isActivate: user.isActivate,
    };

    authService
      .changeProfile(payload)
      .then(() => {
        //edit successfully - alert
        enqueueSnackbar("Edit information successfully!", {
          variant: severityConst.SUCCESS,
        });

        userDispatch({
          type: userConst.EDIT_PROFILE,
          payload: {
            ...user,
            firstName: payload.first_name,
            lastName: payload.last_name,
            dateOfBirth: formData.dateOfBirth,
            phoneNumber: payload.phone_number,
          },
        });
      })
      .catch((err) => {
        if (err.response?.data.statusCode === 401) {
          //edit failed - alert
          enqueueSnackbar(err.response.data.message + "! Sign in again!", {
            variant: severityConst.ERROR,
          });
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        } else if (typeof err.response?.data.message !== "string") {
          if (err.response?.data.statusCode === 400) {
            //edit failed - alert
            enqueueSnackbar("Input wrong type!", {
              variant: severityConst.ERROR,
            });
          } else {
            //edit failed - alert
            enqueueSnackbar(err.message, {
              variant: severityConst.ERROR,
            });
          }
        } else {
          enqueueSnackbar(err.response?.data.message, {
            variant: severityConst.ERROR,
          });
        }
      })
      .finally(() => {
        setUpdatingProfile(false);
      });
  };

  useEffect(() => {
    authService
      .getProfile(account.user.id)
      .then((data) => {
        const ddmmyyyy = convertTimestampToDate(data.birthdate);
        const yyyymmdd = convertDateFormatDDMMYYYToYYYYMMDD(ddmmyyyy);
        userDispatch({
          type: userConst.EDIT_PROFILE,
          payload: {
            firstName: data.first_name,
            lastName: data.last_name,
            email: data.email,
            id: data.id,
            isActivate: data.is_activated,
            phoneNumber: data.phone_number,
            userName: data.username,
            dateOfBirth: data.birthdate ? yyyymmdd : null,
          },
        });
        setFormData({
          id: data.id,
          firstName: data.first_name,
          lastName: data.last_name,
          dateOfBirth: data.birthdate ? yyyymmdd : "",
          email: data.email,
          phoneNumber: data.phone_number,
        });
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response.data.message, {
          variant: severityConst.ERROR,
        });
        if (err.response.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      });
  }, []);

  return (
    <Box component="form" onSubmit={(e) => handleSubmitChangeInfo(e)}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            required
            disabled
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={formData ? formData.email : ""}
            onChange={(e) => handleChange(e)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            autoComplete="given-name"
            name="firstName"
            required
            fullWidth
            id="firstName"
            label="First Name"
            value={formData ? formData.firstName : ""}
            onChange={(e) => handleChange(e)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            fullWidth
            id="lastName"
            label="Last Name"
            name="lastName"
            autoComplete="family-name"
            value={formData ? formData.lastName : ""}
            onChange={(e) => handleChange(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            id="dateOfBirth"
            label="Date of birth"
            name="dateOfBirth"
            type="date"
            autoComplete="dateOfBirth"
            value={formData ? formData.dateOfBirth : "2002-01-01"}
            onChange={(e) => handleChange(e)}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            error={errMessage.phoneNumber.length > 0 ? true : false}
            helperText={
              errMessage.phoneNumber.length > 0 ? errMessage.phoneNumber : ""
            }
            id="phoneNumber"
            label="Phone number"
            name="phoneNumber"
            autoComplete="phoneNumber"
            value={formData?.phoneNumber || ""}
            onChange={(e) => handleChange(e)}
          />
        </Grid>
      </Grid>
      <ButtonProgress loading={updatingProfile} text={"Update Info"} />
    </Box>
  );
}
