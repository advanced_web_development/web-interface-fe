import {
  Box,
  Grid,
  TextField,
  InputAdornment,
  IconButton,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

import ButtonProgress from "../button/buttonProgess";
import { AuthContext } from "../../store/context";
import { authService } from "../../services";
import { severityConst, userConst, authConst } from "../../const";

export default function UpdatePasswordForm() {
  const [, authDispatch] = useContext(AuthContext);
  const [updatingPassword, setUpdatingPassword] = useState(false);
  const [formData, setFormData] = useState({
    oldPassword: "",
    password: "",
    confirmedPassword: "",
  });
  const [errMessage, setErrMessage] = useState({
    confirmedPassword: "",
  });
  const [showPassword, setShowPassword] = useState({
    oldPassword: false,
    password: false,
    confirmedPassword: false,
  });
  const navigate = useNavigate();

  const handleTogglePassword = (type) => {
    setShowPassword((prevData) => ({
      ...prevData,
      [type]: !prevData[type],
    }));
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    const newValue = type === "checkbox" ? checked : value;
    //check if password equals to confirmed password
    if (name === "confirmedPassword") {
      if (
        formData.confirmedPassword.length > 0 &&
        formData.password === value
      ) {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "",
        });
      } else {
        setErrMessage({
          ...errMessage,
          confirmedPassword: "Confirmed password must equal to password!",
        });
      }
    }

    setFormData((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleSubmitChangePassword = (event) => {
    event.preventDefault();
    //check if password equals to confirmed password
    if (
      formData.confirmedPassword.length > 0 &&
      formData.password !== formData.confirmedPassword
    ) {
      setErrMessage({
        ...errMessage,
        confirmedPassword: "Confirmed password must equal to password!",
      });
      return;
    }

    if (formData.oldPassword === formData.password) {
      enqueueSnackbar("The new password must be different from the old one!", {
        variant: severityConst.ERROR,
      });
      return;
    }

    setUpdatingPassword(true);

    const payload = {
      old_password: formData.oldPassword,
      new_password: formData.password,
    };

    authService
      .changePassword(payload)
      .then(() => {
        //edit successfully - alert
        enqueueSnackbar("Change password successfully!", {
          variant: severityConst.SUCCESS,
        });
      })
      .catch((err) => {
        if (err.response.data.statusCode === 401) {
          //edit password failed - alert
          enqueueSnackbar(err.response.data.message + "! Sign in again!", {
            variant: severityConst.ERROR,
          });
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        } else {
          //edit failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
        }
      })
      .finally(() => {
        setUpdatingPassword(false);
      });
  };

  return (
    <Box
      component="form"
      onSubmit={(e) => handleSubmitChangePassword(e)}
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
      }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            fullWidth
            required
            name="oldPassword"
            label="Current password"
            type={showPassword.oldPassword ? "text" : "password"}
            id="oldPassword"
            value={formData ? formData.oldPassword : ""}
            onChange={(e) => handleChange(e)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => handleTogglePassword("password")}
                    edge="end"
                  >
                    {showPassword.oldPassword ? (
                      <VisibilityOff />
                    ) : (
                      <Visibility />
                    )}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            fullWidth
            required
            name="password"
            label="Password"
            type={showPassword.password ? "text" : "password"}
            id="password"
            autoComplete="new-password"
            value={formData ? formData.password : ""}
            onChange={(e) => handleChange(e)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => handleTogglePassword("password")}
                    edge="end"
                  >
                    {showPassword.password ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            fullWidth
            error={errMessage.confirmedPassword.length > 0 ? true : false}
            helperText={
              errMessage.confirmedPassword.length > 0
                ? errMessage.confirmedPassword
                : ""
            }
            name="confirmedPassword"
            label="Confirmed password"
            type={showPassword.confirmedPassword ? "text" : "password"}
            id="confirmedpassword"
            value={formData ? formData.confirmedPassword : ""}
            onChange={(e) => handleChange(e)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => handleTogglePassword("confirmedPassword")}
                    edge="end"
                  >
                    {showPassword.confirmedPassword ? (
                      <VisibilityOff />
                    ) : (
                      <Visibility />
                    )}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <ButtonProgress loading={updatingPassword} text={"Update password"} />
    </Box>
  );
}
