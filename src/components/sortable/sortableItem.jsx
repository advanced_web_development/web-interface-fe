import { useSortable } from "@dnd-kit/sortable";
import React, { useState } from "react";

import { CSS } from "@dnd-kit/utilities";
import {
  Box,
  Grid,
  Typography,
  TextField,
  Button,
  IconButton,
} from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import ClearIcon from "@mui/icons-material/Clear";

function SortableItem(props) {
  // console.log("props:", props);
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: props.id.id });
  const [isDragging, setIsDragging] = useState(false);

  const handleMouseDown = () => {
    setIsDragging(true);
  };

  const handleMouseUp = () => {
    setIsDragging(false);
  };

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };
  // console.log("style", style);
  return (
    <Grid
      container
      spacing={3}
      ref={setNodeRef}
      style={style}
      {...attributes}
      {...listeners}
      mb={3}
      alignItems={"center"}
    >
      <Grid item xs={1}>
        <DragIndicatorIcon
          sx={{
            "&:hover": {
              cursor: isDragging ? "grabbing" : "grab",
            },
          }}
          onMouseDown={handleMouseDown}
          onMouseUp={handleMouseUp}
        />
      </Grid>
      <Grid item xs={5}>
        <TextField
          required
          fullWidth
          id="category"
          label="Grade category"
          name={props.id.id}
          //  autoComplete="name"
          value={props ? props.id.name : ""}
          onChange={(e) => props.handleChangeName(e)}
          autoFocus
        />
      </Grid>
      <Grid item xs={5}>
        <TextField
          required
          fullWidth
          id="percentage"
          label="Weigth category"
          //  defaultValue="100"
          name={props.id.id}
          //  autoComplete="name"
          value={props ? props.id.weight.toString() : ""}
          onChange={(e) => props.handleChangeWeight(e)}
        />
      </Grid>
      <Grid item xs={1}>
        <IconButton onClick={props.onDelete}>
          <ClearIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
}

export default SortableItem;
