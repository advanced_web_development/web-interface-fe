import {
  Card,
  CardActionArea,
  CardMedia,
  Avatar,
  Typography,
  CardContent,
  IconButton,
  Tooltip,
  Menu,
  MenuItem,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Button,
  colors,
} from "@mui/material";

import {
  Person as PersonIcon,
  MoreVert as MoreVertIcon,
} from "@mui/icons-material";
import { useContext, useState } from "react";
import { enqueueSnackbar } from "notistack";

import { classesConst, currentClassConst, severityConst } from "../../const";
import { classService, gradeService } from "../../services";
import {
  ClassesContext,
  CurrentClassContext,
  GradeStructureContext,
} from "../../store/context";
import { useNavigate } from "react-router-dom";

export default function ClassCard({ classItem }) {
  const [anchorElOption, setAnchorElOption] = useState(null);
  const [openEditClassForm, setOpenEditClassForm] = useState(false);
  const [openUnenrollForm, setOpenUnenrollForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [isUnenrolling, setIsUnenrolling] = useState(false);
  const [className, setClassName] = useState(classItem.class_entity?.name);
  const [section, setSection] = useState(classItem.class_entity?.section);
  const [subject, setSubject] = useState(classItem.class_entity?.subject);
  const [room, setRoom] = useState(classItem.class_entity?.room);
  const [, classesDispatch] = useContext(ClassesContext);
  const [currentClass, currentClassDispatch] = useContext(CurrentClassContext);
  // const [, gradeCurrentClassDispatch] = useContext(GradeStructureContext);
  const navigate = useNavigate();

  const handleOpenOptionMenu = (event) => {
    setAnchorElOption(event.currentTarget);
  };

  const handleClickOpenEditClassForm = () => {
    setOpenEditClassForm(true);
  };

  const handleCardClassClick = (classItem) => {
    // console.log(classItem);
    currentClassDispatch({
      type: currentClassConst.SET_CURRENT_CLASS,
      payload: {
        userRole: classItem.role,
        id: classItem.class_id,
        name: classItem.class_entity.name,
        image_url: classItem.class_entity.image_url,
        is_watchable_another_student_grade:
          classItem.class_entity.is_watchable_another_student_grade,
        room: classItem.class_entity.room,
        subject: classItem.class_entity.subject,
        section: classItem.class_entity.section,
      },
    });
    // gradeService
    //   .getGradeStructureClass(classItem.class_id)
    //   .then((data) => {
    //     gradeCurrentClassDispatch({
    //       type: currentClassConst.SET_CURRENT_CLASS,
    //       payload: data,
    //     });
    //   })
    //   .catch((err) => {
    //     //axios failed - alert
    //     enqueueSnackbar(err.response?.data.message || err.message, {
    //       variant: severityConst.ERROR,
    //     });
    //     if (err.response?.data.statusCode === 401) {
    //       authDispatch({
    //         type: authConst.LOG_OUT,
    //       });
    //       navigate("/login");
    //     }
    //   });
    navigate(`/class/${classItem.class_id}`);
  };

  const handleCloseEditClassForm = (isEdit) => {
    if (isEdit) {
      setIsEditing(true);

      const data = {
        name: className,
        section: section,
        room: room,
        subject: subject,
        class_id: classItem.class_id,
      };

      classService
        .updateClass(data)
        .then((data) => {
          // console.log(data);
          enqueueSnackbar("Updated class successfully!", {
            variant: severityConst.SUCCESS,
          });
          classService
            .getAllClassOfUser()
            .then((classesList) => {
              // console.log(data);
              classesDispatch({
                type: classesConst.SET_CLASSES,
                payload: classesList,
              });
            })
            .catch((err) => {
              //axios failed - alert
              enqueueSnackbar(err.response?.data.message || err.message, {
                variant: severityConst.ERROR,
              });
              if (err.response?.data.statusCode === 401) {
                authDispatch({
                  type: authConst.LOG_OUT,
                });
                navigate("/login");
              }
            });
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          setClassName(classItem.class_entity?.name);
          setSection(classItem.class_entity?.section);
          setSubject(classItem.class_entity?.subject);
          setRoom(classItem.class_entity?.room);
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
      setIsEditing(false);
    }

    setOpenEditClassForm(false);
    if (!isEdit) {
      setClassName(classItem.class_entity?.name);
      setSection(classItem.class_entity?.section);
      setSubject(classItem.class_entity?.subject);
      setRoom(classItem.class_entity?.room);
    }
  };
  const handleClickOpenUnenrollForm = () => {
    setOpenUnenrollForm(true);
  };

  const handleCloseUnenrollForm = (isUnenroll) => {
    if (isUnenroll) {
      setIsUnenrolling(true);

      const data = {
        class_id: classItem.class_id,
        user_id: classItem.user_id,
      };
      classService
        .deleteParticipant(data)
        .then((data) => {
          // console.log(data);
          enqueueSnackbar("Unenroll class successfully!", {
            variant: severityConst.SUCCESS,
          });
          classService
            .getAllClassOfUser()
            .then((classesList) => {
              // console.log(data);
              classesDispatch({
                type: classesConst.SET_CLASSES,
                payload: classesList,
              });
            })
            .catch((err) => {
              //axios failed - alert
              enqueueSnackbar(err.response?.data.message || err.message, {
                variant: severityConst.ERROR,
              });
              if (err.response?.data.statusCode === 401) {
                authDispatch({
                  type: authConst.LOG_OUT,
                });
                navigate("/login");
              }
            });
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
      setIsUnenrolling(false);
    }

    setOpenUnenrollForm(false);
  };

  const handleCloseOptionMenu = (choice) => {
    setAnchorElOption(null);
    if (choice === classesConst.UNENROLL) {
      handleClickOpenUnenrollForm();
    }
    if (choice === classesConst.COPY_INVITE_LINK) {
      // handleClickOpenCreateClassForm();
      navigator.clipboard.writeText(
        `${window.location.origin}/invitation/by-link?code=${classItem.class_entity.invite_code}`
      );
      enqueueSnackbar("Link copied");
    }
    if (choice === classesConst.EDIT) {
      handleClickOpenEditClassForm();
    }
  };

  return (
    <Card
      sx={{
        height: "100%",
        opacity: classItem?.class_entity?.is_activate ? 1 : 0.5,
      }}
    >
      <Box
        sx={{
          height: "100%",
          minHeight: "300px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "start",
          alignItems: "start",
          cursor: "pointer",
        }}
      >
        <CardMedia
          component="div"
          alt="green iguana"
          sx={{
            height: "100px",
            width: "100%",
            backgroundImage:
              "url(https://www.gstatic.com/classroom/themes/img_learnlanguage.jpg)",
            display: "flex",
            flexDirection: "column-reverse",
            alignItems: "end",
            paddingRight: "30px",
          }}
          children={
            // <Avatar
            //   alt={classItem.class_entity?.users[0].user_entity.first_name}
            //   src="https://i.pinimg.com/originals/5f/5e/48/5f5e48039492904af3cef7c97265399a.jpg"
            //   sx={{ width: 80, height: 80, marginBottom: -5 }}
            // />
            <>
              <Avatar sx={{ width: 80, height: 80, marginBottom: -5 }}>
                <PersonIcon sx={{ width: 60, height: 60 }} />
              </Avatar>
              <Box
                sx={{
                  flexGrow: 0,
                  display: "flex",
                  alignItems: "center",
                  marginBottom: 3,
                  marginRight: -3,
                }}
              >
                <Tooltip title="Open settings">
                  <IconButton
                    onClick={(e) => {
                      if (classItem?.class_entity?.is_activate)
                        handleOpenOptionMenu(e);
                    }}
                    sx={{
                      p: 0,
                      color: "white",
                      backgroundColor: "rgba(0, 0, 0, 0.2)",
                      ":hover": {
                        backgroundColor: "rgba(0, 0, 0, 0.4)",
                      },
                    }}
                  >
                    <MoreVertIcon sx={{ fontSize: 30 }} />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElOption}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElOption)}
                  onClose={() => handleCloseOptionMenu()}
                >
                  {classesConst.CLASS_CARD_SETTINGS.map((setting) => {
                    if (classItem.role === classesConst.ROLE_STUDENT) {
                      if (
                        setting === classesConst.EDIT ||
                        setting === classesConst.COPY_INVITE_LINK
                      ) {
                        return null;
                      }
                    }
                    if (classItem.role === classesConst.ROLE_CREATOR) {
                      if (setting === classesConst.UNENROLL) {
                        return null;
                      }
                    }
                    if (setting === classesConst.COPY_INVITE_LINK)
                      if (classItem.class_entity.invite_code === null)
                        return null;

                    return (
                      <MenuItem
                        key={setting}
                        onClick={() => handleCloseOptionMenu(setting)}
                      >
                        <Typography textAlign="center">{setting}</Typography>
                      </MenuItem>
                    );
                  })}
                </Menu>
              </Box>
            </>
          }
        />
        <CardContent
          onClick={() => {
            if (classItem?.class_entity?.is_activate)
              handleCardClassClick(classItem);
          }}
          sx={{
            paddingTop: 5,
            flexGrow: 1,
            ":hover": {
              textDecoration: "underline",
            },
          }}
        >
          <Typography variant="h6" component="div">
            {/* {classItem.name?.length >= 28
              ? classItem.name.substring(0, 24) + "..."
              : classItem.name} */}
            {classItem.class_entity?.name}
          </Typography>
          <Typography gutterBottom variant="subtitle1">
            {classItem.class_entity?.users[0]?.user_entity.last_name +
              " " +
              classItem.class_entity?.users[0].user_entity.first_name}
          </Typography>
          <Typography
            sx={{
              margin: "auto",
            }}
            variant="body2"
            color="text.secondary"
          >
            {classItem.class_entity?.description?.length >= 250
              ? classItem.class_entity.description.substring(0, 248) + "..."
              : classItem.class_entity?.description}
          </Typography>
        </CardContent>
        {/* form create class */}
        <Dialog
          open={openEditClassForm}
          onClose={() => handleCloseEditClassForm(false)}
        >
          <DialogTitle>Update class</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Class name (require)"
              fullWidth
              variant="standard"
              value={className}
              onChange={(e) => setClassName(e.target.value)}
            />
            <TextField
              autoFocus
              margin="dense"
              label="Section"
              fullWidth
              variant="standard"
              autoComplete="off"
              value={section}
              onChange={(e) => setSection(e.target.value)}
            />
            <TextField
              autoFocus
              margin="dense"
              label="Subject"
              id="subject"
              fullWidth
              variant="standard"
              autoComplete="subject"
              value={subject}
              onChange={(e) => setSubject(e.target.value)}
            />
            <TextField
              autoFocus
              margin="dense"
              label="Room"
              fullWidth
              autoComplete="room"
              variant="standard"
              value={room}
              onChange={(e) => setRoom(e.target.value)}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => handleCloseEditClassForm(false)}
              disabled={isEditing ? true : false}
            >
              Cancel
            </Button>
            <Button
              onClick={() => handleCloseEditClassForm(true)}
              disabled={
                className.length > 0 ? (isEditing ? true : false) : true
              }
            >
              Update
            </Button>
          </DialogActions>
        </Dialog>
        {/* Unenroll class */}
        <Dialog
          open={openUnenrollForm}
          onClose={() => handleCloseUnenrollForm(false)}
        >
          <DialogTitle>
            Unenroll from {" " + classItem.class_entity?.name}?
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              You will be removed from this class.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => handleCloseUnenrollForm(false)}
              disabled={isUnenrolling ? true : false}
            >
              Cancel
            </Button>
            <Button
              onClick={() => handleCloseUnenrollForm(true)}
              disabled={isUnenrolling ? true : false}
            >
              Unenroll
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    </Card>
  );
}
