import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import {
  Avatar,
  IconButton,
  Menu,
  Typography,
  MenuItem,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import React, { useContext, useState } from "react";
import { MoreVert } from "@mui/icons-material";
import { enqueueSnackbar } from "notistack";

import { classesConst, authConst, severityConst } from "../../const";
import { AuthContext, ClassesContext, UserContext } from "../../store/context";
import { useNavigate } from "react-router-dom";
import { classService } from "../../services";

export const NormalDataTable = ({
  list,
  setList,
  morevert,
  isTeacher,
  isCreator,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [openUnenrollForm, setOpenUnenrollForm] = useState(false);
  const [isUnenrolling, setIsUnenrolling] = useState(false);
  const [curElementUserId, setCurElementUserId] = useState(null);
  const [curElementClassId, setCurElementClassId] = useState(null);
  const [curElementName, setCurElementName] = useState(null);
  const [, authDispatch] = useContext(AuthContext);
  const [user] = useContext(UserContext);
  const [, classesDispatch] = useContext(ClassesContext);
  const navigate = useNavigate();
  const open = Boolean(anchorEl);

  const handleStudentClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setCurElementClassId(null);
    setCurElementUserId(null);
    setCurElementName(null);
  };

  const handleCloseUnenrollForm = (isUnenroll, class_id, user_id) => {
    if (isUnenroll && curElementClassId !== null && curElementUserId !== null) {
      setIsUnenrolling(true);

      const data = {
        class_id: class_id,
        user_id: user_id,
      };
      // console.log(data);
      classService
        .deleteParticipant(data)
        .then((data) => {
          // console.log(data);
          enqueueSnackbar("Unenroll class successfully!", {
            variant: severityConst.SUCCESS,
          });

          if (user_id === user.id) {
            classService
              .getAllClassOfUser()
              .then((classesList) => {
                // console.log(data);
                classesDispatch({
                  type: classesConst.SET_CLASSES,
                  payload: classesList,
                });
                navigate("/home");
              })
              .catch((err) => {
                //axios failed - alert
                enqueueSnackbar(err.response?.data.message || err.message, {
                  variant: severityConst.ERROR,
                });
                if (err.response?.data.statusCode === 401) {
                  authDispatch({
                    type: authConst.LOG_OUT,
                  });
                  navigate("/login");
                }
              });
          } else {
            const newUsersList = list.filter(
              (item) => item.user_id !== curElementUserId
            );
            setList(newUsersList);
          }
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
      setIsUnenrolling(false);
    }

    setCurElementClassId(null);
    setCurElementUserId(null);
    setCurElementName(null);
    setOpenUnenrollForm(false);
    setAnchorEl(null);
  };
  return (
    <TableContainer
      component={Paper}
      sx={{ boxShadow: "none", border: "none" }}
    >
      <Table>
        <TableBody>
          {list.map((element) => (
            <TableRow
              key={element.user_id}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row" sx={{ width: 10 }}>
                <Avatar></Avatar>
              </TableCell>
              <TableCell>{element.name}</TableCell>
              {(user.id === element.user_id &&
                element.role !== classesConst.ROLE_CREATOR) ||
              (isCreator && user.id !== element.user_id) ||
              (isTeacher && element.role === classesConst.ROLE_STUDENT) ? (
                <TableCell align="right">
                  <IconButton
                    onClick={(e) => {
                      handleStudentClick(e);
                      setCurElementUserId(element.user_id);
                      setCurElementClassId(element.class_id);
                      setCurElementName(element.name);
                    }}
                  >
                    <MoreVert />
                  </IconButton>
                </TableCell>
              ) : (
                <TableCell align="right"></TableCell>
              )}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {/* menu morevert */}
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
        sx={{
          "& .MuiPaper-root": {
            boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)", // your desired box shadow
          },
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <MenuItem onClick={() => setOpenUnenrollForm(true)}>
          <Typography>Remove</Typography>
        </MenuItem>
      </Menu>
      {/* Unenroll class */}
      <Dialog
        open={openUnenrollForm}
        onClose={() => handleCloseUnenrollForm(false)}
      >
        <DialogTitle>Unenroll this class?</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {user.id === curElementUserId
              ? "You will be removed from this class."
              : `${curElementName} will be removed from this class.`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => handleCloseUnenrollForm(false)}
            disabled={isUnenrolling ? true : false}
          >
            Cancel
          </Button>
          <Button
            onClick={() =>
              handleCloseUnenrollForm(true, curElementClassId, curElementUserId)
            }
            disabled={isUnenrolling ? true : false}
          >
            Unenroll
          </Button>
        </DialogActions>
      </Dialog>
    </TableContainer>
  );
};
