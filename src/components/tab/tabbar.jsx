import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import React, { useState, useEffect, useContext } from "react";
import { useNavigate, useLocation, useParams } from "react-router-dom";
import { useTheme } from "@mui/material/styles";
import { enqueueSnackbar } from "notistack";

import { classService } from "../../services";
import { authConst, classesConst } from "../../const";
import {
  ClassesContext,
  CurrentClassContext,
  UserContext,
} from "../../store/context";

function TabBar() {
  const navigate = useNavigate();
  const { classId } = useParams();
  const location = useLocation();
  const [classes] = useContext(ClassesContext);
  const [currentClass] = useContext(CurrentClassContext);
  const [user] = useContext(UserContext);
  const [value, setValue] = useState(0);
  // const [isTeacher, setIsTeacher] = useState(false);
  const theme = useTheme();
  const colorContext = theme.palette.mode === "dark" ? "#121212" : "white";
  function checkTab(value, role) {
    const tabs = {
      teacher: ["news", "people", "setting", "grades", "grade-reviews"],
      creator: ["news", "people", "setting", "grades", "grade-reviews"],
      student: ["news", "people", "mapping", "points", "grade-reviews"],
    };
    return tabs[role][value] || "news";
  }

  const handleChange = (event, newValue) => {
    const tab = checkTab(newValue, currentClass.role);
    setValue(newValue);
    if (tab === "news") {
      navigate(`/class/${classId}`);
    } else {
      navigate(`/class/${classId}/${tab}`);
    }
  };

  useEffect(() => {
    const pathSegments = location.pathname.split("/");
    const curTab = pathSegments.length > 0 ? pathSegments.length - 1 : 0;
    const lastSegment = pathSegments[curTab];
    if (lastSegment === classId) {
      setValue(0);
    } else if (lastSegment === "people") {
      setValue(1);
    } else if (lastSegment === "setting") {
      setValue(2);
    } else if (lastSegment === "grades") {
      setValue(3);
    } else if (lastSegment === "mapping") {
      setValue(2);
    } else if (lastSegment === "points") {
      setValue(3);
    } else if (lastSegment === "grade-reviews") {
      setValue(4);
    } else {
      setValue(0); // Default to the first tab if the segment is unknown
    }
  }, [location.pathname]);

  return (
    <>
      <Box
        sx={{
          width: "100%",
          position: "fixed",
          backgroundColor: colorContext,
          zIndex: 100,
        }}
      >
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab label="News" />
            <Tab label="People" />
            {currentClass.role === classesConst.ROLE_STUDENT ? (
              <Tab label="Mapping" />
            ) : (
              <Tab label="Setting" />
            )}
            {currentClass.role === classesConst.ROLE_STUDENT ? (
              <Tab label="Points" />
            ) : (
              <Tab label="Grades" />
            )}
            <Tab label="Grade reviews" value={4} />
          </Tabs>
        </Box>
      </Box>
    </>
  );
}

export default TabBar;
