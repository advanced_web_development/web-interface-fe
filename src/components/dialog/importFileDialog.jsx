import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  TextField,
} from "@mui/material";
import React, { useState, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { enqueueSnackbar } from "notistack";
import { severityConst, authConst } from "../../const";
import { gradeService } from "../../services";
import { AuthContext } from "../../store/context";

import * as XLSX from "xlsx";

function ImportFileDialog({
  openDialog,
  setOpen,
  setFlagChange,
  flagChange,
  gradeExamIdToSend,
}) {
  const { classId } = useParams();
  const [, authDispatch] = useContext(AuthContext);
  const [file, setFile] = useState();
  const navigate = useNavigate();

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnChange = (e) => {
    setFile(e.target.files[0]);
  };

  const xlsxFileToArrayAndSend = (file) => {
    const reader = new FileReader();
    if (!gradeExamIdToSend) {
      reader.onload = (event) => {
        const data = new Uint8Array(event.target.result);
        const workbook = XLSX.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
        const array = XLSX.utils.sheet_to_json(sheet, { header: 1 });
        // console.log("array:", array);
        const filteredArray = array.filter(
          (row, index) => index > 0 && row.length > 0
        );

        // assuming the first row contains headers
        const headers = array.shift();
        const payload = {
          studentList: filteredArray.map((row) =>
            headers.reduce((obj, header, index) => {
              if (header === "StudentID") {
                header = "StudentId";
              }
              if (header === "Name") {
                header = "FullName";
              }
              obj[header] = `${row[index]}`;
              return obj;
            }, {})
          ),
          classId,
        };
        // console.log("payload cho nay ne:", payload);
        gradeService
          .uploadStudentFile(payload)
          .then((data) => {
            enqueueSnackbar("Import file successfully!", {
              variant: severityConst.SUCCESS,
            });

            setOpen(false);
            navigate("./", { replace: true });
            setFlagChange((prevFlag) => !prevFlag);
          })
          .catch((err) => handleApiError(err))
          .finally(() => {
            setFile();
          });
        // console.log("payload:", payload);
        // setFile();
        // setOpen(false);
        // setFlagChange(!flagChange);
      };

      reader.readAsArrayBuffer(file);
    } else {
      reader.onload = (event) => {
        const data = new Uint8Array(event.target.result);
        const workbook = XLSX.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
        const array = XLSX.utils.sheet_to_json(sheet, { header: 1 });

        const filteredArray = array.filter(
          (row, index) => index > 0 && row.length > 0
        );
        const headers = array.shift();
        const payload = {
          studentExamList: filteredArray.map((row) =>
            headers.reduce((obj, header, index) => {
              if (header === "Student_id") {
                header = "studentId";
                obj[header] = `${row[index]}`;
              }
              if (header === "Name") {
                return obj;
              }
              if (header === "Grade") {
                header = "grade";
                obj[header] = row[index];
              }

              return {
                ...obj,
                isFinalized: false,
              };
            }, {})
          ),
          classId,
          examId: gradeExamIdToSend,
        };

        gradeService
          .uploadExamFile(payload)
          .then((data) => {
            enqueueSnackbar("Import file exam successfully!", {
              variant: severityConst.SUCCESS,
            });
            setFile();
            setOpen(false);
            setFlagChange((prevFlag) => !prevFlag);
          })
          .catch((err) => handleApiError(err))
          .finally(() => {
            setFile();
          });
        // setFile();
        // setOpen(false);
        // setFlagChange(!flagChange);
      };

      reader.readAsArrayBuffer(file);
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();

    if (!file) {
      enqueueSnackbar("Please select a file to import.", {
        variant: severityConst.WARNING,
      });
      return;
    }

    xlsxFileToArrayAndSend(file);
  };

  const handleApiError = (err) => {
    enqueueSnackbar(err.response?.data.message || err.message, {
      variant: severityConst.ERROR,
    });
    if (err.response?.data.statusCode === 403) {
      navigate("/home");
    }
    if (err.response?.data.statusCode === 401) {
      authDispatch({
        type: authConst.LOG_OUT,
      });
      navigate("/login");
    }
  };

  return (
    <Dialog open={openDialog} onClose={handleClose} fullWidth maxWidth="sm">
      <DialogTitle>Import XLSX Data</DialogTitle>
      <DialogContent>
        <input
          type={"file"}
          id={"xlsxFileInput"}
          accept={".xlsx"}
          onChange={handleOnChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={(e) => handleOnSubmit(e)}>Import</Button>
      </DialogActions>
    </Dialog>
  );
}

export default ImportFileDialog;
