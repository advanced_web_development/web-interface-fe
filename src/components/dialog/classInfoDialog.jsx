import {
  IconButton,
  Typography,
  Button,
  Divider,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  Box,
} from "@mui/material";
import LinkIcon from "@mui/icons-material/Link";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import CloseIcon from "@mui/icons-material/Close";
import { enqueueSnackbar } from "notistack";
import { useState } from "react";

import { severityConst } from "../../const";

export default function ClassInfoDialog({
  openDialog,
  setOpenDialog,
  classInfor,
}) {
  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleCopyLinkInvite = () => {
    navigator.clipboard
      .writeText(
        `${window.location.origin}/invitation/by-link?code=${classInfor.invite_code}`
      )
      .then(() => {
        setOpenDialog(false);
        enqueueSnackbar("Link copied");
      })
      .catch((error) => {});
  };

  return (
    <Dialog
      onClose={handleCloseDialog}
      aria-labelledby="customized-dialog-title"
      open={openDialog}
    >
      <DialogTitle
        sx={{
          display: "flex",
          justifyContent: "flex-end",
        }}
        id="customized-dialog-title"
      >
        <IconButton
          aria-label="close"
          onClick={handleCloseDialog}
          sx={{
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <DialogContent
        sx={{
          paddingBottom: 0,
          minWidth: {
            xl: 600,
          },
        }}
      >
        <Typography
          align="center"
          sx={{
            typography: {
              md: "h1",
              xs: "h2",
            },
            alignSelf: "center",
          }}
          color={"primary"}
        >
          {classInfor?.invite_code}
        </Typography>
        <Divider sx={{ mt: 2 }} />
      </DialogContent>
      <DialogActions sx={{ display: "flex", justifyContent: "start", px: 3 }}>
        <Typography flexGrow={1} color={"primary"}>
          {classInfor?.name}
        </Typography>
        <Button
          disableElevation
          style={{ textTransform: "none" }}
          autoFocus
          startIcon={<ContentCopyIcon />}
          onClick={handleCopyLinkInvite}
        >
          Copy invite link
        </Button>
      </DialogActions>
    </Dialog>
  );
}
