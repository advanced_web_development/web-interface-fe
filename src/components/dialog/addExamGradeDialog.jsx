import {
  Box,
  Grid,
  IconButton,
  Typography,
  Avatar,
  Divider,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Card,
  CardContent,
  InputLabel,
  Select,
  FormControl,
  MenuItem,
} from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import { useNavigate, useParams } from "react-router-dom";
import React, { useEffect, useState, useContext } from "react";
import { enqueueSnackbar } from "notistack";
import { ReactMultiEmail, isEmail } from "react-multi-email";
import "react-multi-email/dist/style.css";

import { severityConst, classesConst, authConst } from "../../const";
import { classService, gradeService } from "../../services";
import ButtonProgress from "../../components/button/buttonProgess";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function ExamGradeDialog({
  openDialog,
  setOpen,
  gradeStructure,
  currentExamDialog,
  setFlagChange,
  flagChange,
  gradeExamExisted,
}) {
  const [gradeCategory, setGradeCategory] = useState(""); // state to store value by select
  const [gradeExam, setGradeExam] = useState();
  const [createGradeExam, setCreateGradeExam] = useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  // console.log("grade exam existed:", gradeExamExisted);

  const handleChangeCategory = (event) => {
    setGradeCategory(event.target.value);
  };

  const handleChangeCreateGradeExam = (e) => {
    let { name, value, type, checked } = e.target;
    let newValue;
    if (name === "title") {
      newValue = type === "checkbox" ? checked : value;
    }
    if (name === "max_grade") {
      if (isNaN(value)) {
        // If not a number, return without making any changes
        return;
      }

      newValue = type === "checkbox" ? checked : parseInt(value, 10);
    }
    setGradeExam((prevData) => ({
      ...prevData,
      [name]: newValue,
    }));
  };

  const handleSubmitChangeInfo = (e) => {
    e.preventDefault();
    setCreateGradeExam(true);
    //get grade composition id
    const grade_compostion = gradeStructure.find(
      (grade) => grade.name === gradeCategory
    );

    //get title, grade category, max_grade to payload
    const payload = {
      title: gradeExam.title,
      max_grade: gradeExam.max_grade,
      grade_composition_id: grade_compostion.id,
      // userId: 2,
    };
    const hasDuplicate = gradeExamExisted.some((item) => {
      if (item.gradeExamTitle === payload.title) {
        return true;
      }
      return false;
    });

    if (hasDuplicate) {
      enqueueSnackbar(`The exam title has existed`, {
        variant: severityConst.WARNING,
      });
      setCreateGradeExam(false);
      return;
    }

    gradeService
      .createGradeExam(payload)
      .then((data) => {
        //   console.log("data:", data);
        enqueueSnackbar("Create an exam grade successfully!", {
          variant: severityConst.SUCCESS,
        });
        setFlagChange((prevFlag) => !prevFlag);
        setOpen(false);
      })
      .catch((err) => {
        //axios failed - alert
        enqueueSnackbar(err.response?.data.message || err.message, {
          variant: severityConst.ERROR,
        });
        if (err.response?.data.statusCode === 403) {
          navigate("/home");
        }
        if (err.response?.data.statusCode === 401) {
          authDispatch({
            type: authConst.LOG_OUT,
          });
          navigate("/login");
        }
      })
      .finally(() => {
        setCreateGradeExam(false);
      });
    setCreateGradeExam(false);
    setOpen(false);
  };

  // console.log("first:", gradeExam);

  useEffect(() => {
    if (currentExamDialog) {
      // console.log("first", currentExamDialog);
      setGradeExam({
        title: currentExamDialog.grade_exam_title,
        max_grade: currentExamDialog.grade_exam_max_grade,
      });
      setGradeCategory(currentExamDialog.grade_exam_category);
    }
  }, []);

  return (
    <Dialog
      fullScreen
      open={openDialog}
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <AppBar
        sx={{
          position: "relative",
          boxShadow: "none",
          backgroundColor: "white",
        }}
      >
        <Toolbar>
          <IconButton edge="start" onClick={handleClose} aria-label="close">
            <CloseIcon />
          </IconButton>
          <Typography
            sx={{ ml: 2, flex: 1, color: "black" }}
            variant="h6"
            component="div"
          >
            Create new exam grade
          </Typography>
          {/* <Button autoFocus sx={{ color: "#7a7979" }} onClick={handleClose}>
                Save
              </Button> */}
        </Toolbar>
      </AppBar>
      <Divider sx={{ color: "##4a4a4a" }} />
      <Box
        component="form"
        sx={{ mx: "auto" }}
        onSubmit={(e) => handleSubmitChangeInfo(e)}
      >
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          sx={{
            width: {
              sm: "60%",
              md: "60%",
              lg: "50%",
              xl: "50%",
            },
            mt: 6,
            mx: "auto",
          }}
        >
          {/* Card contain title and description of exam grade */}
          <Card sx={{ minWidth: 380 }}>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    id="title"
                    label="Grade exam title"
                    name="title"
                    //  autoComplete="name"
                    value={gradeExam?.title ? gradeExam?.title : ""}
                    onChange={(e) => handleChangeCreateGradeExam(e)}
                    required
                    autoFocus
                  />
                </Grid>

                <Grid item xs={6}>
                  <FormControl fullWidth required>
                    <InputLabel id="grade-category">Grade Category</InputLabel>
                    <Select
                      labelId="grade-category"
                      id="grade-category"
                      value={gradeCategory}
                      // renderValue={()=>{}}
                      label="Grade category"
                      onChange={handleChangeCategory}
                    >
                      {gradeStructure &&
                        gradeStructure.map((grade) => (
                          <MenuItem value={grade.name} key={grade.name}>
                            {grade.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    id="max_grade"
                    label="Max grade"
                    name="max_grade"
                    //  autoComplete="name"
                    value={gradeExam?.max_grade ? gradeExam?.max_grade : 0}
                    onChange={(e) => handleChangeCreateGradeExam(e)}
                    required
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

        <ButtonProgress
          loading={createGradeExam}
          text={"Save"}
          onClick={(e) => {
            handleSubmitChangeInfo(e);
          }}
        />
      </Box>
    </Dialog>
  );
}

export default ExamGradeDialog;
