import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  TextField,
} from "@mui/material";
import React, { useState, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { enqueueSnackbar } from "notistack";
import { severityConst, authConst } from "../../const";
import { gradeService } from "../../services";
import { AuthContext } from "../../store/context";

function AlertNotFinalizedDialog({
  openDialog,
  setOpen,
  gradeExamTitle,
  notFinalizedList,
}) {
  const { classId } = useParams();
  const [, authDispatch] = useContext(AuthContext);
  const [file, setFile] = useState();
  const navigate = useNavigate();

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog open={openDialog} onClose={handleClose} fullWidth maxWidth="sm">
      <DialogTitle>
        List student id can not finalized in {gradeExamTitle}
      </DialogTitle>
      <DialogContent>
        <p>{notFinalizedList.join(", ")}</p>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
}

export default AlertNotFinalizedDialog;
