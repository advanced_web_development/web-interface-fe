import {
  Avatar,
  Card,
  Typography,
  CardActionArea,
  CardContent,
  Box,
  Divider,
  Button,
  TextField,
  IconButton,
  CircularProgress,
} from "@mui/material";
import { MoreVert, Send } from "@mui/icons-material";
import { useContext, useEffect, useState } from "react";
import { enqueueSnackbar } from "notistack";

import { formatVietnamDate } from "../../utils/convertDateFormat";
import { AuthContext, PostsContext, UserContext } from "../../store/context";
import { classService } from "../../services";
import { classesConst, authConst, severityConst } from "../../const";
import { useNavigate } from "react-router-dom";
import PostMenu from "./postMenu";

export default function ClassPost({ post, isTeacher }) {
  const [user] = useContext(UserContext);
  const suffixes = post?.comments?.length > 1 ? "s" : "";
  const [creatingComment, setCreatingComment] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [showAllCmt, setShowAllCmt] = useState(false);
  const [posts, postsDispatch] = useContext(PostsContext);
  const [, authDispatch] = useContext(AuthContext);
  const navigate = useNavigate();

  const handleCreateComment = () => {
    setCreatingComment(true);
    if (newComment.length > 0) {
      classService
        .createPostComment({
          postId: post.id,
          content: newComment,
        })
        .then((data) => {
          const postIdToUpdate = data.class_post_id;
          // Create a new array with the updated posts
          const updatedPosts = posts.map((post) => {
            if (post.id === postIdToUpdate) {
              return {
                ...post,
                comments: [...post.comments, data],
              };
            }
            return post; // Keep other posts unchanged
          });
          postsDispatch({
            type: classesConst.SET_POSTS,
            payload: updatedPosts,
          });
          setNewComment("");
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .then(() => {
          setCreatingComment(false);
        });
    }
  };

  const handleToggleCmts = () => {
    setShowAllCmt((prev) => !prev);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        border: "1px solid #acacac",
        borderRadius: 1,
        mb: 2,
      }}
    >
      <CardContent sx={{ p: 2 }}>
        <Box display={"flex"}>
          <Avatar>{post?.author?.first_name?.charAt(0)}</Avatar>
          <Box display={"flex"} flexGrow={1} flexDirection={"column"} px={2}>
            <Typography>{`${post?.author?.last_name} ${post?.author?.first_name}`}</Typography>
            <Typography variant="caption" sx={{ opacity: 0.5 }}>
              {formatVietnamDate(post.created_at)}
            </Typography>
          </Box>
          {isTeacher || user?.id === post?.author?.id ? (
            <PostMenu postId={post.id} content={post.content} />
          ) : null}
        </Box>
        <Typography variant="body2" pt={1}>
          {post?.content}
        </Typography>
      </CardContent>
      <Divider />
      <Box
        sx={{ px: 2, pb: 2, pt: 1 }}
        display={"flex"}
        flexDirection={"column"}
      >
        <Button
          color="standardGrey"
          size="small"
          sx={{ alignSelf: "self-start", mb: 1 }}
          onClick={handleToggleCmts}
        >
          {`${post?.comments?.length} comment${suffixes}`}
        </Button>
        {showAllCmt ? (
          post.comments.map((cmt) => (
            <Box key={cmt.id} display={"flex"} alignItems={"center"} mb={1}>
              <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                {cmt?.user?.first_name?.charAt(0)}
              </Avatar>
              <Box display={"flex"} flexDirection={"column"}>
                <Box display={"flex"}>
                  <Typography variant="body2">{`${cmt?.user?.last_name} ${cmt?.user?.first_name}`}</Typography>
                  <Typography ml={1} variant="caption">
                    {formatVietnamDate(cmt.created_at)}
                  </Typography>
                </Box>
                <Typography variant="body2">{cmt.content}</Typography>
              </Box>
            </Box>
          ))
        ) : post.comments.length > 0 ? (
          <Box
            key={post.comments[post?.comments?.length - 1].id}
            display={"flex"}
            alignItems={"center"}
            mb={1}
          >
            <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
              {post.comments[
                post?.comments?.length - 1
              ]?.user?.first_name?.charAt(0)}
            </Avatar>
            <Box display={"flex"} flexDirection={"column"}>
              <Box display={"flex"}>
                <Typography variant="body2">{`${
                  post.comments[post?.comments?.length - 1]?.user?.last_name
                } ${
                  post.comments[post?.comments?.length - 1]?.user?.first_name
                }`}</Typography>
                <Typography ml={1} variant="caption">
                  {formatVietnamDate(
                    post.comments[post?.comments?.length - 1].created_at
                  )}
                </Typography>
              </Box>
              <Typography variant="body2">
                {post.comments[post?.comments?.length - 1].content}
              </Typography>
            </Box>
          </Box>
        ) : null}
        <Box display={"flex"} alignItems={"center"}>
          <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
            {user.firstName.charAt(0)}
          </Avatar>
          <TextField
            id="outlined-basic"
            label="Add a class comment"
            variant="outlined"
            size="small"
            value={newComment}
            onChange={(e) => setNewComment(e.target.value)}
            fullWidth
          />
          <IconButton
            disabled={creatingComment || newComment.length === 0}
            onClick={handleCreateComment}
          >
            {creatingComment ? <CircularProgress /> : <Send />}
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
