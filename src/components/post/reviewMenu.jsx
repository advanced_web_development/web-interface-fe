import { useContext, useEffect, useState } from "react";
import {
  FiberManualRecord,
  MoreVert,
  Notifications,
  NotificationsActive,
} from "@mui/icons-material";
import {
  Badge,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Button,
  CircularProgress,
  TextField,
  Divider,
  FormControl,
  InputLabel,
  Select,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";

import { PostsContext, ReviewsContext } from "../../store/context";
import { classService, notiService } from "../../services";
import { classesConst, notiConst, severityConst } from "../../const";
import { useLocation, useNavigate } from "react-router-dom";
import { DELETE_POST, EDIT_POST, EDIT_REVIEW } from "../../const/classes";

export default function ReviewMenu({ reviewPost }) {
  const [anchorElUser, setAnchorElUser] = useState(null);
  const navigate = useNavigate();
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [updatedGrade, setUpdatedGrade] = useState(
    reviewPost?.updated_grade || ""
  );
  const [updatedState, setUpdatedState] = useState(reviewPost?.state);
  const [, reviewPostsDispatch] = useContext(ReviewsContext);
  const location = useLocation();
  const isPostPage = location.pathname.includes("review");
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = (type) => {
    setAnchorElUser(null);
    if (type === "delete") {
      setOpenDeleteDialog(true);
    }
    if (type === "edit") {
      setOpenEditDialog(true);
    }
  };

  // const handleCloseDeleteDialog = (isDelete) => {
  //   setOpenDeleteDialog(false);
  //   if (isDelete) {
  //     //delete post and update
  //     classService
  //       .deleteClassPost(reviewId)
  //       .then((data) => {
  //         enqueueSnackbar("Delete successfully", {
  //           variant: severityConst.SUCCESS,
  //         });
  //         reviewPostsDispatch({
  //           type: DELETE_POST,
  //           payload: reviewId,
  //         });
  //         if (isPostPage) {
  //           navigate(`/class/${data.class_id}`);
  //         }
  //       })
  //       .catch((err) => {
  //         //axios failed - alert
  //         enqueueSnackbar(err.response?.data.message || err.message, {
  //           variant: severityConst.ERROR,
  //         });
  //         if (err.response?.data.statusCode === 401) {
  //           authDispatch({
  //             type: authConst.LOG_OUT,
  //           });
  //           navigate("/login");
  //         }
  //       });
  //   }
  // };

  const handleCloseEditDialog = (isEdit) => {
    setOpenEditDialog(false);
    if (isEdit) {
      classService
        .updateReviewPost({
          updatedGrade: updatedGrade.length > 0 ? +updatedGrade : null,
          examReviewId: reviewPost.id,
          status: updatedState,
        })
        .then((data) => {
          enqueueSnackbar("Edit successfully", {
            variant: severityConst.SUCCESS,
          });
          reviewPostsDispatch({
            type: EDIT_REVIEW,
            payload: data,
          });
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    } else {
      setUpdatedGrade(reviewPost?.updated_grade || "");
    }
  };

  useEffect(() => {
    setUpdatedGrade(reviewPost?.updated_grade || "");
  }, [reviewPost]);

  return (
    <>
      <IconButton sx={{ width: 30, height: 30 }} onClick={handleOpenUserMenu}>
        <MoreVert />
      </IconButton>
      <Menu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={() => handleCloseUserMenu()}
      >
        <MenuItem onClick={() => handleCloseUserMenu("edit")}>Edit</MenuItem>
        {/* <MenuItem onClick={() => handleCloseUserMenu("delete")}>
          Delete
        </MenuItem> */}
      </Menu>

      {/* dialog for confirming delete */}
      {/* <Dialog
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete announcement?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Comments will also be deleted
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleCloseDeleteDialog(false)}>Cancel</Button>
          <Button onClick={() => handleCloseDeleteDialog(true)} autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog> */}

      {/* dialog for editing */}
      <Dialog
        open={openEditDialog}
        onClose={() => handleCloseEditDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Update grade review</DialogTitle>
        <Divider variant="middle" />
        <DialogContent>
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <TextField
              value={updatedGrade}
              onChange={(e) => {
                var value = parseInt(e.target.value, 10);
                if (isNaN(value)) {
                  setUpdatedGrade("");
                  return;
                }
                if (value > reviewPost?.ExamStudent.Exam.max_grade)
                  value = reviewPost?.ExamStudent.Exam.max_grade;
                if (value < 0) value = 0;

                setUpdatedGrade(value.toString());
              }}
              type="number"
              label={`Update grade (max: ${reviewPost?.ExamStudent?.Exam.max_grade})`}
              InputProps={{
                min: 0,
                max: reviewPost?.ExamStudent?.Exam.max_grade,
              }}
            />
            <FormControl sx={{ mt: 2 }}>
              <InputLabel>State</InputLabel>
              <Select
                value={updatedState}
                onChange={(e) => setUpdatedState(e.target.value)}
                label="State"
              >
                <MenuItem value={classesConst.NEW_REQUEST}>New</MenuItem>
                <MenuItem value={classesConst.INPROG_REQUEST}>
                  Processing
                </MenuItem>
                <MenuItem value={classesConst.RESOLVED_REQUEST}>
                  Resolved
                </MenuItem>
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleCloseEditDialog(false)}>Cancel</Button>
          <Button onClick={() => handleCloseEditDialog(true)}>Update</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
