import {
  Avatar,
  Card,
  Typography,
  CardActionArea,
  CardContent,
  Box,
  Divider,
  Button,
  TextField,
  IconButton,
  CircularProgress,
  CardActions,
  Collapse,
} from "@mui/material";
import {
  Announcement,
  Check,
  CheckCircleOutline,
  HourglassBottom,
  MoreVert,
  Pending,
  Send,
} from "@mui/icons-material";
import { useContext, useEffect, useState } from "react";
import { enqueueSnackbar } from "notistack";

import { formatVietnamDate } from "../../utils/convertDateFormat";
import {
  AuthContext,
  PostsContext,
  ReviewsContext,
  UserContext,
} from "../../store/context";
import { classService } from "../../services";
import { classesConst, authConst, severityConst } from "../../const";
import { useNavigate } from "react-router-dom";
import ReviewMenu from "./reviewMenu";

const convertStatus = (s) => {
  if (s === classesConst.NEW_REQUEST) {
    return "new";
  } else if (s === classesConst.INPROG_REQUEST) {
    return "processing";
  } else {
    return "resolved";
  }
};

export default function ReviewPost({ reviewPost, isTeacher }) {
  const [user] = useContext(UserContext);
  const suffixes = reviewPost?.ReviewComment?.length > 1 ? "s" : "";
  const [creatingComment, setCreatingComment] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [showAllCmt, setShowAllCmt] = useState(false);
  const [reviewPosts, reviewPostsDispatch] = useContext(ReviewsContext);
  const [, authDispatch] = useContext(AuthContext);
  const [openDetail, setOpenDetail] = useState(false);
  const navigate = useNavigate();

  const handleCreateComment = () => {
    setCreatingComment(true);
    if (newComment.length > 0) {
      classService
        .createReviewPostComment({
          examId: reviewPost.ExamStudent?.Exam?.id,
          classId: reviewPost.ExamStudent?.Exam?.GradeComposition?.class_id,
          examReviewId: reviewPost.id,
          content: newComment,
        })
        .then((data) => {
          const postIdToUpdate = data.exam_review_id;
          // Create a new array with the updated posts
          const updatedReviewPosts = reviewPosts.map((post) => {
            if (post.id === postIdToUpdate) {
              return {
                ...post,
                ReviewComment: [...post.ReviewComment, data],
              };
            }
            return post; // Keep other posts unchanged
          });
          reviewPostsDispatch({
            type: classesConst.SET_REVIEWS,
            payload: updatedReviewPosts,
          });
          setNewComment("");
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        })
        .then(() => {
          setCreatingComment(false);
        });
    }
  };

  const handleToggleCmts = () => {
    setShowAllCmt((prev) => !prev);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        border: "1px solid #acacac",
        borderRadius: 1,
        my: 1,
      }}
    >
      <CardActionArea onClick={() => setOpenDetail((pre) => !pre)}>
        <CardContent>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              alignContent: "center",
            }}
          >
            <Announcement />
            <Typography
              sx={{ flexGrow: 1 }}
            >{`Grade review for exam ${reviewPost.ExamStudent.Exam.title} of student ID: ${reviewPost.ExamStudent?.student_class_student_id}`}</Typography>
            <Typography variant="caption" sx={{ opacity: 0.5 }}>
              {`Requested ${formatVietnamDate(reviewPost.created_at)}`}
            </Typography>
          </Box>
          <Box display={"flex"}>
            <Typography sx={{ mr: 1 }}>{`Status: ${convertStatus(
              reviewPost.state
            )}`}</Typography>
            {reviewPost.state === classesConst.NEW_REQUEST ? (
              <Pending color="error" />
            ) : reviewPost.state === classesConst.INPROG_REQUEST ? (
              <HourglassBottom color="warning" />
            ) : (
              <CheckCircleOutline color="success" />
            )}
          </Box>
        </CardContent>
      </CardActionArea>
      <Divider />
      <Collapse in={openDetail}>
        <Box sx={{ display: "flex", px: 2, pb: 2, pt: 1 }}>
          <Box sx={{ display: "flex", flexDirection: "column", flexGrow: 1 }}>
            <Typography variant="body2">{`Explaination:   ${reviewPost.explaination}`}</Typography>
            <Typography variant="body2">{`Expected grade: ${reviewPost.expected_grade}/${reviewPost.ExamStudent.Exam.max_grade}`}</Typography>
            <Typography variant="body2">{`Updated grade: ${
              reviewPost.updated_grade ? reviewPost.updated_grade : ""
            }`}</Typography>
            {reviewPost.updated_at ? (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "start",
                }}
              >
                <Typography variant="body2">{`Updated by: ${
                  reviewPost.User.last_name + " " + reviewPost.User.first_name
                } at ${formatVietnamDate(reviewPost.updated_at)}`}</Typography>
              </Box>
            ) : null}
          </Box>
          {isTeacher || user?.id === reviewPost?.author?.id ? (
            <ReviewMenu reviewPost={reviewPost} />
          ) : null}
        </Box>
        <Divider />
        <Box
          sx={{ px: 2, pb: 2, pt: 1 }}
          display={"flex"}
          flexDirection={"column"}
        >
          <Button
            color="standardGrey"
            size="small"
            sx={{ alignSelf: "self-start", mb: 1 }}
            onClick={handleToggleCmts}
          >
            {`${reviewPost?.ReviewComment?.length} comment${suffixes}`}
          </Button>
          {showAllCmt ? (
            reviewPost.ReviewComment.map((cmt) => (
              <Box key={cmt.id} display={"flex"} alignItems={"center"} mb={1}>
                <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                  {cmt?.User?.first_name?.charAt(0)}
                </Avatar>
                <Box display={"flex"} flexDirection={"column"}>
                  <Box display={"flex"}>
                    <Typography variant="body2">{`${cmt?.User?.last_name} ${cmt?.User?.first_name}`}</Typography>
                    <Typography ml={1} variant="caption">
                      {formatVietnamDate(cmt.created_at)}
                    </Typography>
                  </Box>
                  <Typography variant="body2">{cmt.content}</Typography>
                </Box>
              </Box>
            ))
          ) : reviewPost.ReviewComment?.length > 0 ? (
            <Box
              key={
                reviewPost.ReviewComment[reviewPost?.ReviewComment?.length - 1]
                  .id
              }
              display={"flex"}
              alignItems={"center"}
              mb={1}
            >
              <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
                {reviewPost.ReviewComment[
                  reviewPost?.ReviewComment?.length - 1
                ]?.User?.first_name?.charAt(0)}
              </Avatar>
              <Box display={"flex"} flexDirection={"column"}>
                <Box display={"flex"}>
                  <Typography variant="body2">{`${
                    reviewPost.ReviewComment[
                      reviewPost?.ReviewComment?.length - 1
                    ]?.User?.last_name
                  } ${
                    reviewPost.ReviewComment[
                      reviewPost?.ReviewComment?.length - 1
                    ]?.User?.first_name
                  }`}</Typography>
                  <Typography ml={1} variant="caption">
                    {formatVietnamDate(
                      reviewPost.ReviewComment[
                        reviewPost?.ReviewComment?.length - 1
                      ].created_at
                    )}
                  </Typography>
                </Box>
                <Typography variant="body2">
                  {
                    reviewPost.ReviewComment[
                      reviewPost?.ReviewComment?.length - 1
                    ].content
                  }
                </Typography>
              </Box>
            </Box>
          ) : null}
          <Box display={"flex"} alignItems={"center"}>
            <Avatar sx={{ mr: 1, width: 30, height: 30 }}>
              {user.firstName.charAt(0)}
            </Avatar>
            <TextField
              id="outlined-basic"
              label="Add a class comment"
              variant="outlined"
              size="small"
              value={newComment}
              onChange={(e) => setNewComment(e.target.value)}
              fullWidth
            />
            <IconButton
              disabled={creatingComment || newComment.length === 0}
              onClick={handleCreateComment}
            >
              {creatingComment ? <CircularProgress /> : <Send />}
            </IconButton>
          </Box>
        </Box>
      </Collapse>
    </Box>
  );
}
