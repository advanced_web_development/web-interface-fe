import { useContext, useEffect, useState } from "react";
import {
  FiberManualRecord,
  MoreVert,
  Notifications,
  NotificationsActive,
} from "@mui/icons-material";
import {
  Badge,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Button,
  CircularProgress,
  TextField,
} from "@mui/material";
import { enqueueSnackbar } from "notistack";

import { PostsContext } from "../../store/context";
import { classService, notiService } from "../../services";
import { notiConst, severityConst } from "../../const";
import { formatVietnamDate } from "../../utils/convertDateFormat";
import { useLocation, useNavigate } from "react-router-dom";
import { DELETE_POST, EDIT_POST } from "../../const/classes";

export default function PostMenu({ postId, content }) {
  const [anchorElUser, setAnchorElUser] = useState(null);
  const navigate = useNavigate();
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [updateContent, setUpdateContent] = useState(content);
  const [, postsDispatch] = useContext(PostsContext);
  const location = useLocation();
  const isPostPage = location.pathname.includes("post");

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = (type) => {
    setAnchorElUser(null);
    if (type === "delete") {
      setOpenDeleteDialog(true);
    }
    if (type === "edit") {
      setOpenEditDialog(true);
    }
  };

  const handleCloseDeleteDialog = (isDelete) => {
    setOpenDeleteDialog(false);
    if (isDelete) {
      //delete post and update
      classService
        .deleteClassPost(postId)
        .then((data) => {
          enqueueSnackbar("Delete successfully", {
            variant: severityConst.SUCCESS,
          });
          postsDispatch({
            type: DELETE_POST,
            payload: postId,
          });
          if (isPostPage) {
            navigate(`/class/${data.class_id}`);
          }
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    }
  };

  const handleCloseEditDialog = (isEdit) => {
    setOpenEditDialog(false);
    if (isEdit) {
      classService
        .updateClassPost(
          {
            content: updateContent,
          },
          postId
        )
        .then((data) => {
          enqueueSnackbar("Edit successfully", {
            variant: severityConst.SUCCESS,
          });
          postsDispatch({
            type: EDIT_POST,
            payload: data,
          });
        })
        .catch((err) => {
          //axios failed - alert
          enqueueSnackbar(err.response?.data.message || err.message, {
            variant: severityConst.ERROR,
          });
          if (err.response?.data.statusCode === 401) {
            authDispatch({
              type: authConst.LOG_OUT,
            });
            navigate("/login");
          }
        });
    } else {
      setUpdateContent(content);
    }
  };

  useEffect(() => {
    setUpdateContent(content);
  }, [content]);

  return (
    <>
      <IconButton onClick={handleOpenUserMenu}>
        <MoreVert />
      </IconButton>
      <Menu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={() => handleCloseUserMenu()}
      >
        <MenuItem onClick={() => handleCloseUserMenu("edit")}>Edit</MenuItem>
        <MenuItem onClick={() => handleCloseUserMenu("delete")}>
          Delete
        </MenuItem>
      </Menu>

      {/* dialog for confirming delete */}
      <Dialog
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete announcement?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Comments will also be deleted
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleCloseDeleteDialog(false)}>Cancel</Button>
          <Button onClick={() => handleCloseDeleteDialog(true)} autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>

      {/* dialog for editing */}
      <Dialog
        open={openEditDialog}
        onClose={() => handleCloseEditDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Announcement</DialogTitle>
        <DialogContent
          sx={{
            minWidth: {
              xs: 280,
              sm: 350,
              md: 500,
              lg: 600,
            },
          }}
        >
          <TextField
            fullWidth
            value={updateContent}
            onChange={(e) => setUpdateContent(e.target.value)}
            multiline
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleCloseEditDialog(false)}>Cancel</Button>
          <Button onClick={() => handleCloseEditDialog(true)}>Save</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
