import { Grid, Button, CircularProgress } from "@mui/material";
import { Facebook, Google } from "@mui/icons-material";
import { useState } from "react";
import { enqueueSnackbar } from "notistack";

import { authService } from "../../services";
import { severityConst } from "../../const";
import { BASE_URL } from "../../const/api";

export default function ThirdPartyLogin() {
  const [loadingLoginFacebook, setLoadingLoginFacebook] = useState(false);
  const [loadingLoginGoogle, setLoadingLoginGoogle] = useState(false);

  const handleSubmitLoginFacebook = () => {
    setLoadingLoginFacebook(true);
    window.location.replace(BASE_URL + "/auth/facebook");
  };

  const handleSubmitLoginGoogle = () => {
    setLoadingLoginGoogle(true);
    window.location.replace(BASE_URL + "/auth/google");
  };

  return (
    <Grid container justifyContent="space-around" sx={{ mt: 2 }}>
      <Grid item xs={5}>
        <Button
          fullWidth
          variant="outlined"
          type="button"
          onClick={() => handleSubmitLoginFacebook()}
          disabled={loadingLoginFacebook}
          sx={{
            borderColor: "gray",
          }}
          startIcon={
            loadingLoginFacebook ? <CircularProgress size={18} /> : <Facebook />
          }
        >
          Facebook
        </Button>
      </Grid>
      <Grid item xs={5}>
        <Button
          fullWidth
          variant="outlined"
          type="button"
          onClick={() => handleSubmitLoginGoogle()}
          disabled={loadingLoginGoogle}
          sx={{
            borderColor: "gray",
            color: "red",
          }}
          startIcon={
            loadingLoginGoogle ? (
              <CircularProgress
                sx={{
                  color: "red",
                }}
                size={18}
              />
            ) : (
              <Google
                sx={{
                  color: "red",
                }}
              />
            )
          }
        >
          Google
        </Button>
      </Grid>
    </Grid>
  );
}
