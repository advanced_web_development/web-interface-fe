import { io } from "socket.io-client";
const URL = import.meta.env.VITE_NOTIFICATIONS_URL;
const PATH = import.meta.env.VITE_SOCKET_PATH;

export const socket = io(URL, {
  autoConnect: true,
  path: PATH,
});
