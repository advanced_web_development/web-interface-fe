import axios, { privateAxios } from "../libs/axios";

export const getNotis = async (lastId) => {
  const response = await privateAxios.get(
    `/users/notifications?lastId=${lastId}`
  );
  return response?.data;
};

export const countingUnSeenNotis = async () => {
  const response = await privateAxios.get(
    `/users/notifications/counting-un-seen`
  );
  return response?.data;
};

export const seenNotis = async (postId) => {
  const response = await privateAxios.put(`/users/notifications/${postId}`);
  return response?.data;
};
