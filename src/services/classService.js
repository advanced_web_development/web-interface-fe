import axios, { privateAxios } from "../libs/axios";

export const createClass = async (payload) => {
  const response = await privateAxios.post("/v1/classroom", {
    ...payload,
  });
  return response?.data;
};
export const updateClass = async (payload) => {
  const response = await privateAxios.put("/v1/classroom", {
    ...payload,
  });
  return response?.data;
};
export const getClassInfo = async (id) => {
  const response = await privateAxios.get(`/v1/classroom/${id}`);
  return response?.data;
};
export const addStudent = async (payload) => {
  const response = await privateAxios.post("/v1/enrollment/student", {
    ...payload,
  });
  return response?.data;
};
export const addTeacher = async (payload) => {
  const response = await privateAxios.post("/v1/enrollment/teacher", {
    ...payload,
  });
  return response?.data;
};
export const getAllUserOfClass = async (id) => {
  const response = await privateAxios.get(`/v1/enrollment/class/${id}`);
  return response?.data;
};
export const getAllClassOfUser = async () => {
  const response = await privateAxios.get("/v1/enrollment/user");
  return response?.data;
};
export const deleteParticipant = async (payload) => {
  const response = await privateAxios.delete("/v1/enrollment", {
    data: {
      ...payload,
    },
  });
  return response?.data;
};
export const inviteTeachersByMaiil = async (payload) => {
  const response = await privateAxios.post(
    "/v1/classroom/invite/teachers/by-mail",
    {
      ...payload,
    }
  );
  return response?.data;
};
export const inviteStudentsByMaiil = async (payload) => {
  const response = await privateAxios.post(
    "/v1/classroom/invite/students/by-mail",
    {
      ...payload,
    }
  );
  return response?.data;
};
export const addTeacherByToken = async (payload) => {
  const response = await privateAxios.post(
    "/v1/classroom/add-teacher-by-token",
    {
      ...payload,
    }
  );
  return response?.data;
};
export const addStudentByToken = async (payload) => {
  const response = await privateAxios.post(
    "/v1/classroom/add-student-by-token",
    {
      ...payload,
    }
  );
  return response?.data;
};

export const toggleEnableInviteCodes = async (id) => {
  const response = await privateAxios.post(
    `/v1/classroom/toggle-enable-invite-code/${id}`
  );
  return response?.data;
};

export const createClassPost = async (payload) => {
  const response = await privateAxios.post("/v1/classroom/class-post", {
    ...payload,
  });
  return response?.data;
};

export const getClassPost = async (postId) => {
  const response = await privateAxios.get(`/v1/classroom/class-post/${postId}`);
  return response?.data;
};

export const getClassPostWithPagination = async (classId, lastId) => {
  const response = await privateAxios.get(
    `/v1/classroom/all-class-posts/${classId}?lastPostId=${lastId}`
  );
  return response?.data;
};

export const createPostComment = async (payload) => {
  const response = await privateAxios.post("/v1/classroom/post-comment", {
    ...payload,
  });
  return response?.data;
};

export const deleteClassPost = async (postId) => {
  const response = await privateAxios.delete(
    `/v1/classroom/class-post/${postId}`
  );
  return response?.data;
};

export const updateClassPost = async (payload, postId) => {
  const response = await privateAxios.put(
    `/v1/classroom/class-post/${postId}`,
    {
      ...payload,
    }
  );
  return response?.data;
};

export const createReviewPostComment = async (payload) => {
  const response = await privateAxios.post(
    "/exam/class/review/comment/create",
    {
      ...payload,
    }
  );
  return response?.data;
};

export const updateReviewPost = async (payload) => {
  const response = await privateAxios.post(`/exam/teacher/review/status/set`, {
    ...payload,
  });
  return response?.data;
};

export const getReviewPost = async (postId) => {
  const response = await privateAxios.get(
    `/exam/class/review/get?examReviewId=${postId}`
  );
  return response?.data;
};
