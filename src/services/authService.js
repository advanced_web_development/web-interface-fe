import axios, { privateAxios } from "../libs/axios";

export const signUp = async (payload) => {
  const response = await axios.post("/auth/jwt/sign_up", {
    ...payload,
  });
  return response?.data;
};

export const loginJwt = async ({ username, password }) => {
  const response = await axios.post("/auth/jwt/sign_in", {
    username,
    password,
  });
  return response?.data;
};

export const loginFacebook = async () => {
  const response = await axios.get("/auth/facebook");
  return response;
};

export const loginGoogle = async () => {
  const response = await axios.get("/auth/google");
  return response;
};

export const logout = async () => {
  const response = await privateAxios.post("/auth/jwt/sign_out");
  return response;
};

export const getProfile = async () => {
  const response = await privateAxios.get(`/users/detail`);
  return response?.data;
};

export const postReSendEmail = async () => {
  const response = await privateAxios.post(
    `/auth/jwt/resend_email_confirmation`
  );
};

export const putTokenConfirm = async (payload) => {
  const response = await privateAxios.put("/auth/jwt/confirm", { ...payload });
  return response?.data;
};

export const changePassword = async (payload) => {
  const response = await privateAxios.put("/users/update_password", {
    ...payload,
  });
  return response?.data;
};

export const updateAccount = async (payload) => {
  const response = await privateAxios.put("/users/update_account", {
    ...payload,
  });
  return response?.data;
};

export const changeProfile = async (payload) => {
  const response = await privateAxios.put("/users/update", {
    ...payload,
  });
  return response?.data;
};

export const submitUsernameForgetPassword = async (payload) => {
  const response = await axios.post("/auth/jwt/forgot_password", {
    ...payload,
  });
  return response?.data;
};

export const verifyCodeForgetPassword = async (payload) => {
  const response = await axios.post("/auth/jwt/verify_code", {
    ...payload,
  });
  return response?.data;
};

export const updateForgetPassword = async (payload) => {
  const response = await axios.post("/auth/jwt/update_password", {
    ...payload,
  });
  return response?.data;
};

export const wakeUpMailService = async () => {
  const response = await axios.get("https://mail-service.adaptable.app/");
  if (response?.data) {
    // console.log("wake up mail service!");
  }
};
