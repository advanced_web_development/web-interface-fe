export * as authService from "./authService";
export * as classService from "./classService";
export * as gradeService from "./gradeService";
export * as notiService from "./notiService";
