import axios, { privateAxios } from "../libs/axios";

export const getGradeStructureClass = async (class_id) => {
  const response = await privateAxios.get(`/grade/read`, {
    params: { class_id },
  });
  return response?.data;
};

export const updateGradeStructureListClass = async (payload) => {
  // console.log("payload:", payload);
  const response = await privateAxios.post(`/grade/update-list`, {
    ...payload,
  });
  return response?.data;
};

//Grade
export const createGradeExam = async (payload) => {
  const response = await privateAxios.post(`/exam/teacher/create`, {
    ...payload,
  });
  return response?.data;
};

export const getExamGradeByClassId = async (payload) => {
  const response = await privateAxios.get(`/exam/class/get-by-class`, {
    params: { classId: payload },
  });
  return response?.data;
};
export const getExamGradeByGradeComposition = async (payload) => {
  const response = await privateAxios.get(
    `/exam/teacher/get-by-grade-composition`,
    {
      ...payload,
    }
  );
  return response?.data;
};

export const updateExamGrade = async (payload) => {
  const response = await privateAxios.patch(`/exam/teacher/update`, {
    ...payload,
  });
  return response?.data;
};
export const deleteExamGrade = async (payload) => {
  const response = await privateAxios.delete(`/exam/teacher/delete`, {
    ...payload,
  });
  return response?.data;
};

export const uploadExamFile = async (payload) => {
  const response = await privateAxios.post(`/exam/class/upload-exam-mark`, {
    ...payload,
  });
  return response?.data;
};

export const saveTable = async (payload) => {
  const response = await privateAxios.post(`/exam/upload-all`, {
    ...payload,
  });
  return response?.data;
};

export const requestReviewGradeExam = async (payload) => {
  const response = await privateAxios.post(`/exam/student/review/create`, {
    ...payload,
  });
  return response?.data;
};

export const getAllExamMarkForClass = async (payload) => {
  const response = await privateAxios.get(`/exam/class/all-exam-result`, {
    params: { classId: payload },
  });
  return response?.data;
};

export const getAllExamMarkForStudent = async (payload) => {
  const response = await privateAxios.get(`/exam/student/all-exam-result`, {
    params: {
      classId: payload.classId,
      studentId: payload.studentId,
    },
  });
  return response?.data;
};

export const finalizedForExamGrade = async (payload) => {
  const response = await privateAxios.post(`/exam/class/finalize`, {
    ...payload,
  });
  return response?.data;
};

export const downLoadTemplateMapped = async (classId) => {
  const response = await privateAxios.get(`/exam/template/mapped`, {
    params: { classId },
    responseType: "blob",
  });
  const blob = new Blob([response.data], {
    type: response.headers["content-type"],
  });
  const url = window.URL.createObjectURL(blob);

  const a = document.createElement("a");
  a.href = url;
  a.download = "studentListMapped.xlsx"; // Specify the file name with extension
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);

  window.URL.revokeObjectURL(url);
  // return response?.data;
};

//mapping
export const uploadStudentFile = async (payload) => {
  const response = await privateAxios.post(`/mapping/upload-student`, {
    ...payload,
  });
  return response?.data;
};

export const getStudentList = async (payload) => {
  const response = await privateAxios.get(`/mapping/get-student-list`, {
    params: { classId: payload },
  });
  return response?.data;
};

export const getMappedStudentList = async (payload) => {
  const response = await privateAxios.get(`/mapping/get-mapped-student-list`, {
    params: { classId: payload },
  });
  return response?.data;
};

export const mapStudent = async (payload) => {
  const response = await privateAxios.post(`/mapping/map-student`, {
    ...payload,
  });
  return response?.data;
};

// export const getStudentById = async (payload) => {
//   const response = await privateAxios.get(`/mapping/get-student?classId=`);
//   return response?.data;
// };

export const downloadTemplateStudent = async (payload) => {
  const response = await privateAxios.get(`/mapping/download-template`, {
    params: { isEmailIncluded: payload },
  });
  return response?.data;
};

export const getAllClassGradeReviews = async (classId) => {
  const response = await privateAxios.get(
    `/exam/class/review/get-all?classId=${classId}`
  );
  return response?.data;
};

export const getGradeReviewById = async (examReviewId) => {
  const response = await privateAxios.get(
    `/exam/class/review/get?examReviewId=${examReviewId}`
  );
  return response?.data;
};

export const getGradeReviewsByClassAndUser = async (classId) => {
  const response = await privateAxios.get(
    `/exam/class/review/get-by-user-class?classId=${classId}`
  );
  return response?.data;
};
