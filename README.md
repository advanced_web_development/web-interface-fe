# Environment setup

## 1. Install all node modules.

Open the terminal in the source code folder and run the following command to install all the necessary node modules:

“npm i”

## 2. Create .env file

The source code requires a .env file to store secret data. Create a .env file in the same level with src folder with the following content:

VITE_BASE_URL="https://auth-service-gygw.onrender.com"
VITE_AUTO_HIDE_DURATION = 5000;
VITE_MAX_SNACK = 3;

Note 1: When running the project locally, the VITE_BASE_URL should be “http://localhost:3000” instead.

## 3. Start the project

Run “npm run dev” to start the project.
